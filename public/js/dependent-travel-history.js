var workCount = 0;
$('#dependent_addTravel').click(function () {

    $('.dependent-travel-table-box').show();
    $('#travelDependentHeader').show();

    count++;

    var row = '<tr class="remove-dependent-travel removeRow' + count + '" id="travelTable' + count + '"> <td contenteditable="true" id="dependent_tableCountry' + count + '" data-id=0 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + workCount + '][country]" value="' + $("#dependent_Country").val() + '">' + $('#dependent_Country').val() + '</td> <td contenteditable="true" id="dependent_tableProgram' + count + '" data-id=1 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + workCount + '][program]" value="' + $('#dependent_program').val() + '">' + $('#dependent_program').val() + '</td> <td id="dependent_tableStatus' + count + '" data-id=2 onclick="editCell(this.id)"><input type="hidden" name="travel[' + workCount + '][status]" value="' + $("#dependent_status").val() + '">' + $('#dependent_status').val() + '</td> <td contenteditable="true" id="dependent_tableYear' + count + '" data-id=3 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + workCount + '][year]"  value="' + $("#dependent_year2").val() + '">' + $('#dependent_year2').val() + '</td><td contenteditable="true" id="dependent_tableYearDuration' + count + '" data-id=4 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + workCount + '][duration]"  value="' + $("#dependent_yearDuration").val() + '">' + $('#dependent_yearDuration').val() + '</td> <td contenteditable="true" id="dependent_tableRemark' + count + '" data-id=5 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + workCount + '][remark]"  value="' + $("#dependent_remark1").val() + '">' + $('#dependent_remark1').val() + '</td> <td> <button value="Remove " class="btn btn-danger btn-sm" id="removeRow' + count + '" onclick="deleteRowTravel(this.id)" type="button"> <i class="fa fa-minus" aria-hidden="true"></i> </button> </td> </tr>';
    $('#travelData').append(row);

    workCount++;

    $("#dependent_Country,#dependent_program,#dependent_status,#dependent_year2,#dependent_yearduration,#dependent_remark1").val('');
});

function deleteRowTravel(id) {
    $("." + id).remove();
    if ($('.remove-dependent-travel').length == 0) {

        $('.dependent-travel-table-box').hide();

    }
}
function editCell(id) {
    var innerText = $("#" + id).text();
    var index = $("#" + id).attr('data-id');
    if (index == 2) {
        $("#" + id).empty().append('<select id="' + id + 'Edit" data-id="' + id + '" class="form-control input-sm" value="' + innerText + '" onblur="changeValueOfCell(this.id)" style="width:100%;height:100%;"> <option value="">Select</option> <option>Approved</option> <option>Declined</option> </select>');
    }
    $("#" + id + "Edit").focus();
}

function changeValueOfCell(id) {
    var innerText = $("#" + id).val();
    var cellId = $("#" + id).attr('data-id');
    $("#" + cellId).empty().text(innerText);
}

/*$('#submit-travelHistory').click(function(event) {

    $('#submit-travelHistory').prop('disabled',true);


    $('.help-block').text('');

    event.preventDefault();

    var response = $('#dependentTravelHistoryForm').serialize();
    var dependent_id = $('.inquiryDependentId').val();
    $.ajax({
        type: 'POST',
        url: 'dependent-travel-history-information/' + dependent_id,
        data: response,
        success: function(response) {
            if (response == "success") {
                toastr.success('Added Successfully');
            }
            console.log(response);
        },
        error: function(error) {

            $('#submit-travelHistory').prop('disabled',false);

            console.log(error);

        }

    });

});*/
