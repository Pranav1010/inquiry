var travelCount = $('.remove-dependent-edit-travel').length + 1;
$('#edit_dependent_addTravel').click(function () {

    $('.edit_dependent-travel-table-box').show();

    travelCount++;

    count++;

    var row = '<tr class="remove-dependent-edit-travel removeRow' + count + '" id="edit_travelTable' + count + '"> <td contenteditable="true" id="edit_dependent_tableCountry' + count + '" data-id=0 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + travelCount + '][country]" value="' + $("#edit_dependent_tableCountry").val() + '">' + $('#edit_dependent_tableCountry').val() + '</td> <td contenteditable="true" id="edit_dependent_tableProgram' + count + '" data-id=1 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + travelCount + '][program]" value="' + $('#edit_dependent_program').val() + '">' + $('#edit_dependent_program').val() + '</td> <td id="edit_dependent_tableStatus' + count + '" data-id=2 onclick="editCell(this.id)"><input type="hidden" name="travel[' + travelCount + '][status]" value="' + $("#edit_dependent_status").val() + '">' + $('#edit_dependent_status').val() + '</td> <td contenteditable="true" id="edit_dependent_tableYear' + count + '" data-id=3 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + travelCount + '][year]"  value="' + $("#edit_dependent_year2").val() + '">' + $('#edit_dependent_year2').val() + '</td><td contenteditable="true" id="edit_dependent_tableYearDuration' + count + '" data-id=4 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + travelCount + '][duration]"  value="' + $("#edit_dependent_yearDuration").val() + '">' + $('#edit_dependent_yearDuration').val() + '</td> <td contenteditable="true" id="edit_dependent_tableRemark' + count + '" data-id=5 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + travelCount + '][remark]"  value="' + $("#edit_dependent_remark1").val() + '">' + $('#edit_dependent_remark1').val() + '</td> <td> <button value="Remove " class="btn btn-danger btn-sm" id="removeRow' + count + '" onclick="deleteRowTravelDependent(this.id)" type="button"> <i class="fa fa-minus" aria-hidden="true"></i> </button> </td> </tr>';
    $('#edit-travelData').append(row);


    $("#edit_dependent_tableCountry,#edit_dependent_program,#edit_dependent_status,#edit_dependent_year2,#edit_dependent_yearduration,#edit_dependent_remark1").val('');
});

function deleteRowTravelDependent(id) {
    $("." + id).remove();
    if ($('.remove-dependent-edit-travel').length == 0) {

        $('.dependent-travel-edit-table-box').hide();

    }
}
function editCell(id) {
    var innerText = $("#" + id).text();
    var index = $("#" + id).attr('data-id');
    if (index == 2) {
        $("#" + id).empty().append('<select id="' + id + 'Edit" data-id="' + id + '" class="form-control input-sm" value="' + innerText + '" onblur="changeValueOfCell(this.id)" style="width:100%;height:100%;"> <option value="">Select</option> <option>Approved</option> <option>Declined</option> </select>');
    }
    $("#" + id + "Edit").focus();
}

function changeValueOfCell(id) {
    var innerText = $("#" + id).val();
    var cellId = $("#" + id).attr('data-id');
    $("#" + cellId).empty().text(innerText);
}

/*
$('#edit-travel-dependent').click(function(event) {

    $('.help-block').text('');

    event.preventDefault();

    var response = $('#editDependentTravelHistoryForm').serialize();
    var dependent_id = $('#inquiryDependentUserId').val();
    $.ajax({
        type: 'put',
        url: '../'+dependent_id+'/edit-dependent-travel-information/',
        data: response,
        success: function(response) {
            if (response == "updated") {
                toastr.success('Updated Successfully');
            }
            console.log(response);
        },
        error: function(error) {

            console.log(error);

        }

    });

});*/
