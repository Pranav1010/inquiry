var count = 0;
var k = $('.remove-dependent-academic').length + 1;

$('#editDepedentAcademicAdd').click(function() {
    count++;
    var row = '<tr class="removeRow' + k + '"> <td> <input type="text" placeholder="Other" name="dependent_academic[' + k + '][exam_passed]" class="form-control input-sm" id="dependent_exam_passed' + count + '"> </td> <td> <input type="text" name="dependent_academic[' + k + '][institute]" class="form-control input-sm" id="dependent_institute' + count + '"> </td> <td> <input type="text" name="dependent_academic[' + k + '][year]" class="form-control input-sm" id="dependent_year' + count + '"> </td> <td> <input type="text" name="dependent_academic[' + k + '][board]" class="form-control input-sm" id="dependent_board' + count + '"> </td> <td> <input type="text" name="dependent_academic[' + k + '][result]" class="form-control input-sm" id="dependent_result"> </td><td><button value="Remove " class="btn btn-danger btn-sm" id="removeRow' + k + '" onclick="deleteRowDependentAcademic(this.id)" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button></td></tr>';
    $('#dependent_academic_container').append(row);
    k++;
});

function deleteRowDependentAcademic(id) {
    $("." + id).remove();
}

/*
$('#edit-dependent-academic').click(function(event) {

    $('.help-block').text('');

    event.preventDefault();

    var response = $('#editDependentAcademicForm').serialize();
    var dependent_id = $('#inquiryDependentUserId').val();
    $.ajax({
        type: 'put',
        url: '../'+dependent_id+'/edit-dependent-academic-information/',
        data: response,
        success: function(response) {
            if (response == "updated") {
                toastr.success('Updated Successfully');
            }
            console.log(response);
        },
        error: function(error) {

            console.log(error);

        }

    });

});*/
