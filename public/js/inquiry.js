$('.inquiryDetailSubmit').click(function (event) {

    $('.help-block').text('');

    event.preventDefault();

    var response = $('#inquiryDetailsForm').serialize();


    $.ajax({
        type: 'POST',
        url: 'inquiry-details',
        data: response,
        success: function (response) {

            if (response.result === "success") {
                toastr.success('Added Successfully');

                // window.location = '/inquiries/'+ response.inquiry_id +'/edit'
                $('.inquiryUserId').val(response.inquiry_id);
                $('#personalInfo').prop('disabled', true);
                $('#mainTabs').show();
                $("#program").select2({placeholder: "Select Visa Type"});
                $("#status").select2({placeholder: "Select Visa Status"});
            }
        },
        error: function (error) {

            error = error.responseJSON;

            console.log(error);

            $.each(error, function (key, value) {
                $('#personalInfo').prop('disabled', false);
                $('.' + key).text(value);
            });
        }
    });
});

/*$('.inquiryDependentDetailSubmit').click(function(event) {


    $('.help-block').text('');

    event.preventDefault();

    var response = $('#inquiryDependentDetailsForm').serialize();

    console.log($('#inquiryDependentDetailsForm'));

    $.ajax({
        type: 'POST',
        url: 'dependent-information',
        data: response,
        success: function(response) {
            if (response == "success") {
                toastr.success('Added Successfully');
            }
            console.log(response);
        },
        error: function(error) {

            $('#submit-dependent-exam').prop('disabled',false);


            console.log(error);

        }

    });

});*/

$("#exam-tab").on("click", function(){
    setTimeout(function(){
        $(".minimal").select2({placeholder: "Select"});
    }, 100);
});

$("#dependentExamTab").on("click", function(){
    setTimeout(function(){
        $(".minimal1").select2({placeholder: "Select"});
    }, 100);
});

$("#travel-tab").on("click", function(){
    setTimeout(function(){
        $(".visa-type").select2({placeholder: "Select Visa Type"});
        $(".visa-status").select2({placeholder: "Select Visa Status"});
    }, 100);
});

$("#dependentTravelTab").on("click", function(){
    setTimeout(function(){
        $(".visa-type1").select2({placeholder: "Select Visa Type"});
        $(".visa-status1").select2({placeholder: "Select Visa Status"});
    }, 100);
});