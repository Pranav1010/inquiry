$(document).ready(function () {
    $(".grid-row-delete").addClass("btn");
    $(".fa.fa-edit").removeClass("fa-edit").addClass("fa-pencil").parent().addClass("btn grid-row-edit");
    $('.datepicker').datepicker({ format: 'dd-mm-yyyy' });
});

$(document).on('pjax:end', function () {
    $(".grid-row-delete").addClass("btn");
    $(".fa.fa-edit").removeClass("fa-edit").addClass("fa-pencil").parent().addClass("btn grid-row-edit");
    $('.datepicker').datepicker({ format: 'dd-mm-yyyy' });
});





