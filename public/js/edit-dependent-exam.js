$('#edit_dependent_check1').change(function() {
    $("#edit_dependent_listening1").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_reading1").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_writing1").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_speaking1").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_overall1").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_examYear").prop("disabled", !$(this).is(':checked'));

});
$('#edit_dependent_check2').change(function() {
    $("#edit_dependent_listening2").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_reading2").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_writing2").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_speaking2").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_overall2").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_examYear2").prop("disabled", !$(this).is(':checked'));

})
$('#edit_dependent_check3').change(function() {
    $("#edit_dependent_listening3").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_reading3").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_writing3").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_overall3").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_examYear3").prop("disabled", !$(this).is(':checked'));

});
$('#edit_dependent_check4').change(function() {
    $("#edit_dependent_overall4").prop("disabled", !$(this).is(':checked'));
    $("#edit_dependent_examYear4").prop("disabled", !$(this).is(':checked'));

});

/*
$('#edit-dependent-exam').click(function(event) {

    $('.help-block').text('');

    event.preventDefault();

    var response = $('#editDependentExamInformation').serialize();
    var dependent_id = $('.inquiryDependentUserId').val();
    $.ajax({
        type: 'put',
        url: '../' + dependent_id + '/edit-dependent-exam-information/',
        data: response,
        success: function(response) {
            if (response == "updated") {
                toastr.success('Updated Successfully');
            }
            console.log(response);
        },
        error: function(error) {

            console.log(error);

        }

    });

});*/
