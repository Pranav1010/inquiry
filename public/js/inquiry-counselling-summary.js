$('#submit-counselling').click(function (event) {

    $('#submit-counselling').prop('disabled',true);


    $('.help-block').text('');

    event.preventDefault();

    var response = $('#counsellingSummaryForm').serialize();
    var inquiry_id = $('#inquiryId').val();


    $.ajax({
        type:'POST',
        url:'counselling-summary-information/'+ inquiry_id,
        data: response,
        success:function (response) {
            if(response == "success"){
                
                toastr.options.onShown = function () {
                    window.location = '/inquiries';

                }
                toastr.success('Added Successfully');

            }
            console.log(response);
        },
        error:function (error) {

            console.log(error);
            $('#submit-counselling').prop('disabled',false);


        }

    });

});


