function addRowCountry(tableID) {
    var count = $('.remove-program').length + 1;
    count++;

    $('.preference-country-box').show();
    var row = '<tr class="remove-program removeRow' + count + '"> <td id="tablePreferedProgram' + count + '" data-id=1 ondblclick="editCell(this.id)"><input type="hidden"  name="program[]" value="' + $('#inquiryProgram option:selected').val() + ' ">' + $('#inquiryProgram option:selected').text() + '</td> <td id="tablePreferedCountry' + count + '" data-id=0 ondblclick="editCell(this.id)"><input type="hidden"  name="country[]" value="' + $('#inquiryCountry option:selected').val() + ' ">' + $('#inquiryCountry option:selected').text() + '</td><td><button value="Remove " class="btn btn-danger btn-sm hide1" id="removeRow' + count + '" onclick="deleteRowCountry(this.id)" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button></td></tr>';
    $('#preferedCountryList').append(row);
}

function deleteRowCountry(id) {
    $("." + id).remove();
    if ($('.remove-program').length == 0) {

        $('.preference-country-box').hide();

    }
}
//collapse button js
$("#div_expand").click(function () {
    $('.personal-info.box-body').toggle();
    $(this).find('i').toggleClass('fa-minus fa-plus')
});

// function for hide show dependent tab
function show1() {
    document.getElementById('dependent').style.display = 'block';
    $('.dependent-detail').show();

}

function show2() {
    document.getElementById('dependent').style.display = 'none';
    $('.dependent-detail').hide();
}

//tabs js
$(document).ready(function () {
    jQuery('.tabs .tab-links a').on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
});

$(document).ready(function () {

    //display reasonbox on user changed
    displayReasonBox = function () {
        var selectedUserId = $("#userData").val();
        var assignBox = $("#assigning-reason");
        var userId = assignBox.data("id");

        if (selectedUserId != userId) {
            assignBox.show();
        } else {
            assignBox.hide();
        }
    };

    displayReasonBox();

    $("#city").select2({placeholder : "Select City"});
    $("#area").select2({placeholder : "Select Area"});
    $("#inquiryCountry").select2({placeholder : "Select Country"});
    $("#inquiryProgram").select2({placeholder : "Select Program"});
    $("#inquirySource").select2({placeholder : "Select Source"});
    $("#inquiryBranch").select2({placeholder : "Select Branch"});
    $("#userData").select2({placeholder : "Select User"});

    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_flat-blue'
    });

    //display box on source changed

    displayOtherBox = function () {
        var selectedSource = $("#inquiry-source option:selected").text();
        var otherBox = $("#otherBox");
        var friendBox = $("#friendBox");
        var tagbox = $("#tagbox");

        if (selectedSource == "Other") {
            friendBox.hide();
            tagbox.hide();
            otherBox.show();
        } else if (selectedSource == "Friend") {
            otherBox.hide();
            friendBox.show();
            tagbox.show()
        } else {
            otherBox.hide();
            friendBox.hide();
            tagbox.hide();
        }
    };

    displayOtherBox();

});
//city wise area js
$('#editCity').on('change', function (e) {
    var city_id = e.target.value;

    //ajax
    $.get('../ajax-area/' + city_id, function (data) {
        $('#editArea').empty();
        $.each(data, function (index, area) {
            $('#editArea').append('<option value="' + area.id + '">' + area.name + '</option>')
        });
    });
});

// //program wise country js
// $('#inquiryProgram').on('change', function (e) {
//     var program_id = e.target.value;
//
//     // ajax
//     $.get('ajax-country/' + program_id, function (data) {
//         $('#inquiryCountry').empty();
//         $.each(data, function (index, value) {
//             $('#inquiryCountry').append('<option value="' + value.country_id + '">' + value['country']['name'] + '</option>')
//         });
//     });
// });
$('#edit-personalInfo').on('click', function (event) {
    $('.help-block').text('');

    event.preventDefault();

    var response = $('#edit-personalInfoForm').serialize();
    var inquiry_id = $('#inquiryFormId').val();
    $.ajax({
        type:'put',
        url:'edit-personal-information/',
        data: response,
        success:function (response) {
            if(response.result == "updated"){
                toastr.success('Updated Successfully');
                $('.inquiryUserId').val(response.inquiry_id);
            }
        },
        error:function (error) {


            error = error.responseJSON;
            // console.log(error);

            $.each(error, function (key, value) {

                console.log(value);

                $('.' + key).text(value);//TODO: error message not seen

            });


        }

    });

});


$("#div_expand_bottom_edit").click(function () {
    $('.personal-info.box-body').toggle();
    $("#div_expand").find('i').toggleClass('fa-minus fa-plus');
});