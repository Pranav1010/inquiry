var countryObject = {};

function addRowCountry(tableID, e) {

    e.preventDefault();

    var inquiryCountry = $('#inquiryCountry');
    var inquiryProgram = $('#inquiryProgram');

    if (inquiryCountry.val() === "") {

        toastr.error('Select any country first');
        return;

    } else if (inquiryProgram.val() === "") {

        toastr.error('Select any program first');
        return;

    } else if (countryObject[inquiryCountry.val() + "-" + inquiryProgram.val()] && countryObject[inquiryCountry.val() + "-" + inquiryProgram.val()] === inquiryCountry.find(":selected").text()) {

        toastr.error('This country - program pair is already exists');
        return;

    }

    countryObject[inquiryCountry.val() + "-" + inquiryProgram.val()] = inquiryCountry.find(":selected").text();

    count++;
    var row = '<tr id="removeProgram' + count + '" class="remove-program removeRow' + count + '"> <td id="tablePreferedCountry' + count + '" data-id=0 ondblclick="editCell(this.id)"><input type="hidden"  name="country[]" value="' + inquiryCountry.val() + ' ">' + inquiryCountry.find(":selected").text() + '</td> <td id="tablePreferedProgram' + count + '" data-id=1 ondblclick="editCell(this.id)"><input type="hidden"  name="program[]" value="' + inquiryProgram.val() + ' ">' + inquiryProgram.find(":selected").text() + '</td> <td><button value="Remove " class="btn btn-danger btn-sm hide1" id="removeRow' + count + '" onclick="deleteRowCountry(this.id)" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button></td></tr>';
    $('#preferredCountryList').append(row);

    $('.preference-country-box').show();
}

function deleteRowCountry(id) {
    $("." + id).remove();

    var inquiryCountry = $('#inquiryCountry');
    var inquiryProgram = $('#inquiryProgram');

    delete countryObject[inquiryCountry.val() + "-" + inquiryProgram.val()];

    if ($('.remove-program').length === 0) {

        $('.preference-country-box').hide();

    }
}

//collapse button js
$("#div_expand").click(function () {
    $('.personal-info.box-body').toggle();
    $(this).find('i').toggleClass('fa-minus fa-plus')
});

// function for hide show dependent tab
function show1() {
    $('#dependent').css("display", "block");
    $('.dependent-detail').show();

}

function show2() {
    $('#dependent').css("display", "none");
    $('.dependent-detail').hide();
}

$(document).ready(function () {

    jQuery('.tabs .tab-links a').on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });

    if ($("#preferredCountryList tr").length === 0) {
        $(".preference-country-box").hide();
    }

    $("#city").select2({placeholder: "Select City"});
    $("#area").select2({placeholder: "Select Area"});
    $("#inquiryCountry").select2({placeholder: "Select Country"});
    $("#inquiryProgram").select2({placeholder: "Select Program"});
    $("#inquirySource").select2({placeholder: "Select Source"});
    $("#inquiryBranch").select2({placeholder: "Select Branch"});
    $("#userData").select2({placeholder: "Select User"});

    $('input[type="radio"]').iCheck({
        radioClass: 'iradio_flat-blue'
    });

    //display reasonbox on user changed
    displayReasonBox = function () {
        var selectedUserId = $("#userData").val();
        var assignBox = $("#assigning-reason");
        var userId = assignBox.data("id");

        if (selectedUserId != userId) {
            assignBox.show();
        } else {
            assignBox.hide();
        }
    };

    displayReasonBox();

    //display box on source changed

    displayOtherBox = function () {
        var selectedSource = $("#inquiry-source option:selected").text();
        var otherBox = $("#otherBox");
        var friendBox = $("#friendBox");
        var tagbox = $("#tagbox");

        if (selectedSource == "Other") {
            friendBox.hide();
            tagbox.hide();
            otherBox.show();
        } else if (selectedSource == "Walk-in") {
            otherBox.hide();
            friendBox.show();
            tagbox.show()
        } else {
            otherBox.hide();
            friendBox.hide();
            tagbox.hide();
        }
    };

    displayOtherBox();

});

//city wise area js
$('#city').on('change', function (e) {
    var city_id = e.target.value;

    //ajax
    $.get('ajax-area/' + city_id, function (data) {
        $('#area').empty();
        $.each(data, function (index, area) {
            $('#area').append('<option value="' + area.id + '">' + area.name + '</option>')
        });
    });
});


$('#personalInfo').click(function (event) {

    $('.help-block').text('');

    event.preventDefault();

    var response = $('#personalInfoForm').serialize();

    $.ajax({
        type: 'POST',
        url: 'personal-information/',
        data: response,
        success: function (response) {

            if (response.result === "success") {
                toastr.success('Added Successfully');

                // window.location = '/inquiries/'+ response.inquiry_id +'/edit'
                $('.inquiryUserId').val(response.inquiry_id);
                $('#personalInfo').prop('disabled', true);
                $('#mainTabs').show();
                $("#program").select2({placeholder: "Select Visa Type"});
                $("#status").select2({placeholder: "Select Visa Status"});
            }
        },
        error: function (error) {

            error = error.responseJSON;

            console.log(error);

            $.each(error, function (key, value) {
                $('#personalInfo').prop('disabled', false);
                $('.' + key).text(value);
            });
        }
    });
});

$("#div_expand_bottom").click(function () {
    $('.personal-info.box-body').toggle();
    $("#div_expand").find('i').toggleClass('fa-minus fa-plus');
});