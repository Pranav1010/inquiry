var n = $('.child-info').length + 1;
var children = 0;
var dependentChildrens = 0;
var counter = 0;
var title = $("#child-info-title");
var timeout = null;

if($("#childCount").val() > 0){
    title.show();
}

$('#children').on('input', function () {

    children = $(this).val();
    clearTimeout(timeout);

    timeout = setTimeout(function(){

        if(children === 0){
            title.hide();
        } else if (children > 10){
            toastr.warning('You cannot add more than 10 children.');
            return;
        } else {
            title.show();
        }

        var totalChildrens = children - dependentChildrens;

        if(totalChildrens > 0){
            for(var i=0; i < totalChildrens; i++){
                var div = '<div class="removeRow' + counter + '"><div class="row"><div class="col-md-8"><div class="form-group"><label>Name</label><input type="text" class="form-control" name="child[' + counter + '][child_name]"></div></div><div class="col-md-4"><div class="form-group"><label>Birth Date</label><div class="input-group date"><input type="text" class="form-control datepicker" onchange="calculateAge('+counter+', this)" name="child[' + counter + '][birth_date]"><div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div></div></div></div></div><div class="row"><div class="col-md-4"><div class="form-group"><label>Age</label><div class="row"><div class="col-md-6"><input type="number" name="child[' + counter + '][age_yr]" placeholder="Year" id="age_year'+counter+'" class="form-control input-sm" min=0></div><div class="col-md-6"><input type="number" name="child[' + counter + '][age_mnth]" id="age_month'+counter+'" placeholder="Month" class="form-control input-sm" min="0"></div></div></div></div><div class="col-md-8"><div class="form-group pull-right" style="margin-top: 20px;"><button value="Remove" class="btn btn-danger btn-sm pull-right" id="removeRow' + counter + '" onclick="deleteRowChild(this.id)" type="button"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div></div></div>';
                dependentChildrens++;
                counter++;
                $('.child-info').prepend(div);
            }

            $('.datepicker').datepicker({ format: 'dd-mm-yyyy' });
        } else{
            var removeChildren = dependentChildrens-children;
            for(var j = 0; j < removeChildren; j++){
                $(".removeRow" + (dependentChildrens-1)).remove();
                dependentChildrens--;
            }
        }

        if(dependentChildrens === 0){
            title.hide();
        }
    }, 500);
});

function calculateAge(count, element) {

    var diffTime = (moment()).diff(moment($(element).val(), "DD/MM/YYYY"));
    var duration = moment.duration(diffTime);
    var years = duration.years(),
        months = duration.months();

    $("#age_year"+count).val(years);
    $("#age_month"+count).val(months);
}

function deleteRowChild(id) {
    $("." + id).remove();
    $('#children').val(dependentChildrens-1);
    dependentChildrens--;

    if(dependentChildrens === 0){
        title.hide();
    }
}

/*
$('#submit-dependent-detail').click(function (event) {

    $('#submit-dependent-detail').prop('disabled', true);
    $('.help-block').text('');
    event.preventDefault();
    var response = $('#dependentDetailForm').serialize();
    var inquiry_id = $('.inquiryUserId').val();

    $.ajax({
        type: 'POST',
        url: 'dependent-information/' + inquiry_id,
        data: response,
        success: function (response) {
            if (response.result === "success") {
                $('#dependentTabs').show();
                $(".noDataDiv").hide();
                toastr.success('Added Successfully');
                $('.inquiryDependentId').val(response.dependent_id);
            }
        },
        error: function (error) {
            error = error.responseJSON;
            $.each(error, function (key, value) {
                $('#submit-dependent-detail').prop('disabled', false);
                console.log(key, value);
                $('.' + key).text(value);
            });
        }
    });

});*/
