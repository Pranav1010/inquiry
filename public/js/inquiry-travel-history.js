var i = 0;
$('#addTravelBox').click(function() {
    $('.travel-box').show();

    count++;

    var row = '<tr class="remove-travel removeRow' + count + '" id="travelTable' + count + '"> <td contenteditable="true" id="tableCountry' + count + '" data-id=0 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + i + '][country]" value="' + $("#Country").val() + '">' + $('#Country').val() + '</td><td contenteditable="true" id="tableProgram' + count + '" data-id=1 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + i + '][program]" value="' + $("#program").val() + '">' + $('#program').val() + '</td><td id="tableStatus' + count + '" data-id=2 onclick="editCell(this.id)"><input type="hidden" name="travel[' + i + '][status]" value="' + $("#status").val() + '">' + $('#status').val() + '</td> <td contenteditable="true" id="tableYear' + count + '" data-id=3 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + i + '][year]"  value="' + $("#year2").val() + '"></td> <td contenteditable="true" id="tableYearDuration' + count + '" data-id=4 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + i + '][duration]"  value="' + $("#yearDuration").val() + '">' + $('#year2').val() + '</td> <td contenteditable="true" id="tableRemark' + count + '" data-id=5 ondblclick="editCell(this.id)"><input type="hidden" name="travel[' + i + '][remark]"  value="' + $("#remark1").val() + '">' + $('#remark1').val() + '</td> <td> <button value="Remove " class="btn btn-danger btn-sm" id="removeRow' + count + '" onclick="deleteRowTravelHistory(this.id)" type="button"> <i class="fa fa-minus" aria-hidden="true"></i> </button> </td> </tr>';
    $('#travelHistory').append(row);

    $("#Country,#program,#status,#year2,#yearDuration,#remark1").val('');

    i++;
});

function deleteRowTravelHistory(id) {
    $("." + id).remove();
    if ($('.remove-travel').length == 0) {

        $('.travel-box').hide();

    }
}

function editCell(id) { // TODO: make this function for dependent travel box also
    var innerText = $("#" + id).text();
    var index = $("#" + id).attr('data-id');
    if (index == 2) {
        $("#" + id).empty().append('<select id="' + id + 'Edit" data-id="' + id + '" class="form-control input-sm" value="' + innerText + '" onblur="changeValueOfCell(this.id)" style="width:100%;height:100%;"> <option value="">Select</option> <option>Approved</option> <option>Declined</option> </select>');
    }
    $("#" + id + "Edit").focus();
}

function changeValueOfCell(id) {
    var innerText = $("#" + id).val();
    var cellId = $("#" + id).attr('data-id');
    $("#" + cellId).empty().text(innerText);
}

/*
$('#submit-travel-history').click(function (event) {

    $('#submit-travel-history').prop('disabled',true);


    $('.help-block').text('');

    event.preventDefault();

    var response = $('#travelHistoryForm').serialize();
    var inquiry_id = $('.inquiryUserId').val();

    $.ajax({
        type: 'POST',
        url: 'travel-history-information/' + inquiry_id,
        data: response,
        success: function (response) {
            if (response == "success") {
                toastr.success('Added Successfully');
            }
            console.log(response);
        },
        error: function (error) {

            console.log(error);
            $('#submit-travel-history').prop('disabled',false);


        }

    });
});*/
