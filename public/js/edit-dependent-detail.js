var children = 0;
var dependentChildrens = 0;
var title = $("#child-info-title");
var timeout = null;

$('#children').on('input', function () {

    children = $(this).val();
    clearTimeout(timeout);

    timeout = setTimeout(function(){

        if(children === 0){
            title.hide();
        } else if (children > 10){
            toastr.warning('You cannot add more than 10 children.');
            return;
        } else {
            title.show();
        }

        var totalChildrens = children - dependentChildrens;

        if(totalChildrens > 0){
            for(var i=0; i < totalChildrens; i++){
                var div = '<div class="removeRow' + dependentChildrens + '"><div class="row"><div class="col-md-8"><div class="form-group"><label>Name</label><input type="text" class="form-control" name="child[' + dependentChildrens + '][child_name]"></div></div><div class="col-md-4"><div class="form-group"><label>Birth Date</label><div class="input-group date"><input type="text" class="form-control datepicker"  name="child[' + dependentChildrens + '][birth_date]"><div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div></div></div></div></div><div class="row"><div class="col-md-4"><div class="form-group"><label>Age</label><div class="row"><div class="col-md-6"><input type="number" name="child[' + dependentChildrens + '][age_yr]" placeholder="Year" class="form-control input-sm" min=0></div><div class="col-md-6"><input type="number" name="child[' + dependentChildrens + '][age_mnth]" placeholder="Month" class="form-control input-sm" min="0"></div></div></div></div><div class="col-md-8"><div class="form-group pull-right" style="margin-top: 20px;"><button value="Remove" class="btn btn-danger btn-sm pull-right" id="removeRow' + dependentChildrens + '" onclick="deleteRowChild(this.id)" type="button"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div></div></div>';
                dependentChildrens++;
                $('.child-info').prepend(div);
            }

            $('.datepicker').datepicker({ format: 'mm-dd-yyyy' });
        } else{
            var removeChildren = dependentChildrens-children;
            for(var j = 0; j < removeChildren; j++){
                $(".removeRow" + (dependentChildrens-1)).remove();
                dependentChildrens--;
            }
        }

        if(dependentChildrens === 0){
            title.hide();
        }
    }, 500);
});

function deleteRowChild(id) {
    $("." + id).remove();
    $('#children').val(dependentChildrens-1);
    dependentChildrens--;

    if(dependentChildrens === 0){
        title.hide();
    }
}

$('#submit-dependent-detail').click(function (event) {

    $('#submit-dependent-detail').prop('disabled', true);
    $('.help-block').text('');

    event.preventDefault();

    var response = $('#editDependentDetailForm').serialize();
    var inquiry_id = $('#inquiryFormId').val();

    $.ajax({
        type: 'put',
        url: '../' + inquiry_id + '/edit-dependent-detail/',
        data: response,
        success: function (response) {
            if (response === "updated") {
                toastr.success('Updated Successfully');
            }
            console.log(response);
        },
        error: function (error) {
            console.log(error);
        }
    });
});


