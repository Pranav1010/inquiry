<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryWorkExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_work_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inquiry_id');
            $table->string('company')->nullable();
            $table->string('designation')->nullable();
            $table->string('durationYears')->nullable();
            $table->string('durationMonths')->nullable();
            $table->string('location')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_work_experiences');
    }
}
