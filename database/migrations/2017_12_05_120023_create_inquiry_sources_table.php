<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquirySourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $sources = array(
            ['name' => 'Newspaper'],
            ['name' => 'Telephonic'],
            ['name' => 'Walk-in'],
            ['name' => 'Friend'],
            ['name' => 'TV'],
            ['name' => 'Website'],
            ['name' => 'E-mail'],
            ['name' => 'Other'],
        );

        DB::table('inquiry_sources')->insert($sources);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_sources');
    }
}
