<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryDependentWorkExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_dependent_work_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dependent_id');
            $table->string('company')->nullable();
            $table->string('designation')->nullable();
            $table->string('duration')->nullable();
            $table->string('location')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_dependent_work_experiences');
    }
}
