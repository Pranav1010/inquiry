<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\City::truncate();

        \App\City::insert([

            [
                'name' => 'Adalaj',
               
            ],
            [
                'name' => 'Adityana',
               
            ],
            [
                'name' => 'Ahmedabad',
               
            ],
            [
                'name' => 'Alang',
               
            ],
            [
                'name' => 'Ambaji',
               
            ],
            [
                'name' => 'Ambaliyasan',
               
            ],
            [
                'name' => 'Amreli',
               
            ],
            [
                'name' => 'Anand',
               
            ],
            [
                'name' => 'Andada',
               
            ],
            [
                'name' => 'Anjar',
               
            ],
            [
                'name' => 'Anklav',
               
            ],
            [
                'name' => 'Anklesvar',
               
            ],
            [
                'name' => 'Antaliya',
               
            ],
            [
                'name' => 'Arambhada',
               
            ],
            [
                'name' => 'Atul',
               
            ],
            [
                'name' => 'Bagasara',
               
            ],
            [
                'name' => 'Balasinor',
               
            ],
            [
                'name' => 'Bantwa',
               
            ],
            [
                'name' => 'Bardoli',
               
            ],
            [
                'name' => 'Bavla',
               
            ],
            [
                'name' => 'Bhachau',
               
            ],
            [
                'name' => 'Bhanvad',
               
            ],

            [
                'name' => 'Bharuch',
               
            ],
            [
                'name' => 'Bhavnagar',
               
            ],
            [
                'name' => 'Bhayavadar',
               
            ],
            [
                'name' => 'Bhuj',
               
            ],
            [
                'name' => 'Bilimora',
               
            ],
            [
                'name' => 'Bodeli',
               
            ],
            [
                'name' => 'Boriavi',
               
            ],
            [
                'name' => 'Borsad',
               
            ],
            [
                'name' => 'Botad',
               
            ],
            [
                'name' => 'Chaklasi',
               
            ],
            [
                'name' => 'Chala',
               
            ],
            [
                'name' => 'Chalala',
               
            ],
            [
                'name' => 'Chalthan',
               
            ],
            [
                'name' => 'Chanasma',
               
            ],
            [
                'name' => 'Chanod',
               
            ],
            [
                'name' => 'Chhatral',
               
            ],
            [
                'name' => 'Chhota',
               
            ],
            [
                'name' => 'Chikhli',
               
            ],
            [
                'name' => 'Chiloda',
               
            ],
            [
                'name' => 'Chorvad',
               
            ],
            [
                'name' => 'Dabhoi',
               
            ],
            [
                'name' => 'Dakor',
               
            ],
            [
                'name' => 'Damnagar',
               
            ],
            [
                'name' => 'Deesa',
               
            ],
            [
                'name' => 'Dehgam',
               
            ],
            [
                'name' => 'Devgadbaria',
               
            ],
            [
                'name' => 'Devsar',
               
            ],
            [
                'name' => 'Dhandhuka',
               
            ],
            [
                'name' => 'Dhanera',
               
            ],
            [
                'name' => 'Dharampur',
               
            ],
            [
                'name' => 'Dhola',
               
            ],
            [
                'name' => 'Dholka',
               
            ],
            [
                'name' => 'Dhoraji',
               
            ],
            [
                'name' => 'Dhrangadhra',
               
            ],
            [
                'name' => 'Dhrol',
               
            ],
            [
                'name' => 'Digvijaygram',
               
            ],
            [
                'name' => 'Dohad',
               
            ],
            [
                'name' => 'Dungra',
               
            ],
            [
                'name' => 'Dwarka',
               
            ],
            [
                'name' => 'Gadhada',
               
            ],
            [
                'name' => 'Gandevi',
               
            ],
            [
                'name' => 'Gandhidham',
               
            ],
            [
                'name' => 'Gandhinagar',
               
            ],
            [
                'name' => 'Gariadhar',
               
            ],
            [
                'name' => 'Ghogha',
               
            ],
            [
                'name' => 'Godhra',
               
            ],
            [
                'name' => 'Gondal',
               
            ],
            [
                'name' => 'Hajira',
               
            ],
            [
                'name' => 'Halol',
               
            ],
            [
                'name' => 'Halvad',
               
            ],
            [
                'name' => 'Harij',
               
            ],
            [
                'name' => 'Himatnagar',
               
            ],
            [
                'name' => 'Idar',
               
            ],
            [
                'name' => 'Jafrabad',
               
            ],
            [
                'name' => 'Jambusar',
               
            ],
            [
                'name' => 'Jamjodhpur',
               
            ],
            [
                'name' => 'Jamnagar',
               
            ],
            [
                'name' => 'Jasdan',
               
            ],
            [
                'name' => 'Jetpur',
               
            ],
            [
                'name' => 'Jhalod',
               
            ],
            [
                'name' => 'Junagadh',
               
            ],
            [
                'name' => 'Kadi',
               
            ],
            [
                'name' => 'Kadodara',
               
            ],
            [
                'name' => 'Kalavad',
               
            ],
            [
                'name' => 'Kalol',
               
            ],

            [
                'name' => 'Kandla',
               
            ],
            [
                'name' => 'Kanodar',
               
            ],
            [
                'name' => 'Kapadvanj',
               
            ],
            [
                'name' => 'Karjan',
               
            ],
            [
                'name' => 'Katpar',
               
            ],
            [
                'name' => 'Keshod',
               
            ],
            [
                'name' => 'Kevadiya',
               
            ],
            [
                'name' => 'Khambhalia',
               
            ],
            [
                'name' => 'Khambhat',
               
            ],
            [
                'name' => 'Kharaghoda',
               
            ],
            [
                'name' => 'Kheda',
               
            ],
            [
                'name' => 'Khedbrahma',
               
            ],
            [
                'name' => 'Kheralu',
               
            ],
            [
                'name' => 'Kodinar',
               
            ],
            [
                'name' => 'Kosamba',
               
            ],
            [
                'name' => 'Kutiyana',
               
            ],
            [
                'name' => 'Lathi',
               
            ],
            [
                'name' => 'Limbdi',
               
            ],
            [
                'name' => 'Limla',
               
            ],
            [
                'name' => 'Lunawada',
               
            ],
            [
                'name' => 'Mahesana',
               
            ],
            [
                'name' => 'Mahudha',
               
            ],
            [
                'name' => 'Mahuva',
               
            ],
            [
                'name' => 'Mahuvar',
               
            ],
            [
                'name' => 'Malpur',
               
            ],
            [
                'name' => 'Manavadar',
               
            ],
            [
                'name' => 'Mandvi',
               
            ],
            [
                'name' => 'Mangrol',
               
            ],
            [
                'name' => 'Mansa',
               
            ],
            [
                'name' => 'Meghraj',
               
            ],
            [
                'name' => 'Mehmedabad',
               
            ],
            [
                'name' => 'Mithapur',
               
            ],
            [
                'name' => 'Modasa',
               
            ],
            [
                'name' => 'Morvi',
               
            ],
            [
                'name' => 'Mundra',
               
            ],
            [
                'name' => 'Nadiad',
               
            ],
            [
                'name' => 'Nandej',
               
            ],
            [
                'name' => 'Navsari',
                
            ],
            [
                'name' => 'Ode',
               
            ],
            [
                'name' => 'Okha',
               
            ],
            [
                'name' => 'Paddhari',
               
            ],
            [
                'name' => 'Padra',
               
            ],
            [
                'name' => 'Palanpur',
               
            ],
            [
                'name' => 'Palej',
               
            ],
            [
                'name' => 'Palitana',
               
            ],
            [
                'name' => 'Pardi',
               
            ],
            [
                'name' => 'Parnera',
               
            ],
            [
                'name' => 'Patan',
               
            ],
            [
                'name' => 'Petlad',
               
            ],
            [
                'name' => 'Porbandar',
               
            ],
            [
                'name' => 'Prantij',
               
            ],
            [
                'name' => 'Radhanpur',
               
            ],
            [
                'name' => 'Rajkot',
               
            ],
            [
                'name' => 'Rajpipla',
               
            ],
            [
                'name' => 'Rajula',
               
            ],
            [
                'name' => 'Ranavav',
               
            ],
            [
                'name' => 'Rapar',
               
            ],
            [
                'name' => 'Salaya',
               
            ],
            [
                'name' => 'Sanand',
               
            ],
            [
                'name' => 'Santrampur',
               
            ],
            [
                'name' => 'Sarigam',
               
            ],
            [
                'name' => 'Savarkundla',
               
            ],
            [
                'name' => 'Sayan',
               
            ],
            [
                'name' => 'Sidhpur',
               
            ],
            [
                'name' => 'Sihor',
               
            ],
            [
                'name' => 'Sikka',
               
            ],
            [
                'name' => 'Songadh',
               
            ],
            [
                'name' => 'Surajkaradi',
               
            ],
            [
                'name' => 'Surat',
               
            ],
            [
                'name' => 'Talaja',
               
            ],
            [
                'name' => 'Talod',
               
            ],
            [
                'name' => 'Thangadh',
               
            ],
            [
                'name' => 'Tharad',
               
            ],
            [
                'name' => 'Ukai',
               
            ],
            [
                'name' => 'Umbergaon',
               
            ],

            [
                'name' => 'Umreth',
               
            ],
            [
                'name' => 'Una',
               
            ],
            [
                'name' => 'Unjha',
               
            ],
            [
                'name' => 'Upleta',
               
            ],
            [
                'name' => 'Vadia',
               
            ],
            [
                'name' => 'Vadnagar',
               
            ],
            [
                'name' => 'Vadodara',
               
            ],
            [
                'name' => 'Vaghodia',
               
            ],
            [
                'name' => 'Valsad',
               
            ],

            [
                'name' => 'Vanthali',
               
            ],

            [
                'name' => 'Vapi',
               
            ],
            [
                'name' => 'Vartej',
               
            ],
            [
                'name' => 'Vasna',
               
            ],
            [
                'name' => 'Veraval',
               
            ],
            [
                'name' => 'Vijapur',
               
            ],
            [
                'name' => 'Viramgam',
               
            ],
            [
                'name' => 'Visavadar',
               
            ],
            [
                'name' => 'Visnagar',
               
            ],
            [
                'name' => 'Vyara',
               
            ],
            [
                'name' => 'Wadhwan',
               
            ],
            [
                'name' => 'Wankaner',
               
            ]

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
