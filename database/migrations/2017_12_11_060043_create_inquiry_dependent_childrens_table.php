<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryDependentChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_dependent_childrens', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dependent_id');
            $table->string('name')->nullable();
            $table->integer('age_in_year')->nullable();
            $table->integer('age_in_month')->nullable();
            $table->date('birth_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_dependent_childrens');
    }
}
