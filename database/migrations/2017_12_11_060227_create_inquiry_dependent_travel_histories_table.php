<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryDependentTravelHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_dependent_travel_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dependent_id');
            $table->string('country')->nullable();
            $table->string('program')->nullable();
            $table->string('status')->nullable();
            $table->string('year')->nullable();
            $table->string('duration')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_dependent_travel_histories');
    }
}
