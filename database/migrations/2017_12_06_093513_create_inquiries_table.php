<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('course_id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('mobile');
            $table->string('city');
            $table->string('area')->nullable();
            $table->string('pincode')->nullable();
            $table->string('country')->nullable();
            $table->string('source');
            $table->text('remarks')->nullable();
            $table->text('reason')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiries');
    }
}
