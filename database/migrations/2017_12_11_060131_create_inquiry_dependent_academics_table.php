<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryDependentAcademicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_dependent_academics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dependent_id');
            $table->string('exam_passed')->nullable();
            $table->string('institute')->nullable();
            $table->string('year')->nullable();
            $table->string('board')->nullable();
            $table->string('result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_dependent_academics');
    }
}
