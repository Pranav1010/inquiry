<?php

use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Permission;
use Illuminate\Database\Seeder;

class AdminTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create a user.
        Administrator::truncate();
        Administrator::create([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'name'     => 'Administrator',
        ]);

        // create a role.
        Role::truncate();
        Role::create(
            [
                'name' => 'Administrator',
                'slug' => 'administrator',
            ],
            [
                'name' => 'Guest',
                'slug' => 'guest',
            ],
            [
                'name' => 'Counselor',
                'slug' => 'counselor',
            ],
            [
                'name' => 'Telecaller',
                'slug' => 'telecaller',
            ],
            [
                'name' => 'Receptionist',
                'slug' => 'receptionist',
            ]
        );

        // add role to user.
        Administrator::first()->roles()->save(Role::first());

        //create a permission
        Permission::truncate();
        Permission::insert([
            [
                'name'        => 'All permission',
                'slug'        => '*',
                'http_method' => '',
                'http_path'   => '*',
            ],
            [
                'name'        => 'Dashboard',
                'slug'        => 'dashboard',
                'http_method' => 'GET',
                'http_path'   => '/\r\ndashboard',
            ],
            [
                'name'        => 'Login',
                'slug'        => 'auth.login',
                'http_method' => '',
                'http_path'   => "auth/login\r\nauth/logout",
            ],
            [
                'name'        => 'User setting',
                'slug'        => 'auth.setting',
                'http_method' => 'GET,PUT',
                'http_path'   => '/auth/setting',
            ],
            [
                'name'        => 'Auth management',
                'slug'        => 'auth.management',
                'http_method' => '',
                'http_path'   => "auth/roles\r\nauth/permissions\r\nauth/users",
            ],
        ]);

        Role::first()->permissions()->save(Permission::first());

        // add default menus.
        Menu::truncate();
        Menu::insert([
            [
                'parent_id' => 0,
                'order'     => 1,
                'title'     => 'Dashboard',
                'icon'      => 'fa-dashboard',
                'uri'       => '/',
            ],
            [
                'parent_id' => 0,
                'order'     => 2,
                'title'     => 'Masters',
                'icon'      => 'fa-bars',
                'uri'       => '',
            ],
            [
                'parent_id' => 0,
                'order'     => 9,
                'title'     => 'Inquiries',
                'icon'      => 'fa-list-alt',
                'uri'       => '/inquiries',
            ],
            [
                'parent_id' => 0,
                'order'     => 10,
                'title'     => 'Admin',
                'icon'      => 'fa-tasks',
                'uri'       => '',
            ],
            [
                'parent_id' => 2,
                'order'     => 3,
                'title'     => 'Cities',
                'icon'      => 'fa-tasks',
                'uri'       => '/cities',
            ],
            [
                'parent_id' => 2,
                'order'     => 4,
                'title'     => 'Sources',
                'icon'      => 'fa-circle',
                'uri'       => '/sources',
            ],
            [
                'parent_id' => 2,
                'order'     => 5,
                'title'     => 'Branches',
                'icon'      => 'fa-code-fork',
                'uri'       => '/branches',
            ],
            [
                'parent_id' => 2,
                'order'     => 6,
                'title'     => 'Programs',
                'icon'      => 'fa-book',
                'uri'       => '/programs',
            ],
            [
                'parent_id' => 2,
                'order'     => 7,
                'title'     => 'Countries',
                'icon'      => 'fa-globe',
                'uri'       => '/countries',
            ],
            [
                'parent_id' => 2,
                'order'     => 8,
                'title'     => 'Area',
                'icon'      => 'fa-map-marker',
                'uri'       => '/areas',
            ],
            [
                'parent_id' => 4,
                'order'     => 11,
                'title'     => 'Users',
                'icon'      => 'fa-users',
                'uri'       => 'auth/users',
            ],
            [
                'parent_id' => 4,
                'order'     => 12,
                'title'     => 'Roles',
                'icon'      => 'fa-user',
                'uri'       => 'auth/roles',
            ],
            [
                'parent_id' => 4,
                'order'     => 13,
                'title'     => 'Permission',
                'icon'      => 'fa-ban',
                'uri'       => 'auth/permissions',
            ],
            [
                'parent_id' => 4,
                'order'     => 14,
                'title'     => 'Menu',
                'icon'      => 'fa-bars',
                'uri'       => 'auth/menu',
            ],
            [
                'parent_id' => 4,
                'order'     => 15,
                'title'     => 'Operation log',
                'icon'      => 'fa-history',
                'uri'       => 'auth/logs',
            ]
        ]);

        // add role to menu.
        Menu::find(2)->roles()->save(Role::first());
    }
}
