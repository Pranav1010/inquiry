<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


//Auth::routes();


//Route::get('/', 'HomeController@index');
/*Route::get('/home', 'HomeController@index');
Route::get('/home/yesterday', 'HomeController@yesterday');
Route::get('/home/older', 'HomeController@older');
Route::get('/home/today', 'HomeController@today');
Route::get('/home/tomorrow', 'HomeController@tomorrow');
Route::get('/home/newer', 'HomeController@newer');*/
Route::get('/userWise/{user}', 'HomeController@index');
//Route::resource('/branches', 'BranchesController');
//Route::resource('/courses', 'CoursesController');
//Route::resource('/inquiries', 'InquiriesController');
Route::resource('/users', 'UsersController');
//Route::get('/completed', 'InquiriesController@completedInquiries');
//Route::get('/cancelled', 'InquiriesController@cancelledInquiries');
//Route::get('/forwarded', 'InquiriesController@forwardedInquiries');
//Route::get('/inquiries/{inquiry}/followUp', 'InquiriesController@followUp');
//Route::post('/inquiries/followUp', 'InquiriesController@storeFollowUp');
//Route::get('/inquiries/{inquiry}/followUp/create', 'InquiriesController@createFollowUp');
//Route::get('/inquiries/followUp/{followUp}/edit', 'InquiriesController@editFollowUp');
//Route::put('/inquiries/followUp/{followUp}', 'InquiriesController@UpdateFollowUp');
//Route::resource('/cities', 'CityController');
//Route::resource('/countries', 'CountryController');
//Route::resource('/areas', 'AreaController');
/*Route::resource('/roles', 'RoleController');*/
Route::resource('/departments', 'DepartmentController');
Route::resource('/designations', 'DesignationController');
//new route
/*Route::get('/completed', 'VisaInquiryController@completedInquiries');
Route::get('/cancelled', 'VisaInquiryController@cancelledInquiries');
Route::get('/forwarded', 'VisaInquiryController@forwardedInquiries');
Route::get('/inquiries/{inquiry}/followUp', 'VisaInquiryController@followUp');
Route::post('/inquiries/followUp', 'VisaInquiryController@storeFollowUp');
Route::get('/inquiries/{inquiry}/followUp/create', 'VisaInquiryController@createFollowUp');
Route::get('/inquiries/followUp/{followUp}/edit', 'VisaInquiryController@editFollowUp');
Route::put('/inquiries/followUp/{followUp}', 'VisaInquiryController@UpdateFollowUp');

Route::get('inquiries/ajax-area/{id}', 'VisaInquiryController@getArea');
Route::get('inquiries/ajax-friend-list/', 'VisaInquiryController@autoCompleteFriend');*/
//Route::get('inquiries/ajax-country/{id}', 'InquiriesController@getCountry');


//Route::put('/branches/{branch_id}/edit', 'BranchesController@update');
//Route::get('branches/{id}/load-{action}','BranchesController@loadModal');
/*Route::resource('/sources', 'InquirySourceController');*/


//Route::resource('/inquiries', 'VisaInquiryController');
/*Route::post('inquiries/personal-information/', 'VisaInquiryController@store');*/
/*Route::post('inquiries/academic-information/{id}', 'VisaInquiryController@setAcademicInformation');
Route::post('inquiries/exam-information/{id}', 'VisaInquiryController@setExamInformation');
Route::post('inquiries/work-experience-information/{id}', 'VisaInquiryController@setWorkExperienceInformation');
Route::post('inquiries/travel-history-information/{id}', 'VisaInquiryController@setTravelHistoryInformation');
Route::post('inquiries/counselling-summary-information/{id}', 'VisaInquiryController@setCounsellingSummaryInformation');
Route::post('inquiries/dependent-information/{id}', 'VisaInquiryController@setDependentInformation');
Route::post('inquiries/dependent-academic-information/{dependent_id}', 'VisaInquiryController@setDependentAcademicInformation');
Route::post('inquiries/dependent-exam-information/{id}', 'VisaInquiryController@setDependentExamInformation');
Route::post('inquiries/dependent-work-experience-information/{id}', 'VisaInquiryController@setDependentWorkExperienceInformation');
Route::post('inquiries/dependent-travel-history-information/{id}', 'VisaInquiryController@setDependentTravelHistoryInformation');

Route::put('inquiries/{visa_inquiry}/edit-personal-information/', 'VisaInquiryController@update');
Route::put('inquiries/{inquiry_id}/edit-academic-information/', 'VisaInquiryController@updateInquiryAcademic');
Route::put('inquiries/{inquiry_id}/edit-work-experience-information/', 'VisaInquiryController@updateInquiryWorkExperience');
Route::put('inquiries/{inquiry_id}/edit-travel-history-information/', 'VisaInquiryController@updateInquiryTravelHistory');
Route::put('inquiries/{inquiry_id}/edit-counselling-summary-information/', 'VisaInquiryController@updateInquiryCounsellingSummary');
Route::put('inquiries/{inquiry_id}/edit-exam-information/', 'VisaInquiryController@updateInquiryExam');

Route::put('inquiries/{dependent_id}/edit-dependent-detail/', 'VisaInquiryController@updateDependentDetail');
Route::put('inquiries/{dependent_id}/edit-dependent-academic-information/', 'VisaInquiryController@updateDependentAcademic');
Route::put('inquiries/{dependent_id}/edit-dependent-exam-information/', 'VisaInquiryController@updateDependentExam');
Route::put('inquiries/{dependent_id}/edit-dependent-work-information/', 'VisaInquiryController@updateDependentWorkExperience');
Route::put('inquiries/{dependent_id}/edit-dependent-travel-information/', 'VisaInquiryController@updateDependentTravel');*/










