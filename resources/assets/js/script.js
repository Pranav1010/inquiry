/**
 * Created by Pranav on 29-Jul-17.
 */

$(document).ready(function() {

    //var baseUrl = "/center_mistra/public/";
    var baseUrl = "/";

    getUserWiseData = function(e) {
        var userId = $(e).val();
        location.assign(baseUrl + "userWise/" + userId);
    };

    displayReasonBox = function() {
        var selectedUserId = $("#userData").val();
        var assignBox = $("#assigning-reason");
        var userId = assignBox.data("id");

        if (selectedUserId != userId) {
            assignBox.show();
        } else {
            assignBox.hide();
        }
    };

    displayReasonBox();

    displayOtherBox = function() {
        var selectedSource = $("#inquiry-source").val();
        var otherBox = $("#otherBox");
        if (selectedSource == "Other") {
            otherBox.show();
        } else {
            otherBox.hide();
        }
    };

    displayOtherBox();

    $('#getDataWithDate').on('click', function(e) {
        e.preventDefault();
        var userId = $('#dashboard-user').val();
        var startDate = $('#reservation').data('daterangepicker').startDate.format('YYYY-MM-D');
        var endDate = $('#reservation').data('daterangepicker').endDate.format('YYYY-MM-D');
        location.assign(baseUrl + "userWise/" + userId + "?start=" + startDate + "&end=" + endDate);
    })

});