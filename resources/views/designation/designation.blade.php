<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

@extends('adminlte::page')

@section('title', 'Designation')

@section('content_header')
    <h1>Designation</h1>
@stop
@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title" data-widget="collapse">Add New</h3>
            <div class="box-tools pull-right">
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <form action="{{url('/designations')}}" method="post">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <div class="col-md-1">
                        <label for="select-1">Designation</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="name" id="name" style="width: 100%" autofocus>
                        @if($errors->has('name'))
                            <span class="help-block" style="color: red" id="errorMessage">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-1">
                        <label for="select-1">Department</label>
                    </div>
                    <div class="col-md-3">
                        <select class="form-control" id="city_id" name="department_id">
                            @foreach(App\Department::all() as $department)
                                <option value="{{$department->id}}">{{$department->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <input type="submit" class="btn btn-primary form-control" style="width: 50%;" value="Add">
                    </div>
                </div>
            </form>
        </div>

        <div class="box">
            <div class="box-body">
                <table id="example" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th style="width:7%">Sr. No.</th>
                        <th>Designation</th>
                        <th>Department</th>
                        <th width="7%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php $i = 1 ?>
                    @foreach($designations as $designation)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{$designation->name}}</td>
                            <td>{{$designation->department['name']}}</td>
                            <td>
                                <div class="btn-group" role="group" aria-label="...">
                                    <a
                                            href="{{url('/designations/'.$designation->id.'/edit')}}"
                                            class="btn btn-primary btn-sm"
                                            data-toggle="tooltip" title="Edit"><i
                                                class="glyphicon glyphicon-edit"></i></a>

                                    <a href="#" class="btn btn-danger btn-sm"
                                       onclick="event.preventDefault(); $('#form-{{$designation->id}}').submit();"
                                       data-toggle="tooltip" title="Delete"><i
                                                class="glyphicon glyphicon-trash"></i></a>

                                    <form action="{{url('/designations/'.$designation->id)}}"
                                          onsubmit="return confirm('Are you sure you want to delete this item?'); "
                                          id="form-{{$designation->id}}" method="post" style="display: none">
                                        {{csrf_field()}}
                                        {{method_field("DELETE")}}
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.box -->
@stop
@section("adminlte_js")
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>

                @if(Session::has('data'))
        var type = "{{ Session::get('data.type')}}";
        switch (type) {
            case 'info':
                toastr.info("{{ Session::get('data.message') }}");
                break;

            case 'warning':
                toastr.warning("{{ Session::get('data.message') }}");
                break;
            case 'success':
                toastr.success("{{ Session::get('data.message') }}");
                break;
            case 'error':
                toastr.error("{{ Session::get('data.message') }}");
                break;
        }
        @endif

    </script>
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
            $('[data-toggle="tooltip"]').tooltip();

            $("#master-menu").css("background", "white");
            $("#master-menu").css("color", "#265671");

            $("#designation-menu").css("background", "#d3dce4");
            $("#designation-menu a").css("color", "#265671");

            setTimeout(function () {
                $('#errorMessage').fadeOut('fast');
            }, 3000);
        });

    </script>
@endsection

