<table class="table no-header table-bordered">
    <caption><h4>Branch Details:</h4></caption>
    <tbody>
    <tr>
        <td width="20%">
            <b>Branch Name</b>
        </td>
        <td>
            {{$branch->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Contact No.</b>
        </td>
        <td>
            {{$branch->contact_no or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>City</b>
        </td>
        <td>
            {{$branch->city->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Address</b>
        </td>
        <td>
            {{$branch->address or "-"}}
        </td>
    </tr>
    </tbody>
</table>
<table class="table no-header table-bordered">
    <caption><h4>Contact Person Details:</h4></caption>
    <thead>
    <th>Name</th>
    <th>Mobile</th>
    <th>Email</th>
    </thead>
    <tbody>
    @foreach($contactDetails as $key=>$value)
        <tr>
            <td>
                {{$value->name or "-"}}
            </td>
            <td>
                {{$value->mobile or "-"}}
            </td>
            <td>
                {{$value->email or "-"}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>







