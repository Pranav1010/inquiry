<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Edit</h3>
    </div>
    <!-- /.box-header -->

    <form action="{{url('/branches/'.$branch->id)}}" method="POST" class="form-horizontal" id="editBranch">

        {{ csrf_field() }}
        {{method_field('PUT')}}

        <input type="hidden" value="{{$branch->id}}" id="branchId" name="id">

        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Branch Name:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" class="form-control" name="name" placeholder="Input Branch Name"
                                   value="{{$branch->name or ''}}">
                            <span class="help-block name" style="color: red"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Contact No:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" class="form-control" name="contact_no" placeholder="Input Contact Number"
                                   value="{{$branch->contact_no}}">
                            <span class="help-block contact_no" style="color: red;" id="errorMessage"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">City:</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="city" name="city_id">
                            @foreach(App\City::all() as $city)
                                <option value="{{$city->id}}" @if($city->id == $branch->city_id){{'selected'}} @endif>{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address:</label>
                    <div class="col-sm-8">
                        <textarea name="address" placeholder="Input Address" class="form-control"
                                  rows="4">{{$branch->address or ""}}</textarea>
                        <span class="help-block address" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
            </div>

            {{--<div class="col-md-12 row">
                <div class="col-md-6 row" style=" padding-right: 0; ">
                    <div class="col-md-3 form-group">
                        <label>Branch Name:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px; ">
                        <input type="text" class="form-control" name="name" placeholder="Branch Name"
                               value="{{$branch->name or ''}}">
                        <span class="help-block name" style="color: red">
                                    </span>
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Contact No:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px; ">
                        <input type="text" class="form-control" name="contact_no" placeholder="Contact Number"
                               value="{{$branch->contact_no}}">
                        <span class="help-block contact_no" style="color: red;" id="errorMessage">
                                    </span>
                    </div>
                    <div class="col-md-3 form-group">
                        <label>City:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px; ">
                        <select class="form-control" id="city" name="city_id">
                            @foreach(App\City::all() as $city)
                                <option value="{{$city->id}}" @if($city->id == $branch->city_id){{'selected'}} @endif>{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 row" style=" padding-right: 0; ">
                    <div class="col-md-3 form-group" style=" padding-left: 45px; ">
                        <label>Address:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px;">
                        <textarea name="address" placeholder="Address" class="form-control" rows="6">{{$branch->address or ""}}</textarea>
                        <span class="help-block address" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
            </div>--}}
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-9">
                        <h3 style="margin-top: 0px">Contact Person Details</h3>
                    </div>
                    <div class="col-sm-3">
                        <div class="pull-right">
                            <button class="btn btn-info btn-sm" type="button" onclick="addRow('dataTable')">Add More
                                Row
                            </button>
                        </div>
                    </div>
                </div>
                <table id="dataTable" class="form table table-bordered compact">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th style="width: 10%">Action</th>
                    </tr>
                    </thead>
                    <tbody id="contact_person_container">
                    @php $i=1; @endphp
                    @foreach($branchDetails as $key=>$detail)
                        <tr class="remove-row{{$i}} contact-person-row">
                            <input type="hidden" value="{{$detail->id}}" id="branch_id"
                                   name="contactDetail[{{$key + 1 }}][id]">

                            <td>
                                <input type="text" name="contactDetail[{{$key + 1 }}][contactname]"
                                       class="form-control input-sm" id="name" value="{{$detail->name}}">
                                <span class="help-block contactname" style="color: red" id="contactname{{$i}}">
                                    </span>
                            </td>
                            <td>

                                <input type="text" name="contactDetail[{{$key + 1}}][mobile]"
                                       class="form-control input-sm"
                                       id="mobile" value="{{$detail->mobile}}">
                                <span class="help-block mobile" style="color: red" id="mobile{{$i}}">
                                    </span>
                            </td>
                            <td>

                                <input type="email" name="contactDetail[{{$key + 1}}][email]" id="email"
                                       class="form-control input-sm" id="email" value="{{$detail->email}}">
                                <span class="help-block email" style="color: red" id="email{{$i}}">
                                    </span>
                            </td>
                            <td>
                                <button value="Remove " type="button" class="btn btn-danger btn-sm"
                                        onclick="deleteRow('remove-row{{$i}}')">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </td>
                            @php $i++ ;@endphp
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <span class="text-red help-block" id="contactPersonError"></span>
            </div>

            {{--<div class="col-md-12" align="right">
                <button type="submit" class="btn btn-info" >Submit</button>
            </div>--}}

        </div>

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right"
                            data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                    </button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>


    </form>
    <!-- /.box-body -->
</div>

<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body" id="modal-default-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    var count = {{$i - 1 }};

    function addRow(tableID) {
        count++;

        var row = '<tr class="removeRow' + count + ' contact-person-row"><td> <input type="text" id="name' + count + '" class="form-control input-sm" name="contactDetail[' + count + '][contactname]"> <span class="help-block email" style="color: red" id="contactname' + count + '"> </span></td> <td> <input type="text" class="form-control input-sm" name="contactDetail[' + count + '][mobile]"><span class="help-block email" style="color: red" id="mobile' + count + '"> </span></td> <td> <input type="text" class="form-control input-sm"  name="contactDetail[' + count + '][email]"><span class="help-block email" style="color: red" id="email' + count + '"> </span></td><td><button value="Remove " class="btn btn-danger btn-sm" id="removeRow' + count + '" onclick="deleteRow(this.id)"><i class="fa fa-minus" aria-hidden="true"></i></button></td></tr>';

        $('#contact_person_container').append(row);
    }

    function deleteRow(id) {
        $("." + id).remove();
    }

    $("body").on("click", '.open-branch-view-modal', function(){
        var id = $(this).data('id');
        NProgress.start();
        $.ajax({
            type:'GET',
            url:'/branches/'+ id,
            success:function (response) {
                $("#modal-default-body").html(response);
                NProgress.done();
                $("#modal-default").modal("show");
            },
            error:function (error) {
                toastr.error('Something went wrong.');
            }

        });
    });

    $('#editBranch').submit(function (event) {

        $('.help-block').text('');
        event.preventDefault();

        var contactPerson = $(".contact-person-row").length;
        if (contactPerson > 0) {
            var response = $('#editBranch').serialize();
            var branch_id = $('#branchId').val();
            $.ajax({
                type: "PUT",
                url: '/branches/' + branch_id,
                data: response,
                success: function (response) {

                    if (response == "success") {

                        $.pjax({url: "/branches", container: '#pjax-container'});
                        toastr.success('Updated Successfully');
                    }
                },
                error: function (errorThrown) {
                    errorThrown = errorThrown.responseJSON;

                    if (errorThrown.branch_details) {
                        var branch = errorThrown.branch_details;

                        $.each(branch, function (key, value) {

                            $('.' + key).text(value);
                        });
                    }

                    if (errorThrown.contact_details) {
                        var contact = errorThrown.contact_details;

                        $.each(contact, function (key, value) {

                            $.each(value, function (k, v) {

                                $('#' + k + key).text(v);
                            })
                        });
                    }
                }
            });
        } else {

            $("#contactPersonError").text('Please fill the table.');
        }

    });

</script>