
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>
    </div>

    <form action="{{url('/branches')}}" method="POST" class="form-horizontal" id="createBranch">

        {{ csrf_field() }}

        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Branch Name:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" class="form-control " name="name" placeholder="Input Branch Name">
                            <span class="help-block name" style="color: red"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Contact No:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" class="form-control" name="contact_no" placeholder="Input Contact Number" maxlength="10">
                            <span class="help-block contact_no" style="color: red;" id="errorMessage"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">City:</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="city" name="city_id">
                            @foreach(App\City::all() as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address:</label>
                    <div class="col-sm-8">
                        <textarea name="address" placeholder="Input Address" class="form-control" rows="4"></textarea>
                        <span class="help-block address" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
            </div>

            {{--<div class="row">
                <div class="col-md-6" style=" padding-right: 0; ">
                    <div class="col-md-3 form-group">
                        <label>Branch Name:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px; ">
                        <input type="text" class="form-control " name="name" placeholder="Branch Name">
                        <span class="help-block name" style="color: red">
                                    </span>
                    </div>
                    <div class="col-md-3 form-group">
                        <label>Contact No:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px; ">
                        <input type="text" class="form-control" name="contact_no" placeholder="Contact Number" maxlength="10">
                        <span class="help-block contact_no" style="color: red;" id="errorMessage">
                                    </span>
                    </div>
                    <div class="col-md-3 form-group">
                        <label>City:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px; ">
                        <select class="form-control" id="city" name="city_id">
                            @foreach(App\City::all() as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6 row" style=" padding-right: 0; ">
                    <div class="col-md-3 form-group" style=" padding-left: 45px; ">
                        <label>Address:</label>
                    </div>
                    <div class="col-md-9 form-group" style=" padding-right: 0px;">
                        <textarea name="address" placeholder="Address" class="form-control" rows="6"></textarea>
                        <span class="help-block address" style="color: red" id="errorMessage">
                                    </span>
                    </div>
                </div>
            </div>--}}
            <div class="col-md-12">
                <fieldset><h3>Contact Person Details</h3></fieldset>
                <table id="dataTable" class="form table table-bordered compact">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th style="width: 10%;">Action</th>
                    </tr>
                    </thead>
                    <tbody id="contact_person_container">
                    <tr>
                        <td>
                            <input type="text" name="contactDetail[0][contactname]"
                                   class="form-control input-sm" id="name">
                            <span class="help-block contactname-0" style="color: red" id="contactname0">
                                    </span>
                        </td>
                        <td>
                            <input type="text" name="contactDetail[0][mobile]" class="form-control input-sm"
                                   id="mobile" maxlength="10">
                            <span class="help-block mobile" style="color: red" id="mobile0">
                                    </span>
                        </td>
                        <td>
                            <input type="text" name="contactDetail[0][email]" id="email"
                                   class="form-control input-sm">
                            <span class="help-block email" style="color: red" id="email0">
                                    </span>
                        </td>
                        <td>
                            <button class="btn btn-info btn-sm" type="button" onclick="addRow('dataTable')"><i
                                        class="fa fa-plus"
                                        aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

            {{--<div class="col-md-12" align="right">
                <button type="submit" class="btn btn-info" >Submit</button>
            </div>--}}
        </div>

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /.box-body -->
</div>

<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <div class="modal-body" id="modal-default-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>
    var count = 0;
    function addRow(tableID) {
        count++;
        var row = '<tr class="removeRow' + count + '"><td> <input type="text" id="name' + count + '" class="form-control input-sm" name="contactDetail[' + count + '][contactname]"> <span class="help-block email" style="color: red" id="contactname' + count + '"> </span></td> <td> <input type="text" class="form-control input-sm" name="contactDetail[' + count + '][mobile]" maxlength="10"><span class="help-block email" style="color: red" id="mobile' + count + '"> </span></td> <td> <input type="text" class="form-control input-sm"  name="contactDetail[' + count + '][email]"><span class="help-block email" style="color: red" id="email' + count + '"> </span></td><td><button value="Remove " class="btn btn-danger btn-sm" id="removeRow' + count + '" onclick="deleteRow(this.id)"><i class="fa fa-minus" aria-hidden="true"></i></button></td></tr>';
        $('#contact_person_container').append(row);
    }

    $("body").on("click", '.open-branch-view-modal', function(){
        var id = $(this).data('id');
        NProgress.start();
        $.ajax({
            type:'GET',
            url:'/branches/'+ id,
            success:function (response) {
                $("#modal-default-body").html(response);
                NProgress.done();
                $("#modal-default").modal("show");
            },
            error:function (error) {
                toastr.error('Something went wrong.');
            }

        });
    });

    function deleteRow(id) {
        $("." + id).remove();
    }

    $('#createBranch').submit(function (event) {

        $('.help-block').text('');

        event.preventDefault();
        var response = $('#createBranch').serialize();

        $.ajax({
            type: "post",
            url: "../branches",
            data: response,
            success: function (response) {
                if(response == "success"){
                    $.pjax.reload("#pjax-container");
                    toastr.success('Added Successfully');
                }
            },
            error: function (errorThrown) {

                errorThrown = errorThrown.responseJSON;

                if(errorThrown.branch_details){
                    var branch = errorThrown.branch_details;

                    $.each(branch, function (key, value) {

                        $('.' + key).text(value);

                        console.log(value);
                    });
                }

                if(errorThrown.contact_details){
                    var contact = errorThrown.contact_details;

                    $.each(contact, function (key, value) {

                        $.each(value, function (k,v) {

                            $('#' + k + key).text(v);
                            console.log('#' + k + key);
                        })

                    });
                }
            }
        })
    })
</script>