<div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua" style="box-shadow: 3px 4px 10px #888;">
        <div class="inner">
            <h3>{{$totalInquiries}}</h3>
            <p>Total Inquiries</p>
        </div>
        <div class="icon">
            <i class="fa fa-list-alt"></i>
        </div>
        <a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green" style="box-shadow: 3px 4px 10px #888;">
        <div class="inner">
            <h3>{{$completedInquiries}}</h3>
            <p>Completed Inquiries</p>
        </div>
        <div class="icon">
            <i class="fa fa-thumbs-up"></i>
        </div>
        <a href="{{url('/completed')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>
<div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red" style="box-shadow: 3px 4px 10px #888;">
        <div class="inner">
            <h3>{{$cancelledInquiries}}</h3>
            <p>Cancelled Inquiries</p>
        </div>
        <div class="icon">
            <i class="fa fa-thumbs-down"></i>
        </div>
        <a href="{{url('/cancelled')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>

<!--<div class="box col-md-12">
    <div class="box-body">
        <div class="col-lg-5">
            <div class="col-md-2">
                <span><b>Select User: </b></span>
            </div>
            <div class="col-md-10" style="margin-bottom:10px">
                <select class="col-lg-3 form-control" id="dashboard-user" onchange="getUserWiseData(this);">
                    @foreach(Encore\Admin\Auth\Database\Administrator::all() as $user)
                        <option value="{{$user->id}}" @if($userId == $user->id) {{'selected'}}  @endif>{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group col-lg-5">
            <div class="col-md-2">
                <span><b>Select Date: </b></span>
            </div>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date-range" class="form-control pull-right" id="reservation">
            </div>
        </div>

        <div class="col-lg-2">
            <a href="#" id="getDataWithDate" class="btn btn-primary">Get Data</a>
        </div>
        <div class="col-lg-8 col-lg-offset-4" style="text-align: right;">
            <div class="col-md-3">
                <a href="{{url('home/older')}}" class="btn btn-default" style="width: 100%">Day Before Yesterday</a>
            </div>

            <div class="col-md-2">
                <a href="{{url('home/yesterday')}}" class="btn btn-default" style="width: 100%">Yesterday</a>
            </div>
            <div class="col-md-2">
                <a href="{{url('home/today')}}" class="btn btn-default" style="width: 100%">Today</a>
            </div>

            <div class="col-md-2">
                <a href="{{url('home/tomorrow')}}" class="btn btn-default" style="width: 100%">Tomorrow</a>
            </div>

            <div class="col-md-3">
                <a href="{{url('home/newer')}}" class="btn btn-default" style="width: 100%">Day After Tomorrow</a>
            </div>
        </div>
    </div>
</div>


<div class="box col-md-12">
    <div class="box-body">
        <table id="example" class="table table-bordered table-striped dt-responsive">
            <thead>
            <tr>
                <th>#</th>
                <th>Inquiry Name</th>
                <th>Interest Level</th>
                <th>Follow Up Status</th>
                <th>Follow Up Action</th>
                <th>Next Follow Up Date</th>
                <th>Total Follow Ups</th>
                <th>Days Past</th>
                <th width="10%">Comments</th>
                <th width="15%">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1?>
            @foreach($followUps as $followUp)
                <tr>

                    <td>{{ $i++ }}</td>
                    <td>{{$followUp->inquiryWiseUser->inquiry->name}}</td>
                    <td>{{$followUp->interest_level}}</td>
                    <td>{{$followUp->status}}</td>
                    <td>{{$followUp->action}}</td>
                    <td>{{date('d-m-Y H:i:s',strtotime($followUp->next_follow_up))}}</td>
                    <td>{{$followUp->getNumberOfFollowUps($followUp->inquiryWiseUser->inquiry_id)}}</td>
                    <td>{{$followUp->getDaysFromStartDateOfInquiry($followUp->inquiryWiseUser->inquiry->created_at)}}</td>
                    <td>{{$followUp->comments}}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="...">
                            <a href="{{url('/inquiries/'.$followUp->inquiryWiseUser->inquiry->id.'/followUp?inquiry_wise_user_id='.$followUp->inquiry_wise_user_id)}}"
                               class="btn btn-success btn-sm"
                               data-toggle="tooltip" title="View"><i
                                        class="glyphicon glyphicon-eye-open"></i></a>
                            <a href="{{url('/inquiries/followUp/'.$followUp->id.'/edit')}}"
                               class="btn btn-primary btn-sm"
                               data-toggle="tooltip" title="Edit"><i
                                        class="glyphicon glyphicon-edit"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div> -->

<script type="text/javascript">
    /*$(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();
        $('#reservation').daterangepicker({

            locale: {
                format: 'DD-MM-YYYY'
            },

            startDate: "{{$startDate}}",
            endDate: "{{$endDate}}"

        });

    });*/
</script>