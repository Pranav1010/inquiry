<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

@extends('adminlte::page')

@section('title', 'Edit City')

@section('content_header')
    <h1>Edit City</h1>
@stop


@section('content')
    <div class="box">
        <div class="box-header">
            {{--<h3 class="box-title">Courses</h3>--}}
                <a href="{{ url()->previous() }}" class="btn btn-primary pull-right">Back</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="form-group">
                <form action="{{url('/cities/'.$city->id)}}" method="POST">

                    {{ csrf_field() }}
                    {{method_field('PUT')}}
                    <div class="row">
                        <div class="col-md-1" style="float: left;">
                            <label>Name:</label>
                        </div>
                        <div class="col-md-5" style="float: left;">
                            <input type="text" class="form-control" name="name" value="{{$city->name or ""}}">
                            @if($errors->has('name'))
                                <span class="help-block" style="color: red" id="errorMessage">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12" align="right">
                        <button type="submit" class="btn btn-primary" style="margin: 20px 10px 20px 0">Update</button>
                        <a href="{{url('/cities')}}" class="btn btn-primary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
@stop
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
            @if(Session::has('data'))
    var type = "{{ Session::get('data.type')}}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('data.message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('data.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('data.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('data.message') }}");
            break;
    }
    @endif
</script>

@section("adminlte_js")
     <script>
        $(document).ready(function () {
            $('#example').DataTable();
            $('[data-toggle="tooltip"]').tooltip();

            $("#master-menu").css("background", "white");
            $("#master-menu").css("color", "#265671");

            $("#city-menu").css("background", "#d3dce4");
            $("#city-menu a").css("color", "#265671");

            setTimeout(function () {
                $('#errorMessage').fadeOut('fast');
            }, 3000);
        });

    </script>
@endsection