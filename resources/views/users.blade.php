@extends('adminlte::page')

@section('title', 'Manage Staff')

@section('content_header')
    <h1>Manage Team</h1>
@stop


@section('content')

    <div class="box">
        <div align="right">
            <a href="{{url('/users/create')}}" class="btn btn-primary" style="margin: 10px">Add New Member</a>
        </div>
        <div class="box-body">
            <table id="example" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Branch</th>
                    <th width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1?>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->branch->name or ""}}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="...">

                                <a href="{{url('/users/'.$user->id.'/edit')}}" class="btn btn-primary btn-sm"
                                   data-toggle="tooltip" title="Edit"><i
                                            class="glyphicon glyphicon-edit"></i></a>

                                @if($user->is_admin == 1)
                                <a href="#" class="btn btn-danger btn-sm" style="display: none"
                                   onclick="event.preventDefault(); $('#form-{{$user->id}}').submit();"
                                   data-toggle="tooltip" title="Delete"><i
                                            class="glyphicon glyphicon-trash"></i></a>

                                    @else
                                    <a href="#" class="btn btn-danger btn-sm"
                                       onclick="event.preventDefault(); $('#form-{{$user->id}}').submit();"
                                       data-toggle="tooltip" title="Delete"><i
                                                class="glyphicon glyphicon-trash"></i></a>
                                @endif

                                <form action="{{url('/users/'.$user->id)}}"
                                      onsubmit="return confirm('Are you sure you want to delete this item?'); "
                                      id="form-{{$user->id}}" method="post" style="display: hidden">
                                    {{csrf_field()}}
                                    {{method_field("DELETE")}}
                                </form>

                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- /.box -->
@stop

@section("adminlte_js")
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
            $('[data-toggle="tooltip"]').tooltip();

            $("#user-menu").css("background", "white");
            $("#user-menu a").css("color", "#265671");
        });
    </script>
@endsection

