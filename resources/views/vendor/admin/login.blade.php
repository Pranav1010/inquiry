<!DOCTYPE html>
<html style="display: flex; align-items: center; justify-content: center">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{config('admin.title')}} | {{ trans('admin.login') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css") }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/font-awesome/css/font-awesome.min.css") }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css") }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/plugins/iCheck/square/blue.css") }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="//oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page" style="background: #1e3c72; /* Old browsers */ background: -moz-linear-gradient(-45deg, #1e3c72 0%, #2a5298 100%); /* FF3.6-15 */ background: -webkit-linear-gradient(-45deg, #1e3c72 0%,#2a5298 100%); /* Chrome10-25,Safari5.1-6 */ background: linear-gradient(135deg, #1e3c72 0%,#2a5298 100%);">
{{--<div class="login-box">
  <div class="login-logo">
    <a href="{{ admin_base_path('/') }}"><b>{{config('admin.name')}}</b></a>
  </div>--}}
  <!-- /.login-logo -->
  {{--<div class="login-box-body">--}}
    {{--<p class="login-box-msg">{{ trans('admin.login') }}</p>--}}

  <div class="container">
    <div class="row">
      <div class="col-sm-3 col-sm-offset-1">
        <img src="{{ asset("img/logo.png") }}" alt="" style="margin: 14% 0; width: 100%">
      </div>
      <div class="col-sm-5 col-sm-offset-1">
        <form action="{{ admin_base_path('auth/login') }}" style="padding: 20px; border-radius: 4px; background: #ffffff21;" method="post">
            <p class="login-box-msg" style="font-size: 20px; color: white;">{{ trans('admin.login') }}</p>
          <div class="form-group has-feedback {!! !$errors->has('username') ?: 'has-error' !!}">

            @if($errors->has('username'))
              @foreach($errors->get('username') as $message)
                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>{{$message}}</label></br>
              @endforeach
            @endif

            <input type="input" class="form-control" style="border: 1px solid #bbbbbb; background: none; border-radius: 4px; padding: 20px 10px; color: #ffff;" placeholder="{{ trans('admin.username') }}" name="username" value="{{ old('username') }}">
            <span class="glyphicon glyphicon-envelope form-control-feedback" style="padding-top: 5px; color: white;"></span>
          </div>
          <div class="form-group has-feedback {!! !$errors->has('password') ?: 'has-error' !!}">

            @if($errors->has('password'))
              @foreach($errors->get('password') as $message)
                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>{{$message}}</label></br>
              @endforeach
            @endif

            <input type="password" class="form-control" style="border: 1px solid #bbbbbb; background: none; border-radius: 4px; padding: 20px 10px; color: #ffff;" placeholder="{{ trans('admin.password') }}" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback" style="padding-top: 5px; color: white;"></span>
          </div>
          <div class="row" style="margin-top: 40px">

            <!-- /.col -->
            <div class="col-xs-6 col-xs-offset-3">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" style="padding: 10px; border-radius: 4px;" class="btn btn-info btn-block btn-flat">{{ trans('admin.login') }}</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
    </div>
  </div>


  {{--</div>--}}
  <!-- /.login-box-body -->
{{--</div>--}}
<!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="{{ admin_asset("/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js")}} "></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{ admin_asset("/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js")}}"></script>
<!-- iCheck -->
<script src="{{ admin_asset("/vendor/laravel-admin/AdminLTE/plugins/iCheck/icheck.min.js")}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
