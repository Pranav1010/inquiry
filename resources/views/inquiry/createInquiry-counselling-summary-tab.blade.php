<div class="box-body">
    <div class="form-group">
        <form action="{{url('/inquiries')}}" method="POST" id="counsellingSummaryForm">
            {{ csrf_field() }}
            <input type="hidden" name="inquiryId" class="inquiryUserId">
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Country</label>
                                <select name="country_id" class="form-control" id="country_id">
                                    @foreach(App\Country::all() as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program_id" class="form-control" id="program_id">
                                    @foreach(App\Course::groupBy('name')->get() as $course)
                                        <option value="{{$course->id}}">{{$course->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Suggestion</label><br>
                        <textarea name="suggestion" class="form-control" style="height: 109px;"></textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea name="remark" class="form-control" style="height: 109px;"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-md-12 form-group" align="right">
                <button type="button" class="btn btn-primary" id="submit-counselling"
                        style="margin: 20px 10px 20px 0">Save
                </button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
