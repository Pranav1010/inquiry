<div class="box box-info">

    <div class="box-header with-border">
        <h3 class="box-title">Edit</h3>
    </div>

    <form action="{{url('/inquiries/followUp/'.$followUp->id)}}" method="POST" class="form-horizontal">

        {{ csrf_field() }}
        {{method_field('PUT')}}

        <input type="hidden" class="form-control" name="inquiry_wise_user_id"
               value="{{$followUp->inquiry_wise_user_id or ""}}">

        <div class="box-body">
            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Inquiry:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" readonly class="form-control" value="{{$inquiry->name or ""}}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Interest Level:</label>
                    <div class="col-sm-8">
                        <select name="interest_level" class="form-control">
                            @foreach(config('app.followUp_interest') as $interest)
                                <option value="{{$interest}}" @if(isset($followUp) and $interest === $followUp->interest_level)
                                    {{'selected'}}
                                        @endif>{{$interest}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status:</label>
                    <div class="col-sm-8">
                        <select name="status" class="form-control">
                            @foreach(config('app.followUp_status') as $status)
                                <option value="{{$status}}"
                                @if(isset($followUp) and $status === $followUp->status)
                                    {{'selected'}}
                                        @endif>{{$status}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Action:</label>
                    <div class="col-sm-8">
                        <select name="action" class="form-control">
                            @foreach(config('app.followUp_action') as $action)
                                <option value="{{$action}}"
                                @if(isset($followUp) and $action === $followUp->action)
                                    {{'selected'}}
                                        @endif>{{$action}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Next Date Time:</label>
                    <div class="col-sm-8">
                        <div class='input-group date'>
                            <input type='text' class="form-control" name="next_follow_up" id='datetimepicker1'
                                   value=" @if(isset($followUp)) {{date('d-m-Y H:i:s',strtotime($followUp->next_follow_up))}} @endif "
                                   required/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Comments:</label>
                    <div class="col-sm-8">
                        <textarea name="comments" class="form-control" rows="3">{{$followUp->comments or ""}}</textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Update</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>

    </form>

    <!-- /.box-body -->
</div>
<!-- /.box -->

