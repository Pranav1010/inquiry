<div class="box-body exam-marks">
    <div id="examMarks">
        <table class="table table-bordered table-responsive table-striped no-header">
            <thead>
            <th></th>
            <th style="width: 12vw;">Listening</th>
            <th style="width: 12vw;">Reading</th>
            <th style="width: 12vw;">Writing</th>
            <th style="width: 12vw;">Speaking</th>
            <th style="width: 12vw;">Over All</th>
            <th style="width: 12vw;">Exam Year</th>
            <th style="width: 12vw;">Result Status</th>
            </thead>
            <tbody>
            <tr>
                <td>
                    <b>IELTS</b>
                    <input type="hidden" name="exam[1][exam_type]" value="IELTS">
                </td>
                <td>
                    <select name="exam[0][listening]" class="form-control minimal" id="listening1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[0][reading]" class="form-control minimal" id="reading1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[0][writing]" class="form-control minimal" id="writing1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[0][speaking]" class="form-control minimal" id="speaking1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[0][overall]" class="form-control minimal" id="overall1">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="exam[0][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('exam[0][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="exam[0][result]" class="form-control minimal">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <b>PTE</b>
                    <input type="hidden" name="exam[1][exam_type]" value="PTE">
                </td>
                <td>
                    <select name="exam[1][listening]" class="form-control minimal" id="listening2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[1][reading]" class="form-control minimal" id="reading2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[1][writing]" class="form-control minimal" id="writing2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[1][speaking]" class="form-control minimal" id="speaking2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[1][overall]" class="form-control minimal" id="overall2">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="exam[1][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('exam[1][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="exam[1][result]" class="form-control minimal">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <b>TOFEL</b>
                    <input type="hidden" name="exam[2][exam_type]" value="TOFEL">
                </td>
                <td>
                    <select name="exam[2][listening]" class="form-control minimal" id="listening3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[2][reading]" class="form-control minimal" id="reading3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <select name="exam[2][writing]" class="form-control minimal" id="writing3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <select name="exam[2][overall]" class="form-control minimal" id="overall3">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="exam[2][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('exam[0][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="exam[2][result]" class="form-control minimal">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            <tr>
                <td>
                    <b>GRE</b>
                    <input type="hidden" name="exam[3][exam_type]" value="GRE">
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                        <span>- N/A -</span>
                    </div>
                </td>
                <td>
                    <select name="exam[3][overall]" class="form-control minimal" id="overall4">
                        <option value=""></option>
                        @foreach(config("app.marks") as $marks)
                            <option value="{{$marks}}">{{$marks}}</option>
                        @endforeach
                    </select>
                </td>
                <td>
                    <input type="number" contenteditable="true" name="exam[3][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{old('exam[3][year]')}}">
                </td>
                <td style="width: 12vw;">
                    <select name="exam[3][result]" class="form-control minimal">
                        <option value=""></option>
                        @foreach(config("app.result_status") as $result)
                            <option value="{{$result}}">{{$result}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>

            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea name="examRemark" class="form-control" rows="3" value="{{old('remark')}}"></textarea>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-group pull-right" style="margin-top: 40px; margin-bottom: 0;">
                    <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-exam" style="margin: 20px 10px 20px 0"> Save </button>
                    <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</div>
