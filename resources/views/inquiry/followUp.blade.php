<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Inquiry Information</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="bd-callout bd-callout-primary">
                    <div class="row">
                        <div class="col-md-3"><label>Inquiry: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->name or ''}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-primary">
                    <div class="row">
                        <div class="col-md-3"><label>Branch: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->branch->name or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-warning">
                    <div class="row">
                        <div class="col-md-3"><label>Country: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$counselling->country->name or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-warning">
                    <div class="row">
                        <div class="col-md-3"><label>Program: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$counselling->program->name or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-success">
                    <div class="row">
                        <div class="col-md-3"><label>Email: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->email or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-success">
                    <div class="row">
                        <div class="col-md-3"><label>Mobile: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->mobile or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-success">
                    <div class="row">
                        <div class="col-md-3"><label>City: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->city->name or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-success">
                    <div class="row">
                        <div class="col-md-3"><label>Area: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->area->name or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-info">
                    <div class="row">
                        <div class="col-md-3"><label>Source: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->source->name or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-info">
                    <div class="row">
                        <div class="col-md-3" style="padding-right: 0"><label>Assigned To: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{isset($inquiry->userName) ? $inquiry->userName : '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bd-callout bd-callout-info">
                    <div class="row">
                        <div class="col-md-3"><label>Remarks: &nbsp;&nbsp;</label></div>
                        <div class="col-md-9"><span>{{$inquiry->remarks or '-'}}</span></div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Inquiry</label>
                    <input type="text" readonly class="form-control" value="{{$inquiry->name or ''}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Branch</label>
                    <input type="text" readonly class="form-control" value="{{$inquiry->branch->name or ''}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Program</label>
                    <input type="text" readonly class="form-control" value="{{$counselling->program->name or ''}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" readonly class="form-control" value="{{$inquiry->email or ''}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Mobile</label>
                    <input type="text" readonly class="form-control" value="{{$inquiry->mobile or ''}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>City</label>
                    <input type="text" readonly class="form-control" value="{{$inquiry->city->name or ''}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Source</label>
                    <input type="text" readonly class="form-control" value="{{$inquiry->source->name or ''}}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Assigned User</label>
                    <input type="text" readonly class="form-control" value="{{isset($inquiry->userName) ? $inquiry->userName : ''}}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Remarks</label>
                    <textarea readonly rows="4" class="form-control">{{$inquiry->remarks or ''}}</textarea>
                </div>
            </div>
        </div>--}}
    </div>

    <!-- /.box-body -->
</div>
<!-- /.box -->

