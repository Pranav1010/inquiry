<div class="form-group">

    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="hidden" name="formName" value="personalInformation">

    <div class="row" style="background: #d6d6d6; padding-top: 20px;">
        <div class="col-md-4" style="padding: 0 5px;">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Personal Information</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body personal-info">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body">
                                <div class="row">
                                    {{Session::get('inquiry1')}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>First Name*</label>
                                            <input type="text" class="form-control" required name="firstName" value="{{ isset($inquiry->first_name) ? $inquiry->first_name : old('firstName')}}">
                                            <span class="help-block firstName" style="color: red" id="firstName0"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Middle Name</label>
                                            <input type="text" class="form-control" name="middleName" value="{{ isset($inquiry->middle_name) ? $inquiry->middle_name : old('middleName') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" name="lastName" value="{{ isset($inquiry->last_name) ? $inquiry->last_name : old('lastName')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" class="form-control" name="email" value="{{ isset($inquiry->email) ? $inquiry->email : old('email')}}">
                                            <span class="help-block email" style="color: red" id="email0"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Mobile*</label>
                                            <input type="text" class="form-control" name="mobile" maxlength="10" value="{{ isset($inquiry->mobile) ? $inquiry->mobile : old('mobile')}}">
                                            <span class="help-block mobile" style="color: red" id="mobile0"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Alternate Number</label>
                                            <input type="text" class="form-control" name="alternate_mobile" maxlength="10" id="alternateMobile" value="{{ isset($inquiry->alternate_mobile) ? $inquiry->alternate_mobile : old('alternate_mobile')}}">
                                            <span class="help-block alternate_mobile" style="color: red" id="alternate_mobile0"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control datepicker" name="birth_date" id="birth_date" value="@if(isset($inquiry->birth_date)) {{date("d-m-Y", strtotime($inquiry->birth_date))}} @else {{old('birth_date')}} @endif">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Gender</label><br>
                                            <label class="radio-inline"><input type="radio" name="gender" class="minimal" id="gender" value="2" @if(isset($inquiry->gender) && $inquiry->gender == 2) checked @endif> Male</label>
                                            <label class="radio-inline"><input type="radio" name="gender" class="minimal" id="gender" value="1" @if(isset($inquiry->gender) && $inquiry->gender == 1) checked @endif> Female</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Address</label>
                                            <textarea class="form-control" name="address" style="height: 108px">{{ isset($inquiry->address) ? $inquiry->address : old("address") }}</textarea>
                                            <span class="help-block address" style="color: red" id="address0"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <select class="form-control" id="city" name="city">
                                                        <option value=""></option>
                                                        @foreach(App\City::all() as $city)
                                                            <option value="{{$city->id}}" @if(isset($inquiry->city_id) && $city->id == $inquiry->city_id) selected @endif>{{$city->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Area</label><br>
                                                    <select name="area" class="form-control" id="area">
                                                        <option value=""></option>
                                                        @foreach(App\Area::all() as $area)
                                                            <option value="{{$area->id}}" @if(isset($inquiry->area_id) && $area->id == $inquiry->area_id) selected @endif>{{$area->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Marital Status</label></br>
                                            <label class="radio-inline"><input type="radio" name="m_status" onclick="show1()" id="m_status" value="1" @if(isset($inquiry->marital_status) && $inquiry->marital_status == 1 ) checked @endif> Married</label>
                                            <label class="radio-inline"><input type="radio" name="m_status" onclick="show2()" id="m_status" value="2" @if(isset($inquiry->marital_status) && $inquiry->marital_status == 2 ) checked @endif> Single</label>
                                            <label class="radio-inline"><input type="radio" name="m_status" onclick="show2()" id="m_status" value="3" @if(isset($inquiry->marital_status) && $inquiry->marital_status == 3 ) checked @endif> Engaged</label>
                                            <label class="radio-inline"><input type="radio" name="m_status" onclick="show2()" id="m_status" value="4" @if(isset($inquiry->marital_status) && $inquiry->marital_status == 4 ) checked @endif> Widowed</label>
                                            <label class="radio-inline"><input type="radio" name="m_status" onclick="show2()" id="m_status" value="5" @if(isset($inquiry->marital_status) && $inquiry->marital_status == 5 ) checked @endif> Divorced</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" style="padding: 0 5px;">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Program Information</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Country*</label>
                                                    <select name="inquiryCountry" class="form-control" id="inquiryCountry">
                                                        <option value=""></option>
                                                        @foreach(App\Country::all() as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block inquiryCountry" style="color: red" id="country_id0"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Program*</label>
                                                    <select name="course_id" class="form-control" id="inquiryProgram">
                                                        <option value=""></option>
                                                        @foreach(App\Course::groupBy('name')->get() as $course)
                                                            <option value="{{$course->id}}">{{$course->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <span class="help-block course_id" style="color: red" id="program_id0"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group" style="margin-top: 25px;">
                                                    <a href="#" class="btn btn-default btn-sm" id="programCountryAdd" onclick="addRowCountry('preferredCountryTable', event)"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Remarks</label>
                                            <textarea name="remarks" class="form-control" style="height: 127px;"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="preference-country-box">
                                            <table id="preferredCountryTable" class="table table-striped table-bordered"
                                                   cellspacing="0" style=" width: 100%; ">
                                                <thead>
                                                <tr>
                                                    <th style="width: 48%;">Country</th>
                                                    <th style="width: 48%;">Program</th>
                                                    <th style="width: 4%;"></th>
                                                </tr>
                                                </thead>
                                                <tbody id="preferredCountryList">
                                                @if(isset($inquiry))
                                                    @php
                                                        $i=1;
                                                        $preferredCountry = $inquiry->preferedCountry;
                                                    @endphp
                                                    @foreach($preferredCountry as $k=>$v)
                                                        <tr class="removeRow{{$i}} remove-program">
                                                            <td> {{$v->country->name or ''}}
                                                                <input type="hidden" class="form-control" name="program[]" value="{{$v->country->id or ''}}">
                                                            </td>
                                                            <td>{{$v->program->name or ''}}
                                                                <input type="hidden" class="form-control" name="country[]" value="{{$v->program->id or ''}}">
                                                            </td>
                                                            <td>
                                                                <button value="Remove " type="button" class="btn btn-danger btn-sm hide1" onclick="deleteRowCountry('removeRow{{$i}}')">
                                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        @php
                                                            $i++;
                                                        @endphp
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
        <div class="col-md-4" style="padding: 0 5px;">
            <div class="box box-primary" style="margin-bottom: 10px;">
                <div class="box-header with-border">
                    <h3 class="box-title">Source Information</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Source</label>
                                            <select name="source" class="form-control" id="inquirySource" onchange="displayOtherBox();">
                                                <option value=""></option>
                                                @foreach(App\InquirySource::all() as $source)
                                                    <option value="{{$source->id}}" @if( isset($inquiry->source_id) && $source->id == $inquiry->source_id) selected @endif>{{$source->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Branch</label>
                                            <select name="branch_id" id="inquiryBranch" class="form-control">
                                                <option value=""></option>
                                                @foreach(App\Branch::all() as $branch)
                                                    <option value="{{$branch->id}}" @if( isset($inquiry->branch_id) && $branch->id == $inquiry->branch_id) selected @endif>{{$branch->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" id="otherBox" style="display: none;">
                                        <div class="form-group">
                                            <label>Other Source</label>
                                            <textarea name="otherSource" class="form-control" rows="3" placeholder="Enter your source other than given source">{{isset($inquiry->other_source) ? $inquiry->other_source : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="friendBox" style="display: none;">
                                        <div class="form-group">
                                            <label>Reference Person</label>
                                            <input type="text" name="friendource" class="form-control" placeholder="Select your reference`s name" id="friendId" value="{{ isset($inquiry->friend_name) ? $inquiry->friend_name : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="tagbox" style="display: none;">
                                        <div class="form-group" id="friendBox">
                                            <label>Tag</label>
                                            <input type="text" name="tagfriend" class="form-control" placeholder="Enter your reference`s name" id="tagId" value="{{ isset($inquiry->tag) ? $inquiry->tag : '' }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if(!isRole("guest"))
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Inquiry Action</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Assign To</label>
                                    <select name="user_id" class="form-control" id="userData">
                                        <option value=""></option>
                                        @foreach($counselors as $user)
                                            <option value="{{$user->id}}" @if($user->id == $user_id) selected @endif>{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-block user_id" style="color: red"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="form-group pull-right" style="margin-top: 20px;">
                <button type="button" class="btn btn-primary" id="personalInfo">Save</button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                {{-- <div class="collapse-info pull-right" id="div_expand_bottom">
                    <button type="button" class="btn btn-info " style=" height: 34px; margin-left: 4px; ">
                        <i class="fa fa-minus"></i>
                    </button>
                </div> --}}
            </div>
        </div>

    </div>
</div>
