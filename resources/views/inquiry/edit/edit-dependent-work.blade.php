<div class="box-body">
    <div class="form-group">
        <form action="{{url('/inquiries')}}" method="POST" name ="workExperienceForm" id="editDependentWork">
            {{ csrf_field() }}
            <input type="hidden" value="{{isset($dependentUserId)?$dependentUserId->id:''}}" name="inquiryDependentUserId" class="inquiryDependentUserId" id="inquiryDependentUserId">
            <input type="hidden" name="formName"  value="workExperienceForm">
            <div class="pull-right">
                <button type="button" class="btn btn-info btn-sm" id="editDependentWorkAdd">
                    Add More Row
                </button>
            </div>
            <table id="workTable" class="form table  compact">
                <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Designation</th>
                    <th>Duration</th>
                    <th>Location</th>
                    <th>Remark</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="edit_dependent_work_container">
                @php $i=0; @endphp
                @if(count($dependentWork) > 0)
                @foreach($dependentWork as $work)
                    <tr class="remove-dependent-work removeRow{{$i}}">
                        <td>
                            <input type="text" name="work[{{$i}}][company]" class="form-control input-sm"
                                   id="company" value="{{$work->company or ''}}">
                        </td>
                        <td>
                            <input type="text" name="work[{{$i}}][designation]" class="form-control input-sm"
                                   id="designation" value="{{$work->designation or ''}}">
                        </td>
                        <td>
                            <input type="text" name="work[{{$i}}][duration]" class="form-control input-sm"
                                   id="duration" value="{{$work->duration or ''}}">
                        </td>
                        <td>
                            <input type="text" name="work[{{$i}}][location]" class="form-control input-sm"
                                   id="location" value="{{$work->location or ''}}">
                        </td>
                        <td>
                            <input type="text" name="work[{{$i}}][remark]" class="form-control input-sm"
                                   id="remark" value="{{$work->remark or ''}}">
                        </td>
                        <td>
                            <button value="Remove " type="button" class="btn btn-danger btn-sm"
                                    onclick="deleteRowDependentWork('removeRow{{$i}}')">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                    @php $i++; @endphp
                @endforeach
                    @else
                    <tr>
                        <td>
                            <input type="text" name="work[0][company]" class="form-control input-sm"
                                   id="dependent_company">
                        </td>
                        <td>
                            <input type="text" name="work[0][designation]"
                                   class="form-control input-sm"
                                   id="dependent_designation">
                        </td>
                        <td>
                            <input type="text" name="work[0][duration]" class="form-control input-sm"
                                   id="dependent_duration">
                        </td>
                        <td>
                            <input type="text" name="work[0][location]" class="form-control input-sm"
                                   id="dependent_location">
                        </td>
                        <td>
                            <input type="text" name="work[0][remark]" class="form-control input-sm"
                                   id="dependent_remark">
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>

            <div class="col-md-12 form-group" align="right">
                <button type="button" class="btn btn-primary" id="edit-dependent-work"
                        style="margin: 20px 10px 20px 0">Update
                </button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
