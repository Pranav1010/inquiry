<div class="box-body">
    <div class="form-group">
        <form action="{{url('/inquiries')}}" method="POST" id="editDependentTravelHistoryForm">
            {{ csrf_field() }}
            <input type="hidden" name="dependentInquiryId" class="inquiryDependentId" id="dependentInquiryId">
            <input type="hidden" id="inquiryFormId" value="{{$users->id}}">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Country</label>
                        <input type="text" class="form-control" name="country" id="edit_dependent_tableCountry">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Program</label>
                        <input type="text" class="form-control" name="program" id="edit_dependent_program">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Status</label><br>
                        <select name="status" class="form-control" id="edit_dependent_status">
                            <option value="">Select</option>
                            <option>Approved</option>
                            <option>Declined</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Year</label>
                        <input type="text" class="form-control" name="year" id="edit_dependent_year2">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Duration</label>
                        <input type="text" class="form-control" name="dependentYearDuration" id="edit_dependent_yearDuration">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea name="dependent_remarks" class="form-control" rows="1" id="edit_dependent_remark1"></textarea>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group pull-right" style="margin-top: 20px;">
                        <button type="button" class="btn btn-info" id="edit_dependent_addTravel">Add</button>
                    </div>
                </div>
            </div>

            <!-- ************************* data table ************************************ -->
            <div class="dependent-travel-edit-table-box">
                <table id="travel" class="table table-striped table-bordered"
                       cellspacing="0"
                       width="100%">
                    <thead>
                    <tr>
                        <th>Country</th>
                        <th>Program</th>
                        <th>Status</th>
                        <th>Year</th>
                        <th>Duration</th>
                        <th>Remark</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="edit-travelData">
                    @php $i=0; @endphp
                    @if(count($dependentTravel))
                        @foreach($dependentTravel as $travel)
                            <tr class="remove-dependent-edit-travel removeRow{{$i}}">
                                <td>{{$travel->country or ""}}
                                    <input type="hidden" name="travel[{{$i}}][country]" value="{{$travel->country or ""}}">
                                </td>
                                <td>{{$travel->program or ""}}
                                    <input type="hidden" name="travel[{{$i}}][program]" value="{{$country->program or ""}}">
                                </td>
                                <td>{{$travel->status or ""}}
                                    <input type="hidden" name="travel[{{$i}}][status]" value="{{$country->status or ""}}">
                                </td>
                                <td>{{$travel->year or ""}}
                                    <input type="hidden" name="travel[{{$i}}][year]" value="{{$country->year or ""}}">
                                </td>
                                <td>{{$travel->duration or ""}}
                                    <input type="hidden" name="travel[{{$i}}][duration]" value="{{$country->duration or ""}}">
                                </td>
                                <td>{{$travel->remark or ""}}
                                    <input type="hidden" name="travel[{{$i}}][remark]" value="{{$country->remark or ""}}">
                                </td>
                                <td>
                                    <button value="Remove " class="btn btn-danger btn-sm" id="removeRow' + count + '"
                                            onclick="deleteRowTravelDependent('removeRow{{$i}}')" type="button">
                                        <i class="fa fa-minus" aria-hidden="true"> </i>
                                    </button>
                                </td>
                                @php $i++; @endphp
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- *************************end data table ********************************* -->
            <div class="col-md-12 form-group" align="right">
                <button type="button" class="btn btn-primary" id="edit-travel-dependent"
                        style="margin: 20px 10px 20px 0">Update
                </button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
