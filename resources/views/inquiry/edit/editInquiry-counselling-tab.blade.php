{{--@if(isset($counsellingSummary[0]))--}}
<div class="box-body">
    <div class="form-group">
        <form action="{{url('/inquiries')}}" method="POST" id="editCounsellingSummaryForm">
            {{ csrf_field() }}
            <input type="hidden" name="inquiryId" class="inquiryUserId">
            <input type="hidden" id="inquiryFormId" value="{{$users->id}}">

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Country</label>
                                <select name="country_id" class="form-control" id="country_id">
                                    @foreach(App\Country::all() as $country)
                                        <option value="{{$country->id}}" @if(isset($counsellingSummary[0])) @if($country->id == $counsellingSummary[0]->country_id) {{'selected'}} @endif @endif>{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program_id" class="form-control" id="program_id">
                                    @foreach(App\Course::groupBy('name')->get() as $course)
                                        <option value="{{$course->id}}" @if(isset($counsellingSummary[0])) @if($course->id == $counsellingSummary[0]->program_id) {{'selected'}} @endif @endif>{{$course->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Suggestion</label><br>
                        <textarea name="suggestion" class="form-control" style="height: 109px;">@if(isset($counsellingSummary[0])) {{$counsellingSummary[0]->suggestion or ""}}@endif</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea name="remark" class="form-control" style="height: 109px;">@if(isset($counsellingSummary[0])) {{$counsellingSummary[0]->remark or ""}} @endif</textarea>
                    </div>
                </div>
            </div>

            <div class="col-md-12 form-group" align="right">
                <button type="button" class="btn btn-primary" id="edit-counselling"
                        style="margin: 20px 10px 20px 0">Update
                </button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
{{--@else
    <div class="box-body">
        <div class="form-group">
            <form action="{{url('/inquiries')}}" method="POST" id="editCounsellingSummaryForm">
                {{ csrf_field() }}
                <input type="hidden" name="inquiryId" class="inquiryUserId">
                <input type="hidden" id="inquiryFormId">

                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Country</label>
                                    <select name="country_id" class="form-control" id="country_id">
                                        @foreach(App\Country::all() as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Program</label>
                                    <select name="program_id" class="form-control" id="program_id">
                                        @foreach(App\Course::groupBy('name')->get() as $course)
                                            <option value="{{$course->id}}">{{$course->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Suggestion</label><br>
                            <textarea name="suggestion" class="form-control" style="height: 109px;"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea name="remark" class="form-control" style="height: 109px;"></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-1 form-group">
                        <label>Country:</label>
                    </div>
                    <div class="col-md-5 form-group">
                        <select name="country_id" class="form-control" id="country_id">
                            @foreach(App\Country::all() as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-1 form-group">
                        <label>Program:</label>
                    </div>
                    <div class="col-md-5 form-group">
                        <select name="program_id" class="form-control" id="program_id">
                            @foreach(App\Course::groupBy('name')->get() as $course)
                                <option value="{{$course->id}}">{{$course->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-1 form-group">
                        <label>Suggestion:</label>
                    </div>
                    <div class="col-md-5 form-group">
                        <textarea name="suggestion" class="form-control" rows="3"></textarea>
                    </div>
                    <div class="col-md-1 form-group">
                        <label>Remarks:</label>
                    </div>
                    <div class="col-md-5 form-group">
                        <textarea name="remark" class="form-control" rows="3"></textarea>
                    </div>
                </div>

                <div class="col-md-12 form-group" align="right">
                    <button type="button" class="btn btn-primary" id="edit-counselling"
                            style="margin: 20px 10px 20px 0">Update
                    </button>
                    <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endif--}}
