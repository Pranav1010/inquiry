<div class="box-body dependent-detail">
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <form action="{{url('/inquiries')}}" method="POST" id="editDependentDetailForm">
                    {{ csrf_field() }}
                    <input type="hidden" id="inquiryFormId" value="{{$users->id}}" name="inquiryFormId">
                    {{--<input type="hidden" name="inquiryId"--}}
                    {{--value="{{App\InquiryWiseUser::with('userInquiry')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first()->id}}">--}}
                    <input type="hidden" name="dependentInquiryId" class="inquiryUserId">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="dependentName" value="{{$dependentDetail->name or ''}}">
                                <span class="help-block name" style="color: red" id="name0"></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Birth Date</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control datepicker" id="dependentDate" name="dependentBirthDate" value="@if(isset($dependentDetail->birth_date)) {{date("d-m-Y", strtotime($dependentDetail->birth_date))}} @else {{""}} @endif">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Mobile</label>
                                <input type="text" class="form-control" name="mobile" value="{{$dependentDetail->mobile or ''}}">
                                <span class="help-block mobile" style="color: red" id="mobile0"></span>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" value="{{$dependentDetail->email or ''}}">
                                <span class="help-block email" style="color: red" id="email0"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Children</label>
                                <input type="number" name="children" placeholder="Children" class="form-control input-sm" min=0 id="children1" value="{{$dependentDetail->children or ''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row" id = "child-info-title" style="display: none">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h3 class="box-title">Children Details</h3>
                            </div>
                        </div>
                    </div>

                    @php $i=1; @endphp
                    <input type="hidden" id="childCount" value="{{count($dependentChild)}}">

                    <div class="child-info1">
                    @if($dependentChild != "")
                        @foreach($dependentChild as $child)
                            <div class="removeRow{{$i}}">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="child_name[]" id="childName" value="{{$child->name or ''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <div class="input-group date">
                                                <input type="text" class="form-control datepicker" name="birth_date[]" value="@if(isset($child->birth_date)) {{date("d-m-Y", strtotime($child->birth_date))}} @else {{""}} @endif">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Age</label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="number" name="age_yr[]" placeholder="Year" class="form-control input-sm" min=0 id="ageYr" value="{{$child->age_in_year or 0}}">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="number" name="age_mnth[]" placeholder="Month" class="form-control input-sm" min="0" id="ageMnth" value="{{$child->age_in_month or 0}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <button value="Remove" style="margin-top: 20px" class="btn btn-danger btn-sm pull-right" id="removeRow{{$i}}" onclick="deleteRowChildren('removeRow{{$i}}')" type="button">
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @php $i++; @endphp
                        @endforeach
                    @endif
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group pull-right">
                                <button type="button" class="btn btn-primary" id="edit-dependent-detail" style="margin: 20px 10px 20px 0">Update</button>
                                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#dependent_tab1" data-toggle="tab">Academic</a></li>
                        <li><a href="#dependent_tab2" data-toggle="tab">Exam</a></li>
                        <li><a href="#dependent_tab3" data-toggle="tab">Work Experience</a></li>
                        <li><a href="#dependent_tab4" data-toggle="tab">Travel History</a></li>
                    </ul>
                    <div class="tab-content">
                        {{--Academic Tab--}}
                        <div id="dependent_tab1" class="tab-pane active">
                            @include('inquiry.edit.edit-dependent-academic')
                        </div>
                        {{--Exam Tab--}}
                        <div id="dependent_tab2" class="tab-pane">
                            @include('inquiry.edit.edit-dependent-exam')
                        </div>
                        {{--Work Experience Tab--}}
                        <div id="dependent_tab3" class="tab-pane">
                            @include('inquiry.edit.edit-dependent-work')
                        </div>
                        {{--Travel History Tab--}}
                        <div id="dependent_tab4" class="tab-pane">
                            @include('inquiry.edit.edit-dependent-travel')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ************************************************************************************************ -->

    <!-- **************************************************************************************************** -->
</div>


