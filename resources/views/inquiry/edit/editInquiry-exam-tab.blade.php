<div class="box-body">
    <div class="form-group exam-marks">
        <form action="{{url('/inquiries')}}" method="POST" name="examForm" id="editExamInformation">
            {{ csrf_field() }}
            <input type="hidden" name="formName" value="examForm">
            <input type="hidden" id="inquiryFormId" class="inquiryFormId" value="{{$users->id}}">

            <div id="examMarks">
                <div class="row">
                    <div class="col-md-12">
                        <label> IELTS:</label>
                        <input type="hidden" name="exam[0][exam_type]" value="IELTS">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[0][listening]" placeholder="Listening" id="listening1" class="form-control input-sm" value="{{ $examData['IELTS']->listening or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[0][reading]" placeholder="Reading" id="reading1" class="form-control input-sm" value="{{ $examData['IELTS']->reading or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[0][writing]" placeholder="Writing" id="writing1" class="form-control input-sm" value="{{ $examData['IELTS']->writing or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[0][speaking]" placeholder="Speaking" id="speaking1" class="form-control input-sm" value="{{ $examData['IELTS']->speaking or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[0][overall]" placeholder="Over All" id="overall1" class="form-control input-sm" value="{{ $examData['IELTS']->overall or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[0][year]" placeholder="Exam Year" id="examYear"  class="form-control input-sm" value="{{ $examData['IELTS']->year or '' }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label>PTE</label>
                        <input type="hidden" name="exam[1][exam_type]" value="PTE">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][listening]" placeholder="Listening" id="listening2" class="form-control input-sm" value="{{ $examData['PTE']->listening or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][reading]" placeholder="Reading" id="reading2" class="form-control input-sm" value="{{ $examData['PTE']->reading or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][writing]" placeholder="Writing" id="writing2" class="form-control input-sm" value="{{ $examData['PTE']->writing or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][speaking]" placeholder="Speaking" id="speaking2" class="form-control input-sm" value="{{ $examData['PTE']->speaking or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][overall]" placeholder="Over All" id="overall2" class="form-control input-sm" value="{{ $examData['PTE']->overall or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][year]" placeholder="Exam Year" id="examYear2"  class="form-control input-sm" value="{{ $examData['PTE']->year or '' }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label>TOEFL</label>
                        <input type="hidden" name="exam[2][exam_type]" value="TOEFL">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][listening]" placeholder="Listening" id="listening3" class="form-control input-sm" value="{{ $examData['TOEFL']->listening or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][reading]" placeholder="Reading" id="reading3" class="form-control input-sm" value="{{ $examData['TOEFL']->reading or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][writing]" placeholder="Writing" id="writing3" class="form-control input-sm" value="{{ $examData['TOEFL']->writing or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][overall]" placeholder="Over All" id="overall3" class="form-control input-sm" value="{{ $examData['TOEFL']->overall or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][year]" placeholder="Exam Year" id="examYear3"  class="form-control input-sm" value="{{ $examData['TOEFL']->year or '' }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label>GRE</label>
                        <input type="hidden" name="exam[3][exam_type]" value="GRE">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group" style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[3][overall]" placeholder="Over All" id="overall4" class="form-control input-sm" value="{{ $examData['GRE']->overall or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[3][year]" placeholder="Exam Year" id="examYear4"  class="form-control input-sm" value="{{ $examData['GRE']->year or '' }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea name="examRemark" class="form-control" rows="3" value="{{old('remark')}}">
                                {{--{{isset($exams[0]->remark )? $exams[0]->remark : ''}}--}}
                            </textarea>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group pull-right" style="margin-top: 40px; margin-bottom: 0;">
                            <button type="button" class="btn btn-primary" id="edit-exam" style="margin: 20px 10px 20px 0"> Update </button>
                            <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
