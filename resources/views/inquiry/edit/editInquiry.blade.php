<div class="box-body personal-info">
    @include('inquiry.edit.editInquiry-personal-info')
</div>
<!-- end personal info -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Academic</a></li>
        <li><a href="#tab_2" data-toggle="tab">Exam</a></li>
        <li><a href="#tab_3" data-toggle="tab">Work Experience</a></li>
        <li id="dependent"><a href="#tab_4" data-toggle="tab">Dependent Details</a></li>
        <li><a href="#tab_5" data-toggle="tab">Travel History</a></li>
        <li><a href="#tab_6" data-toggle="tab">Counselling Summary</a></li>
    </ul>

    <div class="tab-content">
        <!-- Academic Tab-->
        <div class="tab-pane active" id="tab_1">
            @include('inquiry.edit.editInquiry-academic-tab')
        </div>
        {{--Exam Tab--}}
        <div id="tab_2" class="tab-pane">
            @include('inquiry.edit.editInquiry-exam-tab')
        </div>
        {{--Work Experience Tab--}}
        <div id="tab_3" class="tab-pane">
            @include('inquiry.edit.editInquiry-work-experience-tab')
        </div>
        {{--Dependent Details Tab--}}
        <div id="tab_4" class="tab-pane">
            @include('inquiry.edit.edit-dependent-detail')
        </div>
        {{--Travel History Tab--}}
        <div id="tab_5" class="tab-pane">
            @include('inquiry.edit.editInquiry-travel-history-tab')
        </div>

        <div id="tab_6" class="tab-pane">
            @include('inquiry.edit.editInquiry-counselling-tab')
        </div>
    </div>
</div>