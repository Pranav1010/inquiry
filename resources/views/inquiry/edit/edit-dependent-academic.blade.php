<div class="box-body">
    <div class="form-group">
        <form action="{{url('/inquiries')}}" method="POST" id="editDependentAcademicForm">
            {{ csrf_field() }}
            <input type="hidden" value="{{isset($dependentUserId)?$dependentUserId->id:""}}"
                   name="inquiryDependentUserId" class="inquiryDependentUserId" id="inquiryDependentUserId">
            <div class="pull-right">
                <button type="button" class="btn btn-info btn-sm" id="editDepedentAcademicAdd">
                    Add More Row
                </button>
            </div>
            <table id="dependentAcademicDataTable" class="form table  compact">
                <thead>
                <tr>
                    <th>Exam Passed</th>
                    <th>Institute / University</th>
                    <th>Year Of Passing</th>
                    <th>Board</th>
                    <th>Result</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="dependent_academic_container">
                @php $i=0; @endphp
                @if(count($dependentAcademic) > 0)
                    @foreach($dependentAcademic as $academic)
                        <tr class="remove-dependent-academic removeRow{{$i}}">
                            <td>
                                <input type="text" placeholder="Other" name="dependent_academic[{{$i}}][exam_passed]"
                                       class="form-control input-sm" id="exam_passed"
                                       value="{{$academic->exam_passed or ""}}">
                            </td>
                            <td>
                                <input type="text" name="dependent_academic[{{$i}}][institute]"
                                       class="form-control input-sm"
                                       id="dependent_institute" value="{{$academic->institute or ""}}">
                            </td>
                            <td>
                                <input type="text" name="dependent_academic[{{$i}}][year]" class="form-control input-sm"
                                       id="dependent_year" value="{{$academic->year or ""}}">
                            </td>
                            <td>
                                <input type="text" name="dependent_academic[{{$i}}][board]"
                                       class="form-control input-sm"
                                       id="dependent_board" value="{{$academic->board or ""}}">
                            </td>
                            <td>
                                <input type="text" name="dependent_academic[{{$i}}][result]"
                                       class="form-control input-sm"
                                       id="dependent_result" value="{{$academic->result or ""}}">
                            </td>
                            <td>
                                <button value="Remove " type="button" class="btn btn-danger btn-sm"
                                        onclick="deleteRowDependentAcademic('removeRow{{$i}}')">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                        @php $i++; @endphp
                    @endforeach
                @else
                    {{--{{dd('hiiii')}}--}}
                    <tr>
                        <td>
                            <input type="text" placeholder="Other" name="dependent_academic[0][exam_passed]"
                                   class="form-control input-sm" id="exam_passed">
                        </td>
                        <td>
                            <input type="text" name="dependent_academic[0][institute]"
                                   class="form-control input-sm"
                                   id="dependent_institute">
                        </td>
                        <td>
                            <input type="text" name="dependent_academic[0][year]" class="form-control input-sm"
                                   id="dependent_year">
                        </td>
                        <td>
                            <input type="text" name="dependent_academic[0][board]" class="form-control input-sm"
                                   id="dependent_board">
                        </td>
                        <td>
                            <input type="text" name="dependent_academic[0][result]" class="form-control input-sm"
                                   id="dependent_result">
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="col-md-12 form-group" align="right">
                <button type="button" class="btn btn-primary" id="edit-dependent-academic"
                        style="margin: 20px 10px 20px 0">Update
                </button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
