<div class="box-body">
    <div class="form-group">
        <form action="{{url('/inquiries')}}" method="POST" name="workExperienceForm" id="editWorkExperienceInformation">
            {{ csrf_field() }}
            <input type="hidden" name="inquiryId" class="inquiryUserId">
            <input type="hidden" id="inquiryFormId" value="{{$users->id}}">
            <input type="hidden" name="formName" value="workExperienceForm">
            <div class="pull-right">
                <button type="button" class="btn btn-info btn-sm" id="editWorkExperienceAdd">
                    Add More Row
                </button>
            </div>
            <table id="workTable" class="form table  compact">
                <thead>
                <tr>
                    <th>Company Name</th>
                    <th>Designation</th>
                    <th>Duration</th>
                    <th>Location</th>
                    <th>Remark</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="edit_work_experience_container">
                @php $i=0; @endphp
                @if(count($workExperiences) > 0)
                    @foreach($workExperiences as $workExperience)
                        <tr class="remove-work removeRow{{$i}}">
                            <td>
                                <input type="text" name="work[{{$i}}][company]" class="form-control input-sm"
                                       id="company" value="{{$workExperience->company  or ''}}">
                            </td>
                            <td>
                                <input type="text" name="work[{{$i}}][designation]" class="form-control input-sm"
                                       id="designation" value="{{$workExperience->designation  or ''}}">
                            </td>
                            <td>
                                <input type="text" name="work[{{$i}}][duration]" class="form-control input-sm"
                                       id="duration" value="{{$workExperience->duration  or ''}}">
                            </td>
                            <td>
                                <input type="text" name="work[{{$i}}][location]" class="form-control input-sm"
                                       id="location" value="{{$workExperience->location  or ''}}">
                            </td>
                            <td>
                                <input type="text" name="work[{{$i}}][remark]" class="form-control input-sm"
                                       id="remark" value="{{$workExperience->remark  or ''}}">
                            </td>
                            <td>
                                <button value="Remove " type="button" class="btn btn-danger btn-sm"
                                        onclick="deleteRowWorkExperience('removeRow{{$i}}')">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                        @php $i++; @endphp
                    @endforeach
                @else
                    <tr class="remove-work removeRow0">
                        <td>
                            <input type="text" name="work[0][company]" class="form-control input-sm"
                                   id="company">
                        </td>
                        <td>
                            <input type="text" name="work[0][designation]" class="form-control input-sm"
                                   id="designation">
                        </td>
                        <td>
                            <input type="text" name="work[0][duration]" class="form-control input-sm"
                                   id="duration">
                        </td>
                        <td>
                            <input type="text" name="work[0][location]" class="form-control input-sm"
                                   id="location">
                        </td>
                        <td>
                            <input type="text" name="work[0][remark]" class="form-control input-sm"
                                   id="remark">
                        </td>
                        <td>
                            <button value="Remove " type="button" class="btn btn-danger btn-sm"
                                    onclick="deleteRowWorkExperience('removeRow0')">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="col-md-12 form-group" align="right">
                <button type="button" class="btn btn-primary" id="edit-work-experience"
                        style="margin: 20px 10px 20px 0">Update
                </button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
