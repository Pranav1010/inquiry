<div class="box-body">
    <div class="form-group">
        <form action="{{url('/inquiries')}}" method="POST" id="editAcademicForm">
            {{ csrf_field() }}
            <input type="hidden" name="formName" value="academicForm">
            <input type="hidden" name="inquiryId" class="inquiryUserId">
            <input type="hidden" id="inquiryFormId" value="{{$users->id}}">
            <div class="pull-right">
                <button type="button" class="btn btn-info btn-sm" id="editAcademicAdd">
                    Add More Row
                </button>
            </div>
            <table id="academicDataTable" class="form table  compact">
                <thead>
                <tr>
                    <th>Exam Passed</th>
                    <th>Institute / University</th>
                    <th>Year Of Passing</th>
                    <th>Board</th>
                    <th>Result</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="academic_container">
                @php $i=0; @endphp
                @if(count($academics) > 0)
                    @foreach($academics as $academic)
                        <tr class="remove-academic removeRow{{$i}}">
                            <td>
                                <input type="text" placeholder="Other" name="academic[{{$i}}][exam_passed]"
                                       class="form-control input-sm" id="exam_passed"
                                       value="{{$academic->exam_passed or ''}}">
                            </td>
                            <td>
                                <input type="text" name="academic[{{$i}}][institute]" class="form-control input-sm"
                                       id="institute" value="{{$academic->institute or ''}}">
                            </td>
                            <td>
                                <input type="text" name="academic[{{$i}}][year]" class="form-control input-sm" id="year"
                                       value="{{$academic->year}}">
                            </td>
                            <td>
                                <input type="text" name="academic[{{$i}}][board]" class="form-control input-sm"
                                       id="board" value="{{$academic->board}}">
                            </td>
                            <td>
                                <input type="text" name="academic[{{$i}}][result]" class="form-control input-sm"
                                       id="result" value="{{$academic->result}}">
                            </td>
                            <td>
                                <button value="Remove " type="button" class="btn btn-danger btn-sm"
                                        onclick="deleteRowAcademic('removeRow{{$i}}')">
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                        @php $i++; @endphp
                    @endforeach
                @else
                    <tr class="remove-academic removeRow0">
                        <td>
                            <input type="text" placeholder="Other" name="academic[0][exam_passed]"
                                   class="form-control input-sm" id="exam_passed">
                        </td>
                        <td>
                            <input type="text" name="academic[0][institute]" class="form-control input-sm"
                                   id="institute">
                        </td>
                        <td>
                            <input type="text" name="academic[0][year]" class="form-control input-sm" id="year">
                        </td>
                        <td>
                            <input type="text" name="academic[0][board]" class="form-control input-sm" id="board">
                        </td>
                        <td>
                            <input type="text" name="academic[0][result]" class="form-control input-sm" id="result">
                        </td>
                        <td>
                            <button value="Remove " type="button" class="btn btn-danger btn-sm"
                                    onclick="deleteRowAcademic('removeRow0')">
                                <i class="fa fa-minus" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div class="col-md-12 form-group" align="right">
                <button type="button" class="btn btn-primary" id="edit-academic"
                        style="margin: 20px 10px 20px 0">Update
                </button>
                <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
            </div>
        </form>
    </div>
</div>
