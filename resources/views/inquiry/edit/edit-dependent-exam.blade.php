<div class="box-body">
    <div class="form-group exam-marks">
        <form action="{{url('/inquiries')}}" method="POST" name="examForm" id="editDependentExamInformation">
            {{ csrf_field() }}
            <input type="hidden" name="formName" value="examForm">
            <input type="hidden" value="{{isset($dependentUserId)?$dependentUserId->id:''}}"
                   name="inquiryDependentUserId" class="inquiryDependentUserId" id="inquiryDependentUserId">


            <div class="form-group" id="examMarks">

                <div class="row">
                    <div class="col-sm-12">
                        <label>IELTS</label>
                        <input type="hidden" name="dependent_exam[0][exam_type]" value="IELTS">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="dependent_exam[0][listening]"
                                   placeholder="Listening" id="dependent_listening1" class="form-control input-sm"
                                   value="{{ $dependentExamData['IELTS']->listening or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="dependent_exam[0][reading]"
                                   placeholder="Reading" id="dependent_reading1" class="form-control input-sm"
                                   value="{{ $dependentExamData['IELTS']->reading or ''}}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="dependent_exam[0][writing]"
                                   placeholder="Writing" id="dependent_writing1" class="form-control input-sm"
                                   value="{{ $dependentExamData['IELTS']->writing or ''}}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="dependent_exam[0][speaking]"
                                   placeholder="Speaking" id="dependent_speaking1" class="form-control input-sm"
                                   value="{{ $dependentExamData['IELTS']->speaking or ''}}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="dependent_exam[0][overall]"
                                   placeholder="Over All" id="dependent_overall1" class="form-control input-sm"
                                   value="{{ $dependentExamData['IELTS']->overall or ''}}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="dependent_exam[0][year]"
                                   placeholder="Exam Year" id="dependent_examYear" class="form-control input-sm"
                                   value="{{ $dependentExamData['IELTS']->year or ''}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label>PTE</label>
                        <input type="hidden" name="exam[1][exam_type]" value="PTE">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][listening]" placeholder="Listening"
                                   id="dependent_listening1" class="form-control input-sm"
                                   value="{{ $dependentExamData['PTE']->listening or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][reading]" placeholder="Reading"
                                   id="dependent_reading1" class="form-control input-sm"
                                   value="{{ $dependentExamData['PTE']->reading or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][writing]" placeholder="Writing"
                                   id="dependent_writing1" class="form-control input-sm"
                                   value="{{ $dependentExamData['PTE']->writing or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][speaking]" placeholder="Speaking"
                                   id="dependent_speaking1" class="form-control input-sm"
                                   value="{{ $dependentExamData['PTE']->speaking or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][overall]" placeholder="Over All"
                                   id="dependent_overall1" class="form-control input-sm"
                                   value="{{ $dependentExamData['PTE']->overall or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[1][year]" placeholder="Exam Year"
                                   id="dependent_examYear" class="form-control input-sm"
                                   value="{{ $dependentExamData['PTE']->year or '' }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label>TOEFL</label>
                        <input type="hidden" name="exam[2][exam_type]" value="TOEFL">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][listening]" placeholder="Listening"
                                   id="dependent_listening1" class="form-control input-sm"
                                   value="{{ $dependentExamData['TOEFL']->listening or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][reading]" placeholder="Reading"
                                   id="dependent_reading1" class="form-control input-sm"
                                   value="{{ $dependentExamData['TOEFL']->reading or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][writing]" placeholder="Writing"
                                   id="dependent_writing1" class="form-control input-sm"
                                   value="{{ $dependentExamData['TOEFL']->writing or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group"
                             style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][overall]" placeholder="Over All"
                                   id="dependent_overall1" class="form-control input-sm"
                                   value="{{ $dependentExamData['TOEFL']->overall or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[2][year]" placeholder="Exam Year"
                                   id="dependent_examYear" class="form-control input-sm"
                                   value="{{ $dependentExamData['TOEFL']->year or '' }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <label>GRE</label>
                        <input type="hidden" name="exam[3][exam_type]" value="GRE">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group"
                             style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group"
                             style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group"
                             style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group"
                             style="text-align: center; padding: 5px 0; margin-bottom: 0; background: #e8e8e8;">
                            <span>- N/A -</span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[3][overall]" placeholder="Over All"
                                   id="dependent_overall1" class="form-control input-sm"
                                   value="{{ $dependentExamData['GRE']->overall or '' }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" contenteditable="true" name="exam[3][year]" placeholder="Exam Year"
                                   id="dependent_examYear" class="form-control input-sm"
                                   value="{{ $dependentExamData['GRE']->year or '' }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea name="examRemark" class="form-control"
                                      rows="3">@if(isset($dependentExam[0]) && isset($dependentExam[0]->remark)) {{$dependentExam[0]->remark}} @endif </textarea>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group pull-right" style="margin-top: 40px; margin-bottom: 0;">
                            <button type="button" class="btn btn-primary" id="edit-dependent-exam"
                                    style="margin: 20px 10px 20px 0"> Update
                            </button>
                            <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
