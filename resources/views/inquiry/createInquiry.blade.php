<div class="box-body personal-info">
    @include('inquiry.createInquiry-personal-info')
</div>
<!-- end personal info -->
{{-- <div class="tabs" id="mainTabs" style="display: none;" > --}}
<div class="nav-tabs-custom" id="mainTabs" @if(!isset($inquiry)) @endif>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#academic_tab" data-toggle="tab">Academic</a></li>
        <li><a href="#exam_tab" data-toggle="tab" id="exam-tab">Exam</a></li>
        <li><a href="#work_tab" data-toggle="tab">Work Experience</a></li>
        <li><a href="#dependent_tab" data-toggle="tab">Spouse Details</a></li>
        <li><a href="#travel_tab" data-toggle="tab" id="travel-tab">Travel History</a></li>
        {{--<li><a href="#tab_6" data-toggle="tab">Counselling Summary</a></li>--}}
    </ul>
    <form action="{{url('/inquiries')}}" method="POST" id="inquiryDetailsForm">

        {{ csrf_field() }}
        <input type="hidden" name="inquiryId" class="inquiryUserId">

        <div class="tab-content">
            <!-- Academic Tab-->
            <div class="tab-pane active" id="academic_tab">
                @include('inquiry.createInquiry-academic-tab')
            </div>
            {{--Exam Tab--}}
            <div class="tab-pane" id="exam_tab">
                @include('inquiry.createInquiry-exam-tab')
            </div>
            {{--Work Experience Tab--}}
            <div class="tab-pane" id="work_tab">
                @include('inquiry.createInquiry-work-experience-tab')
            </div>
            {{--Dependent Details Tab--}}
            <div class="tab-pane" id="dependent_tab">
                @include('inquiry.dependent-detail')
            </div>
            {{--Travel History Tab--}}
            <div class="tab-pane" id="travel_tab">
                @include('inquiry.createInquiry-travel-history')
            </div>

            {{--<div class="tab-pane" id="exam_tab">
                @include('inquiry.createInquiry-counselling-summary-tab')
            </div>--}}
        </div>

    </form>

</div>