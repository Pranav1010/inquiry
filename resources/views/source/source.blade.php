<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title" data-widget="collapse">Add New</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <form action="{{url('/sources')}}" method="post" pjax-container>
            {{ csrf_field() }}
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label >Source <span class="text-red">*</span></label>
                            <input type="text" id="name" name="name" value="" class="form-control name" placeholder="Source" required autofocus/>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="pull-right" style="padding-top:4px;">
                            <br>
                            <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form >
    </div>
</div>
