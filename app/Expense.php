<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
        'branch_id',
        'payment_date',
        'category',
        'payee',
        'amount',
        'payment_mode',
        'remarks'
    ];
}
