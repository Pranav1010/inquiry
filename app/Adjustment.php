<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adjustment extends Model
{
    protected $fillable = [
        'student_id',
        'branch_id',
        'course_id',
        'candidate_details',
        'category',
        'total_amount',
        'status',
        'remarks'
    ];

}
