<?php

function isRole($role)
{
    if(Admin::user()) {
        return Admin::user()->isRole($role);
    } else {
        return false;
    }
}