<?php

namespace App;

use App\Country;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name', 'country_id'
    ];

    public function country(){
        return $this->belongsTo(Country::class,'country_id');
    }

}
