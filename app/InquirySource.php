<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InquirySource extends Model
{
    protected $fillable = [
        'name'
    ];

}
