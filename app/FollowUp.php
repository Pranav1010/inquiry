<?php

namespace App;

use Carbon\Carbon;
use Encore\Admin\Auth\Database\Administrator;
use Illuminate\Database\Eloquent\Model;

class FollowUp extends Model
{
    protected $fillable = [
        'inquiry_wise_user_id', 'interest_level', 'status', 'action', 'next_follow_up', 'comments'
    ];

    protected $dates = ['next_follow_up', 'created_at', 'updated_at'];

    public function inquiryWiseUser()
    {
        return $this->belongsTo(InquiryWiseUser::class);
    }

    public function user()
    {
        return $this->belongsTo(Administrator::class);
    }

    public static function getUserFollowUps($userId, $startDate = false, $endDate = false)
    {

        $inquiries = VisaInquiry::where('status', '=', '0')->pluck('id')->toArray();
        
        if($userId != 1){

            $inquiry_wise_user = InquiryWiseUser::whereIn('inquiry_id', $inquiries)->where('user_id', '=', $userId)->pluck('id')->toArray();
        	$followUpIds = FollowUp::select(\DB::raw("max(id) as id"))->whereIn("inquiry_wise_user_id", $inquiry_wise_user)->groupBy("inquiry_wise_user_id")->get();

        }else{

            $inquiry_wise_user = InquiryWiseUser::whereIn('inquiry_id', $inquiries)->pluck('id')->toArray();
        	$followUpIds = FollowUp::select(\DB::raw("max(id) as id"))->whereIn('inquiry_wise_user_id', $inquiry_wise_user)->groupBy("inquiry_wise_user_id")->get();

        }
        if($startDate && $endDate){
            
            return FollowUp::whereIn('id',$followUpIds)->whereDate('next_follow_up', '>=', $startDate)->whereDate('next_follow_up', '<=', $endDate)->orderBy("next_follow_up")->get();
        
        } else {

            return FollowUp::whereIn('id',$followUpIds)->orderBy("next_follow_up")->get();

        }
    }

    public static function getDateWiseFollowUps($userId, $date){

        $today = date('Y-m-d');
        $inquiries = VisaInquiry::where('status', '=', '0')->pluck('id')->toArray();

        if($userId != 1){

            $inquiry_wise_user = InquiryWiseUser::whereIn('inquiry_id', $inquiries)->where('user_id', '=', $userId)->pluck('id')->toArray();
            $followUpIds = FollowUp::select(\DB::raw("max(id) as id"))->whereIn("inquiry_wise_user_id", $inquiry_wise_user)->groupBy("inquiry_wise_user_id")->get();

        }else{

            $inquiry_wise_user = InquiryWiseUser::whereIn('inquiry_id', $inquiries)->pluck('id')->toArray();
            $followUpIds = FollowUp::select(\DB::raw("max(id) as id"))->whereIn('inquiry_wise_user_id', $inquiry_wise_user)->groupBy("inquiry_wise_user_id")->get();

        }

        return FollowUp::whereIn('id',$followUpIds)->whereDate('next_follow_up', '=', $date)->orderBy("next_follow_up")->get();

    }

    public static function getNumberOfFollowUps($inquiry_id){

        $inquiriesWithUsers = InquiryWiseUser::where('inquiry_id', '=', $inquiry_id)->pluck('id')->toArray();
        return FollowUp::whereIn('inquiry_wise_user_id', $inquiriesWithUsers)->count();

    }

    public static function getDaysFromStartDateOfInquiry($creationDate){

        $date1=date_create($creationDate->format('Y-m-d'));
        $date2=date_create(Carbon::now('Asia/Kolkata')->format('Y-m-d'));
        $diff=date_diff($date1,$date2);
        return $diff->format("%a");

    }
}
