<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryProgram extends Model
{
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function program()
    {
        return $this->belongsTo(Course::class);
    }
}
