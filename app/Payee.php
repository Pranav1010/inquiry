<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payee extends Model
{
    protected $fillable = [
        'payee',
        'category',
        'total_amount',
        'paid_amount',
        'due_amount',
        'primary_date',
        'last_date',
        'remarks'
    ];
}
