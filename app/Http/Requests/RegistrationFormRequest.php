<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|max:255|unique:users,username|',
            'password' => 'required|min:6|confirmed',
            'branch_id' => 'integer|min:1',
            'designation_id' => 'integer|min:1',
            'role_id' => 'integer|min:1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'branch_id.min' => 'Please select branch.',
            'designation_id.min' => 'Please select designation',
            'role_id.min' => 'Please select role'
        ];
    }
}
