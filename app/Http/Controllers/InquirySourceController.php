<?php

namespace App\Http\Controllers;

use App\InquirySource;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class InquirySourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sources = InquirySource::all();
        return view('source.source', compact('sources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'unique:inquiry_sources,name',
                'required'
            ]

        ]);

        InquirySource::create(request(['name']));

        return redirect()->back()->with('data', ['type' => 'success', 'message' => 'Added Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    public function edit(InquirySource $source)
    {
        return view('source.editSource', compact('source'));
    }


    public function update(InquirySource $source)
    {
        $this->validate(request(), [
            'name' => [
                'required',
                Rule::unique('inquiry_sources')->ignore($source->id)
            ],

        ]);

        $source->name = request()->name;

        $source->save();

        return redirect('/sources')->with('data', ['type' => 'success', 'message' => 'Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(InquirySource $source)
    {
        $source->delete();
        return redirect('/sources')->with('data', ['type' => 'error', 'message' => 'Deleted Successfully']);

    }
}
