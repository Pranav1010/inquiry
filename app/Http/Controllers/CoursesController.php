<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CoursesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::all();
        return view('course.course', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createCourse');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => [
                'required'
            ],
            'country_id' => [
                'required'
            ]
        ]);

        $course = Course::where('country_id', $request->country_id)->where('name', $request->name)->get();

        if ($course->isEmpty()) {

            Course::create(request(['name', 'country_id']));

            return redirect()->back()->with('data', ['type' => 'success', 'message' => 'Added Successfully']);

        } else {

            return redirect()->back()->with('data', ['type' => 'warning', 'message' => 'Program already exists for the country']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Course $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('course.editCourse', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Course $course
     * @return \Illuminate\Http\Response
     */
    public function update(Course $course)
    {
        $this->validate(request(), [
            'name' => [
                'required',
            ],
        ]);

        $courses = Course::where('country_id', request()->country_id)->where('name', request()->name)->where('id', '!=', $course->id)->first();


        if (!isset($courses)) {
            $course->name = request()->name;
            $course->country_id = request()->country_id;

            $course->save();
            return redirect('/courses')->with('data', ['type' => 'success', 'message' => 'Updated Successfully']);


        } else {

            return redirect()->back()->with('data', ['type' => 'warning', 'message' => 'Program already exists for the country']);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Course $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return redirect('/courses');
    }
}
