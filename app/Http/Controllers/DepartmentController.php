<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DepartmentController extends Controller
{

    public function index()
    {
        $departments = Department::all();
        return view('department.department', compact('departments'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'unique:departments,name',
                'required'
            ]

        ]);

        Department::create(request(['name']));

        return redirect()->back()->with('data', ['type' => 'success', 'message' => 'Added Successfully']);
    }


    public function show($id)
    {

    }

    public function edit(Department $department)
    {
        return view('department.editDepartment', compact('department'));
    }

    public function update(Department $department)
    {
        $this->validate(request(), [
            'name' => [
                'required',
                Rule::unique('departments')->ignore($department->id)
            ],

        ]);

        $department->name = request()->name;

        $department->save();

        return redirect('/departments')->with('data', ['type' => 'success', 'message' => 'Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();
        return redirect('/departments')->with('data', ['type' => 'error', 'message' => 'Deleted Successfully']);

    }
}
