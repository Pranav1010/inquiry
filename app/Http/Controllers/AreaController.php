<?php

namespace App\Http\Controllers;

use App\Area;
use App\City;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::with('city')->get();
        return view('area.area', compact('areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'required'
            ]
        ]);

        $area = Area::where('city_id', $request->city_id)->where('name', $request->name)->get();

        if ($area->isEmpty()) {

            Area::create(request(['city_id', 'name']));
            return redirect()->back()->with('data', ['type' => 'success', 'message' => 'Added Successfully']);
        } else {
            return redirect()->back()->with('data', ['type' => 'warning', 'message' => 'Area already exists for the city']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        return view('area.editArea', compact('area'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Area $area)
    {

        $this->validate(request(), [
            'name' => [
                'required',
            ],
        ]);

        $areas = Area::where('city_id', request()->city_id)->where('name', request()->name)->where('id','!=',$area->id)->first();


        if(!isset($areas))
        {
            $area->city_id = request()->city_id;
            $area->name = request()->name;

            $area->save();
            return redirect('/areas')->with('data', ['type' => 'success', 'message' => 'Updated Successfully']);


        }else{

            return redirect()->back()->with('data', ['type' => 'warning', 'message' => 'Area already exists for the city']);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        $area->delete();
        return redirect('/areas')->with('data', ['type' => 'error', 'message' => 'Deleted Successfully']);
    }
}
