<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuthStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->is('/')){
            if(isRole("guest")) {
                return redirect("/inquiries/create");
            } else if(isRole("receptionist") || isRole("counselor")){
                return redirect("/dashboard");
            }
        }

        return $next($request);
    }
}
