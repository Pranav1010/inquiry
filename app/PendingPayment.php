<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendingPayment extends Model
{
    protected $fillable = [
        'due_date',
        'client_details',
        'course_id',
        'branch_id',
        'installment_no',
        'tax_amount',
        'subtotal_amount',
        'installment_total_amount',
        'received_amount'
    ];
}
