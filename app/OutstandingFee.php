<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutstandingFee extends Model
{
    protected $fillable = [
        'student_fee_id',
        'user_id',
        'remarks'
    ];

    public function studentFee()
    {
        return $this->belongsTo(StudentFee::class);
    }
}
