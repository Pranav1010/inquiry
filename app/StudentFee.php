<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentFee extends Model
{
    protected $fillable = [
        'student_id',
        'installment_no',
        'installment_date',
        'amount',
        'tax',
        'total_amount',
        'received_amount',
        'due_amount',
        'due_date',
        'payment_date',
        'payment_creation_date',
        'payment_method',
        'payment_status',
        'receipt_no',
        'remarks'
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
