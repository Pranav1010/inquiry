<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $guarded = [];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
    
    public function assignor()
    {
        return $this->belongsTo(User::class);
    }

    public function inquiryWiseUser()
    {
        return $this->hasMany(InquiryWiseUser::class);
    }
    
    public function reasons()
    {
    	return $this->hasMany(Reason::class);
    }

    public static function getUserInquiries($userId)
    {
        return Inquiry::where('user_id', '=', $userId)->orderBy('status', 'asc')->orderBy('updated_at', 'desc')->get();
    }

    public static function getUserNames($inquiry_id, $userId)
    {
        if($userId != 1){

            $inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry_id)->where('inquiry_wise_users.user_id', '=', $userId)->pluck('inquiry_wise_users.id')->toArray();

        } else{

            $inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry_id)->pluck('inquiry_wise_users.id')->toArray();

        }

        $userNames = InquiryWiseUser::join('users', 'users.id', '=', 'inquiry_wise_users.user_id')->whereIn('inquiry_wise_users.id', $inquiry_wise_user_id)->pluck('users.name')->toArray();
        return implode(", ", $userNames);
    }

    public static function getNumberOfFollowUps($inquiry_id){

        $inquiriesWithUsers = InquiryWiseUser::where('inquiry_id', '=', $inquiry_id)->pluck('id')->toArray();
        return FollowUp::whereIn('inquiry_wise_user_id', $inquiriesWithUsers)->count();

    }

    public static function getDaysFromStartDateOfInquiry($creationDate){

        $date1=date_create($creationDate->format('Y-m-d'));
        $date2=date_create(Carbon::now('Asia/Kolkata')->format('Y-m-d'));
        $diff=date_diff($date1,$date2);
        return $diff->format("%a");

    }
}
