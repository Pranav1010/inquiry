<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InquiryCounsellingSummary extends Model
{
    public function program()
    {
        return $this->belongsTo(Course::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
