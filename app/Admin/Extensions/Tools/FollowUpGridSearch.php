<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class FollowUpGridSearch extends AbstractTool
{
    protected function script()
    {
        $url = Request::fullUrlWithQuery(['search' => '_search_']);

        return <<<EOT
            var searchTimeout = null;
            var searchInput = $('#followup-search');
            searchInput.on("input", function () {
            
                clearTimeout(searchTimeout);
                searchTimeout = setTimeout(function(){
                
                    var url = "$url".replace("_search_", searchInput.val());
                    $.pjax({container:'#pjax-container', url: url });
                    
                }, 500);
            
            });
EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        return view('vendor.admin.tools.followUpGridSearch');
    }
}