<?php

namespace App\Admin\Controllers;

use App\Country;
use App\Course;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;

class CourseController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('select').select2();
            $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/programs');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/programs');
            "]);

            $content->header('Program');
            //$countries = Country::all();

            $content->row(function(Row $row){
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });

            //$content->row(view('course.course', compact('countries')));
            //$content->row($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Program');

            $content->row(function(Row $row) use ($id){
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid());
            });

            //$content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Program');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Course::class, function (Grid $grid) {

            $grid->column('name','Program');
            $grid->column('country.name','Country');

            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/courses/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });

            $grid->paginate(10);
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Course::class, function (Form $form) {

            $form->text('name')->rules('required');
            $form->select('country_id','Country')->options(Country::all()->pluck('name', 'id'))->rules('required');

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/programs");
            });

            $form->tools(function (Form\Tools $tools) {

                // Disable back btn.
                $tools->disableBackButton();

                // Disable list btn
                $tools->disableListButton();

                // Add a button, the argument can be a string, or an instance of the object that implements the Renderable or Htmlable interface
                //$tools->add('<a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;delete</a>');
            });
        });
    }
}
