<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\InquiryGridSearch;
use App\Admin\Extensions\Tools\FollowUpGridSearch;
use App\Admin\Extensions\Tools\ViewInquiry;
use App\Area;
use App\CountryProgram;
use App\FollowUp;
use App\Inquiry;
use App\InquiryUserHistory;
use App\Reason;
use App\VisaInquiry;
use App\InquiryWiseUser;
use App\InquiryDependentUser;
use App\InquiryAcademic;
use App\InquiryExam;
use App\InquiryWorkExperience;
use App\InquiryTravelHistory;
use App\InquiryCounsellingSummary;
use App\InquiryDependentDetail;
use App\InquiryDependentChildren;
use App\InquiryDependentExam;
use App\InquiryDependentAcademic;
use App\InquiryDependentWorkExperience;
use App\InquiryDependentTravelHistory;

use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Response;

class VisaInquiryController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.datepicker').datepicker({ format: 'mm-dd-yyyy' });
            $('[data-toggle=\"tooltip\"]').tooltip();
            $.getScript('/js/script.js');
            $.getScript('/js/inquiry.js');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Visa Inquiries');
            /*$content->description('description');*/

            $content->body($this->grid());
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('<link/>', {
                   rel: 'stylesheet',
                   type: 'text/css',
                   href: '/css/inquiry.css'
                }).appendTo('head');
            
                $.getScript('/js/inquiry-personal-info.js');
                $.getScript('/js/inquiry-academic.js');
                $.getScript('/js/inquiry-exam.js');
                $.getScript('/js/inquiry-work-experience.js');
                $.getScript('/js/inquiry-travel-history.js');
                $.getScript('/js/inquiry-counselling-summary.js');
                $.getScript('/js/dependent-child.js');
                $.getScript('/js/dependent-academic.js');
                $.getScript('/js/dependent-exam.js');
                $.getScript('/js/dependent-work-experience.js');
                $.getScript('/js/dependent-travel-history.js');
            "]);

            $counselors = Administrator::whereHas("roles", function($query){
                $query->where("slug", "counselor");
            })->get();

            $user_id = Admin::user()->id;

            $content->header('Create Inquiry');
            $content->body(view('inquiry.createInquiry', compact( 'user_id', 'counselors')));
        });
    }

    public function store(Request $request){

        $error = "";
        $userInquiry = "";
        $validate = array(
            'firstName' => 'required',
            'mobile' => 'required | digits:10',
            'course_id' => 'required',
            'inquiryCountry' => 'required',
            'alternate_mobile' => 'numeric | nullable',
            'email' => 'email | nullable',
            'user_id' => 'required'
        );

        $messsages = array(
            'course_id.required' => 'The program field is required',
            'inquiryCountry.required' => 'The country field is required',
            'name.required' => 'The name field is required',
            'user_id.required' => "Please select the counsellor"
        );

        $validateForPersonalInfo = Validator::make($request->all(), $validate, $messsages);

        if ($validateForPersonalInfo->fails()) {

            $error = $validateForPersonalInfo->getMessageBag();
        } else {
            $inquiry = new VisaInquiry();
            $inquiry->first_name = $request->firstName;
            $inquiry->middle_name = $request->middleName;
            $inquiry->last_name = $request->lastName;
            if ($request->birth_date != null) {
                $inquiry->birth_date = date('Y-m-d', strtotime($request->birth_date));
            } else {
                $inquiry->birth_date = null;
            }
            $inquiry->gender = $request->gender;
            $inquiry->mobile = $request->mobile;
            $inquiry->alternate_mobile = $request->alternate_mobile;
            $inquiry->email = $request->email;
            $inquiry->address = $request->address;
            $inquiry->city_id = $request->city;
            $inquiry->area_id = $request->area;
            $inquiry->marital_status = $request->m_status;
            $inquiry->source_id = $request->source;
            $inquiry->friend_name = $request->friendource;
            $inquiry->tag = $request->tagfriend;
            $inquiry->other_source = $request->otherSource;
            $inquiry->branch_id = $request->branch_id;
            $inquiry->remark = $request->remarks;
            $inquiry->save();

            if (isset($request->program)) {
                for ($i = 0; $i < sizeof($request->program); $i++) {
                    $program = new CountryProgram();
                    $program->inquiry_id = $inquiry->id;
                    $program->country_id = $request->country[$i];
                    $program->program_id = $request->program[$i];
                    $program->save();
                }
            } else {
                $program = new CountryProgram();
                $program->inquiry_id = $inquiry->id;
                $program->country_id = $request->inquiryCountry;
                $program->program_id = $request->course_id;
                $program->save();
            }

//            for ($i = 0; $i < sizeof($request->user_id); $i++) {
            $user = new InquiryWiseUser();
            $user->inquiry_id = $inquiry->id;
            $user->user_id = $request->user_id[0];
            $user->save();
//            }
            $userInquiry = InquiryWiseUser::with('userInquiry')->where('inquiry_id', $inquiry->id)->first();

        }
        if ($error != '') {
            return response()->json($error, 500);
        } else {
            return response()->json(array('result' => "success", 'inquiry_id' => $userInquiry->id), 200);
        }
    }

    public function storeInquiryDetails(Request $request){
        dd($request->all());
    }

    public function editInquiryDetails(Request $request, $inquiry_id){
        dd($request->all(), $inquiry_id);
    }

    public function setAcademicInformation(Request $request)
    {


        $academics = $request->academic;
        $inquiry = $request->inquiryId;

        foreach ($academics as $key => $value) {

            if ($value['exam_passed'] == null && $value['institute'] == null && $value['year'] == null && $value['board'] == null && $value['result'] == null) {
                unset($academics[$key]);
            } else {
                $academic = new InquiryAcademic();
                $academic->inquiry_id = $inquiry;
                $academic->exam_passed = $value['exam_passed'];
                $academic->institute = $value['institute'];
                $academic->year = $value['year'];
                $academic->board = $value['board'];
                $academic->result = $value['result'];
                $academic->result = $value['backlog'];
                $academic->save();
            }

        }
        return "success";
    }

    public function setExamInformation(Request $request)
    {

        $exams = $request->exam;
        $inquiry = $request->inquiryId;
        $remark = $request->examRemark;


        if ($inquiry == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryExam();

                    $exam->inquiry_id = $inquiry;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($value['result'])) {
                        $exam->result_status = $value['result'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }

            return "success";

        }
    }

    public function setWorkExperienceInformation(Request $request)
    {

        $workExperiences = $request->work;
        $inquiry = $request->inquiryId;

        if ($inquiry == null) {
            return "error";
        } else {
            foreach ($workExperiences as $key => $value) {

                if ($value['company'] == null && $value['designation'] == null && $value['duration'] == null && $value['location'] == null && $value['remark'] == null) {
                    unset($workExperiences[$key]);
                } else {
                    $workExperience = new InquiryWorkExperience();

                    $workExperience->inquiry_id = $inquiry;
                    $workExperience->company = $value['company'];
                    $workExperience->designation = $value['designation'];
                    $workExperience->durationYears = $value['durationYears'];
                    $workExperience->durationMonths = $value['durationMonths'];
                    $workExperience->location = $value['location'];
                    $workExperience->remark = $value['remark'];
                    $workExperience->save();
                }

            }
            return "success";

        }

    }

    public function setTravelHistoryInformation(Request $request)
    {

        $travelHistory = $request->travel;
        $inquiry = $request->inquiryId;


        if ($inquiry == null) {
            return "error";
        } else {

            if (isset($travelHistory) && $request->travelCountry == null && $request->travelVisaType == null && $request->travelVisaStatus == null && $request->travelApplicationYear == null && $request->travelYearDuration == null  && $request->travelMonthDuration == null && $request->remarks == null) {
                return response()->json("noData", 500);
            } else {
                $travelHistory_first = new InquiryTravelHistory();
                $travelHistory_first->inquiry_id = $inquiry;
                $travelHistory_first->country = $request->travelCountry;
                $travelHistory_first->program = $request->travelVisaType;
                $travelHistory_first->status = $request->travelVisaStatus;
                $travelHistory_first->year = $request->travelApplicationYear;
                $travelHistory_first->durationYears = $request->travelYearDuration;
                $travelHistory_first->durationMonths = $request->travelMonthDuration;
                $travelHistory_first->remark = $request->travelRemarks;
                $travelHistory_first->save();


                foreach ($travelHistory as $key => $value) {

                    if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                        unset($travelHistory[$key]);
                    } else {

                        if (isset($request->travel)) {

                            $travel = new InquiryTravelHistory();
                            $travel->inquiry_id = $inquiry;
                            $travel->country = $request->travelCountry;
                            $travel->program = $request->travelVisaType;
                            $travel->status = $request->travelVisaStatus;
                            $travel->year = $request->travelApplicationYear;
                            $travel->durationYears = $request->travelYearDuration;
                            $travel->durationMonths = $request->travelMonthDuration;
                            $travel->remark = $request->travelRemarks;

                        }/* else {

                            $travel = new InquiryTravelHistory();
                            $travel->inquiry_id = $inquiry;
                            $travel->country = $request->country;
                            $travel->program = $request->program;
                            $travel->status = $request->status;
                            $travel->year = $request->year;
                            $travel->duration = $request->yearDuration;
                            $travel->remark = $request->remarks;
                            $travel->save();

                        }*/
                    }
                }
            }

            return "success";
        }

    }

    public function setCounsellingSummaryInformation(Request $request)
    {
        $summary = new InquiryCounsellingSummary();
        $summary->inquiry_id = $request->inquiryId;
        $summary->country_id = $request->country_id;
        $summary->program_id = $request->program_id;
        $summary->suggestion = $request->suggestion;
        $summary->remark = $request->remark;
        $summary->save();

        return "success";

    }

    public function setDependentInformation(Request $request)
    {
        $error = "";

        $validate = array(
            'dependentMobile' => 'unique:inquiry_dependent_details|digits:10 | nullable',
            'dependentEmail' => 'email | nullable');

        $messsages = array(
            'dependentMobile.digits' => 'The mobile number must be of 10 digits',
            'dependentMobile.unique' => 'The mobile number is already taken',
        );

        $validateForDependentInfo = Validator::make($request->all(), $validate, $messsages);

        if ($validateForDependentInfo->fails()) {

            $error = $validateForDependentInfo->getMessageBag();

        } else {

            $dependent = new InquiryDependentDetail();
            $dependent->inquiry_id = $request->inquiryId;
            $dependent->name = $request->dependentName;

            if ($request->dependentBirthDate != null) {
                $dependent->birth_date = date('Y-m-d', strtotime($request->dependentBirthDate));
            } else {
                $dependent->birth_date = null;
            }

//          $dependent->birth_date = date('Y-m-d', strtotime($request->dependentBirthDate));
            $dependent->dependentMobile = $request->mobile;
            $dependent->dependentEmail = $request->email;
            $dependent->dependentChildren = $request->children;
            $dependent->save();


            $userId = InquiryWiseUser::where('id', $dependent->inquiry_id)->first();

            $user = new InquiryDependentUser();
            $user->dependent_id = $dependent->id;
            $user->inquiry_id = $dependent->inquiry_id;
            $user->user_id = $userId->user_id;
            $user->save();

            if (isset($request->child)) {
                foreach($request->child as $children) {
                    $child = new InquiryDependentChildren();
                    $child->dependent_id = $dependent->id;
                    $child->name = $children['child_name'];
                    $child->age_in_year = $children['age_yr'];
                    $child->age_in_month = $children['age_mnth'];
                    if ($children['birth_date'] != null) {
                        $child->birth_date = date('Y-m-d', strtotime($children['birth_date']));
                    } else {
                        $child->birth_date = null;
                    }
                    $child->save();
                }
            }

        }

        $dependentDetail = InquiryDependentUser::where('user_id', Admin::user()->id)->orderBy('id', 'DESC')->first();

        $dependentId = $dependentDetail->id;

        if ($error != '') {
            return response()->json($error, 500);

        } else {
            return response()->json(array('result' => "success", 'dependent_id' => $dependentId), 200);
        }

    }

    public function setDependentAcademicInformation(Request $request)
    {

        $academics = $request->dependent_academic;
        $dependentId = $request->dependentInquiryId;

        foreach ($academics as $key => $value) {

            if ($value['exam_passed'] == null && $value['institute'] == null && $value['year'] == null && $value['board'] == null && $value['result'] == null && $value['backlog'] != null) {
                unset($academics[$key]);
            } else {
                $academic = new InquiryDependentAcademic();

                $academic->dependent_id = $dependentId;
                $academic->exam_passed = $value['exam_passed'];
                $academic->institute = $value['institute'];
                $academic->year = $value['year'];
                $academic->board = $value['board'];
                $academic->result = $value['result'];
                $academic->backlog = $value['backlog'];
                $academic->save();
            }

        }
        return "success";
    }

    public function setDependentExamInformation(Request $request)
    {

        $exams = $request->dependent_exam;
        $dependentId = $request->dependentInquiryId;
        $remark = $request->examRemark;


        if ($dependentId == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryDependentExam();

                    $exam->dependent_id = $dependentId;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($value['result'])) {
                        $exam->result_status = $value['result'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }
            return "success";
        }
    }

    public function setDependentWorkExperienceInformation(Request $request)
    {
        $workExperiences = $request->work;
        $dependentId = $request->dependentInquiryId;

        if ($dependentId == null) {
            return "error";
        } else {
            foreach ($workExperiences as $key => $value) {

                if ($value['company'] == null && $value['designation'] == null && $value['duration'] == null && $value['location'] == null && $value['remark'] == null) {
                    unset($workExperiences[$key]);
                } else {
                    $workExperience = new InquiryDependentWorkExperience();

                    $workExperience->dependent_id = $dependentId;
                    $workExperience->company = $value['company'];
                    $workExperience->designation = $value['designation'];
                    $workExperience->duration = $value['duration'];
                    $workExperience->location = $value['location'];
                    $workExperience->remark = $value['remark'];
                    $workExperience->save();
                }

            }
            return "success";

        }

    }

    public function setDependentTravelHistoryInformation(Request $request)
    {
        $travelHistory = $request->travel;
        $dependent_id = $request->dependentInquiryId;


        if ($dependent_id == null) {
            return "error";
        } else {

            if (isset($travelHistory) && $request->country == null && $request->program == null && $request->status == null && $request->year == null && $request->dependentYearDuration == null && $request->dependent_remarks == null) {
                return response()->json("noData", 500);
            } else {
                $travelHistory = new InquiryDependentTravelHistory();
                $travelHistory->dependent_id = $dependent_id;
                $travelHistory->country = $request->country;
                $travelHistory->program = $request->program;
                $travelHistory->status = $request->status;
                $travelHistory->year = $request->year;
                $travelHistory->duration = $request->dependentYearDuration;
                $travelHistory->remark = $request->dependent_remarks;
                $travelHistory->save();
            }

            foreach ($travelHistory as $key => $value) {

                if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                    unset($travelHistory[$key]);
                } else {

                    if (isset($request->travel)) {

                        $travel = new InquiryDependentTravelHistory();
                        $travel->dependent_id = $dependent_id;
                        $travel->country = $value['country'];
                        $travel->program = $value['program'];
                        $travel->status = $value['status'];
                        $travel->year = $value['year'];
                        $travel->duration = $value['duration'];
                        $travel->remark = $value['remark'];
                        $travel->save();

                    } else {

                        $travel = new InquiryDependentTravelHistory();
                        $travel->dependent_id = $dependent_id;
                        $travel->country = $request->country;
                        $travel->program = $request->program;
                        $travel->status = $request->status;
                        $travel->year = $request->year;
                        $travel->duration = $request->dependentYearDuration;
                        $travel->remark = $request->dependent_remarks;
                        $travel->save();

                    }
                }
            }

            return "success";
        }

    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            Admin::script(["
                $('<link/>', {
                   rel: 'stylesheet',
                   type: 'text/css',
                   href: '/css/inquiry.css'
                }).appendTo('head');
            
                $.getScript('/js/edit-inquiry-personal-info.js');
                $.getScript('/js/edit-inquiry-academic.js');
                $.getScript('/js/edit-inquiry-exam.js');
                $.getScript('/js/edit-inquiry-work-experience.js');
                $.getScript('/js/edit-inquiry-travel-history.js');
                $.getScript('/js/edit-inquiry-counselling.js');
                $.getScript('/js/edit-dependent-detail.js');
                $.getScript('/js/edit-dependent-academic.js');
                $.getScript('/js/edit-dependent-exam.js');
                $.getScript('/js/edit-dependent-work.js');
                $.getScript('/js/edit-dependent-travel.js');
            "]);

            $inquiry = VisaInquiry::where('id', $id)->first();

            $users = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->first();

            $user_id = $users->user_id;

            $counselors = Administrator::whereHas("roles", function($query){
                $query->where("slug", "counselor");
            })->get();

            $dependentUserId = InquiryDependentUser::where('inquiry_id', $users->id)->orderBy('id', 'DESC')->first();

            $countryDetails = CountryProgram::where("inquiry_id", $inquiry->id)->get();

            $academics = InquiryAcademic::where('inquiry_id', $users->id)->get();

            $exams = InquiryExam::where('inquiry_id', $users->id)->get();
            $examData = [];

            foreach ($exams as $exam) {
                $examData[$exam->exam_type] = $exam;
            }

            $workExperiences = InquiryWorkExperience::where('inquiry_id', $users->id)->get();

            $travelHistory = InquiryTravelHistory::where('inquiry_id', $users->id)->get();

            $counsellingSummary = InquiryCounsellingSummary::where('inquiry_id', $users->id)->get();

            $dependentDetail = InquiryDependentDetail::where('inquiry_id', $users->id)->first();

            if (isset($dependentDetail)) {
                $dependentChild = InquiryDependentChildren::where('dependent_id', $dependentDetail->id)->get();
            } else {
                $dependentChild = '';
            }

            $dependentExam = array();

            if (isset($dependentDetail)) {

                $dependentExam = InquiryDependentExam::where('dependent_id', $dependentUserId->id)->get();
                $dependentExamData = [];

                foreach ($dependentExam as $exam) {
                    $dependentExamData[$exam->exam_type] = $exam;
                }

            }

            $dependentAcademic = array();

            if (isset($dependentDetail)) {
                $dependentAcademic = InquiryDependentAcademic::where('dependent_id', $dependentUserId->id)->get();
            }

            $dependentWork = array();
            if (isset($dependentDetail)) {
                $dependentWork = InquiryDependentWorkExperience::where('dependent_id', $dependentUserId->id)->get();

            }

            $dependentTravel = array();
            if (isset($dependentDetail)) {
                $dependentTravel = InquiryDependentTravelHistory::where('dependent_id', $dependentUserId->id)->get();

            }

            $content->header('Edit Inquiry');

            $content->body(view('inquiry.createInquiry', compact('inquiry', 'dependentUserId', 'users', 'user_id', 'counselors', 'countryDetails', 'academics', 'exams', 'examData', 'workExperiences', 'travelHistory', 'counsellingSummary', 'dependentDetail', 'dependentChild', 'dependentExam', 'dependentAcademic', 'dependentWork', 'dependentTravel', 'dependentExamData')));
        });
    }

    public function update(VisaInquiry $visa_inquiry)
    {
        $error = "";
        $validate = array(
            'name' => 'required',
            'mobile' => ['required', 'digits:10',
                Rule::unique('visa_inquiries')->ignore($visa_inquiry->id)
            ],
            'address' => 'required',
            'alternate_mobile' => 'numeric | nullable',
            'email' => 'email | nullable');

        $messsages = array(
            'mobile.digits' => 'The mobile number must be of 10 digits',
            'mobile.unique' => 'The mobile number is already taken',
            'name.required' => 'The name field is required',
            'alternate_mobile.unique' => 'The alternate number is already taken',
        );

        $validateForPersonalInfo = Validator::make(request()->all(), $validate, $messsages);

        if ($validateForPersonalInfo->fails()) {

            $error = $validateForPersonalInfo->getMessageBag();
        } else {

            $visa_inquiry->id = request()->inquiry_id;
            $visa_inquiry->name = request()->name;
            if (request()->birth_date != null) {
                $visa_inquiry->birth_date = date('Y-m-d', strtotime(request()->birth_date));
            } else {
                $visa_inquiry->birth_date = null;
            }
            $visa_inquiry->gender = request()->gender;
            $visa_inquiry->mobile = request()->mobile;
            $visa_inquiry->alternate_mobile = request()->alternate_mobile;
            $visa_inquiry->email = request()->email;
            $visa_inquiry->address = request()->address;
            $visa_inquiry->city_id = request()->city;
            $visa_inquiry->area_id = request()->area;
            $visa_inquiry->marital_status = request()->m_status;
            $visa_inquiry->source_id = request()->source;
            $visa_inquiry->friend_name = request()->friendource;
            $visa_inquiry->tag = request()->tagfriend;
            $visa_inquiry->other_source = request()->otherSource;
            $visa_inquiry->branch_id = request()->branch_id;
            $visa_inquiry->remark = request()->remarks;
            $visa_inquiry->status = request()->status;
            $visa_inquiry->save();

        }
        CountryProgram::where('inquiry_id', $visa_inquiry->id)->delete();

        if (isset(request()->program)) {

            for ($i = 0; $i < sizeof(request()->program); $i++) {
                $program = new CountryProgram();
                $program->inquiry_id = $visa_inquiry->id;
                $program->country_id = request()->country[$i];
                $program->program_id = request()->program[$i];
                $program->save();
            }
        } else {
            $program = new CountryProgram();
            $program->inquiry_id = $visa_inquiry->id;
            $program->country_id = request()->inquiryCountry;
            $program->program_id = request()->course_id;
            $program->save();
        }

        $userWiseId = InquiryWiseUser::where('inquiry_id', request()->inquiry_id)->first();

        if ($userWiseId->user_id != request()->user_id) {

            $user = new InquiryUserHistory();
            $user->inquiry_id = $userWiseId->id;
            $user->user_id = $userWiseId->user_id;
            $user->save();

            InquiryWiseUser::where('inquiry_id', request()->inquiry_id)->update(['inquiry_id' => $userWiseId->inquiry_id, 'user_id' => request()->user_id]);

        }


        if (!empty(request()->reason)) {

            if (!empty(request()->reason)) {
                $reason = request()->reason;
            } else {
                $reason = "Reason not available.";
            }

            Reason::create([
                'assignor_id' => Admin::user()->id,
                'inquiry_id' => $visa_inquiry->id,
                'reason' => $reason
            ]);

        }

        if ($error != '') {
            return response()->json($error, 500);

        } else {

            return response()->json(array('result' => "updated", 'inquiry_id' => $visa_inquiry->id), 200);

        }

    }

    public function updateInquiryAcademic(Request $request, $id)
    {

        DB::beginTransaction();

        $academicRecords = request()->academic;

        InquiryAcademic::where('inquiry_id', $id)->delete();


        foreach ($academicRecords as $key => $academic) {


            $arr['exam_passed'] = $academic['exam_passed'];
            $arr['institute'] = $academic['institute'];
            $arr['year'] = $academic['year'];
            $arr['board'] = $academic['board'];
            $arr['result'] = $academic['result'];

            $messsages = array(
                'year.numeric' => 'The year field should be numeric',
            );

            $rules = array(
                'year' => ['numeric '],
            );

            $validateForAcademic = Validator::make($arr, $rules, $messsages);

            if ($validateForAcademic->fails()) {

                $error['academic'][$key] = $validateForAcademic->getMessageBag();

            } else {

                $academicInfo = new InquiryAcademic();

                $academicInfo->inquiry_id = $id;
                $academicInfo->exam_passed = $academic['exam_passed'];
                $academicInfo->institute = $academic['institute'];
                $academicInfo->year = $academic['year'];
                $academicInfo->board = $academic['board'];
                $academicInfo->result = $academic['result'];
                $academicInfo->save();

            }

        }

        DB::commit();
        return response()->json("updated", 200);
    }

    public function updateInquiryWorkExperience($id)
    {

//        $arr = [];
//        $error = [];


        DB::beginTransaction();

        $workRecords = request()->work;

        InquiryWorkExperience::where('inquiry_id', $id)->delete();


        foreach ($workRecords as $key => $work) {


            $arr['company'] = $work['company'];
            $arr['designation'] = $work['designation'];
            $arr['duration'] = $work['duration'];
            $arr['location'] = $work['location'];
            $arr['remark'] = $work['remark'];


//            $messsages = array(
//                'duration.numeric' => 'The duration field should be numeric',
//            );
//
//            $rules = array(
//                'duration' => ['numeric '],
//            );

//            $validateForWork = Validator::make($arr, $rules, $messsages);
//
//            if ($validateForWork->fails()) {
//
//                $error['work'][$key] = $validateForWork->getMessageBag();
//
//            } else {

            $workExperience = new InquiryWorkExperience();

            $workExperience->inquiry_id = $id;
            $workExperience->company = $work['company'];
            $workExperience->designation = $work['designation'];
            $workExperience->duration = $work['duration'];
            $workExperience->location = $work['location'];
            $workExperience->remark = $work['remark'];
            $workExperience->save();

        }

//        }

//        if (count($error) > 0) {
////            DB::rollback();
////            return response()->json($error, 500);
//            DB::commit();
//            return response()->json("updated", 200);
//
//        } else {
        DB::commit();
        return response()->json("updated", 200);
//        }
    }

    public function updateInquiryTravelHistory($id, Request $request)
    {

        InquiryTravelHistory::where('inquiry_id', $id)->delete();

        if ($id == null) {
            return "error";
        } else {

            /*if (isset($travelRecords) && $request->country == null && $request->program == null && $request->status == null && $request->year == null && $request->yearDuration == null && $request->remarks == null) {
                return response()->json("noData", 500);
            } else {
                $travelHistory = new InquiryTravelHistory();
                $travelHistory->inquiry_id = $id;
                $travelHistory->country = $request->country;
                $travelHistory->program = $request->program;
                $travelHistory->status = $request->status;
                $travelHistory->year = $request->year;
                $travelHistory->duration = $request->yearDuration;
                $travelHistory->remark = $request->remark;
                $travelHistory->save();
            }*/

            if (isset($request->all()['travel'])) {
                $travelRecords = $request->all()['travel'];
                foreach ($travelRecords as $key => $value) {

                    if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                        unset($travelRecords[$key]);
                    }

                    if (isset($request->travel)) {

                        $travel = new InquiryTravelHistory();
                        $travel->inquiry_id = $id;
                        $travel->country = $value['country'];
                        $travel->program = $value['program'];
                        $travel->status = isset($value['status']) ? $value['status'] : '';
                        $travel->year = $value['year'];
                        $travel->duration = $value['duration'];
                        $travel->remark = $value['remark'];
                        $travel->save();

                    }/* else {

                        $travel = new InquiryTravelHistory();
                        $travel->inquiry_id = $id;
                        $travel->country = $request->country;
                        $travel->program = $request->program;
                        $travel->status = $request->status;
                        $travel->year = $request->year;
                        $travel->duration = $request->yearDuration;
                        $travel->remark = $request->remarks;
                        $travel->save();

                    }*/

                }

            }
        }
        return "updated";

    }

    public function updateInquiryCounsellingSummary(Request $request, $id)
    {

        InquiryCounsellingSummary::where('inquiry_id', $id)->delete();

        $summary = new InquiryCounsellingSummary();
        $summary->inquiry_id = $id;
        $summary->country_id = $request->country_id;
        $summary->program_id = $request->program_id;
        $summary->suggestion = $request->suggestion;
        $summary->remark = $request->remark;
        $summary->save();

        return "updated";

    }

    public function updateInquiryExam($id, Request $request)
    {

        InquiryExam::where('inquiry_id', $id)->delete();

        $exams = $request->exam;
        $inquiry = $id;
        $remark = $request->examRemark;


        if ($inquiry == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryExam();

                    $exam->inquiry_id = $inquiry;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }

            return "updated";

        }

    }

    public function updateDependentDetail($id, Request $request)
    {
        $dependentDetails = InquiryDependentDetail::all();

        if (count($dependentDetails) > 0) {

            foreach ($dependentDetails as $detail) {
                if ($detail->inquiry_id == $id) {
                    InquiryDependentDetail::where('inquiry_id', $id)->update(['name' => $request->dependentName, 'birth_date' => date('Y-m-d', strtotime($request->dependentBirthDate)), 'mobile' => $request->mobile, 'email' => $request->email, 'children' => $request->children]);

                    $userId = InquiryWiseUser::where('id', $detail->inquiry_id)->first();

                    $user = new InquiryDependentUser();
                    $user->dependent_id = $detail->id;
                    $user->inquiry_id = $detail->inquiry_id;
                    $user->user_id = $userId->user_id;
                    $user->save();


                    InquiryDependentChildren::where('dependent_id', $detail->id)->delete();

                    if (isset($request->child)) {
                        foreach($request->child as $children) {
                            $child = new InquiryDependentChildren();
                            $child->dependent_id = $detail->id;
                            $child->name = $children['child_name'];
                            $child->age_in_year = $children['age_yr'];
                            $child->age_in_month = $children['age_mnth'];
                            if ($children['birth_date'] != null) {
                                $child->birth_date = date('Y-m-d', strtotime($children['birth_date']));
                            } else {
                                $child->birth_date = null;
                            }
                            $child->save();
                        }
                    }
                }
            }
        } else {
            $error = "";

            $validate = array(
                'mobile' => ['digits:10', 'nullable',
                    Rule::unique('inquiry_dependent_details')->ignore($id)
                ],
                'email' => 'email | nullable');

            $messsages = array(
                'mobile.digits' => 'The mobile number must be of 10 digits',
                'mobile.unique' => 'The mobile number is already taken',
            );

            $validateForDependentInfo = Validator::make($request->all(), $validate, $messsages);

            if ($validateForDependentInfo->fails()) {

                $error = $validateForDependentInfo->getMessageBag();

            } else {
                $dependent = new InquiryDependentDetail();
                $dependent->inquiry_id = $request->inquiryFormId;
                $dependent->name = $request->dependentName;
                if ($request->birth_date != null) {
                    $dependent->birth_date = date('Y-m-d', strtotime($request->dependentBirthDate));
                } else {
                    $dependent->birth_date = null;
                }
                $dependent->mobile = $request->mobile;
                $dependent->email = $request->email;
                $dependent->children = $request->children;
                $dependent->save();


                $userId = InquiryWiseUser::where('id', $dependent->inquiry_id)->first();

                $user = new InquiryDependentUser();
                $user->dependent_id = $dependent->id;
                $user->inquiry_id = $dependent->inquiry_id;
                $user->user_id = $userId->user_id;
                $user->save();

                if (isset($request->child)) {
                    for ($i = 0; $i < sizeof($request->child); $i++) {
                        $child = new InquiryDependentChildren();
                        $child->dependent_id = $dependent->id;
                        $child->name = $request->child_name[$i];
                        $child->age_in_year = $request->age_yr[$i];
                        $child->age_in_month = $request->age_mnth[$i];
                        if ($request->birth_date != null) {
                            $child->birth_date = date('Y-m-d', strtotime($request->birth_date[$i]));
                        } else {
                            $child->birth_date = null;
                        }
                        $child->save();
                    }
                }
            }
        }
        return "updated";
    }

    public function updateDependentAcademic($id, Request $request)
    {

        InquiryDependentAcademic::where('dependent_id', $id)->delete();


        $academics = $request->dependent_academic;
        $dependentId = $id;

        foreach ($academics as $key => $value) {

            if ($value['exam_passed'] == null && $value['institute'] == null && $value['year'] == null && $value['board'] == null && $value['result'] == null) {
                unset($academics[$key]);
            } else {
                $academic = new InquiryDependentAcademic();

                $academic->dependent_id = $dependentId;
                $academic->exam_passed = $value['exam_passed'];
                $academic->institute = $value['institute'];
                $academic->year = $value['year'];
                $academic->board = $value['board'];
                $academic->result = $value['result'];
                $academic->save();
            }

        }
        return "updated";

    }

    public function updateDependentExam($id, Request $request)
    {

        InquiryDependentExam::where('dependent_id', $id)->delete();

        $exams = $request->exam;
        $dependentId = $id;
        $remark = $request->examRemark;


        if ($dependentId == null) {
            return "error";
        } else {

            foreach ($exams as $key => $value) {

                if ($value['exam_type'] != null && isset($value['listening']) == null && isset($value['reading']) == null && isset($value['writing']) == null && isset($value['speaking']) == null && isset($value['overall']) == null && isset($value['year']) == null) {
                    unset($exams[$key]);
                } else {

                    $exam = new InquiryDependentExam();

                    $exam->dependent_id = $dependentId;
                    $exam->exam_type = $value['exam_type'];
                    if (isset($value['listening'])) {
                        $exam->listening = $value['listening'];
                    }
                    if (isset($value['reading'])) {
                        $exam->reading = $value['reading'];
                    }
                    if (isset($value['writing'])) {
                        $exam->writing = $value['writing'];
                    }
                    if (isset($value['speaking'])) {
                        $exam->speaking = $value['speaking'];
                    }
                    if (isset($value['overall'])) {
                        $exam->overall = $value['overall'];
                    }
                    if (isset($value['year'])) {
                        $exam->year = $value['year'];
                    }
                    if (isset($remark)) {
                        $exam->remark = $remark;
                    }
                    $exam->save();
                }

            }
            return "updated";
        }

    }

    public function updateDependentWorkExperience($id, Request $request)
    {

        InquiryDependentWorkExperience::where('dependent_id', $id)->delete();

        $workExperiences = $request->work;

        $dependentId = $id;

        if ($dependentId == null) {
            return "error";
        } else {

            foreach ($workExperiences as $key => $value) {

                if ($value['company'] == null && $value['designation'] == null && $value['duration'] == null && $value['location'] == null && $value['remark'] == null) {
                    unset($workExperiences[$key]);
                } else {
                    $workExperience = new InquiryDependentWorkExperience();

                    $workExperience->dependent_id = $dependentId;
                    $workExperience->company = $value['company'];
                    $workExperience->designation = $value['designation'];
                    $workExperience->duration = $value['duration'];
                    $workExperience->location = $value['location'];
                    $workExperience->remark = $value['remark'];
                    $workExperience->save();
                }

            }
            return "updated";

        }

    }

    public function updateDependentTravel($id, Request $request)
    {

        InquiryDependentTravelHistory::where('dependent_id', $id)->delete();

        if(isset($request->all()["travel"])){

            $travelHistory = $request->all()["travel"];

            foreach ($travelHistory as $key => $value) {
                if ($value['country'] == null && $value['program'] == null && $value['status'] == null && $value['year'] == null && $value['duration'] == null && $value['remark'] == null) {
                    unset($travelHistory[$key]);
                }

                $travel = new InquiryDependentTravelHistory();
                $travel->dependent_id = $id;
                $travel->country = $value['country'];
                $travel->program = $value['program'];
                $travel->status = $value['status'];
                $travel->year = $value['year'];
                $travel->duration = $value['duration'];
                $travel->remark = $value['remark'];
                $travel->save();
            }
        }
        return "updated";
    }


    public function completedInquiries()
    {
        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Admin::user()->id;

        }

        if ($userId != 1) {

            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('visa_inquiries.status', '=', 1)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        } else {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->where('visa_inquiries.status', '=', 1)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        }

        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function cancelledInquiries()
    {
        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Admin::user()->id;

        }

        if ($userId != 1) {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");

            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('visa_inquiries.status', '=', 2)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();

        } else {

            $inquiries = Inquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->where('visa_inquiries.status', '=', 2)->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
            /*$inquiries = Inquiry::where("status", "=", 2)->get();*/
        }

        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function forwardedInquiries()
    {
        if (Session::has('user_id')) {
            $userId = Session::get('user_id');
        } else {
            $userId = Admin::user()->id;
        }

        if ($userId != 1) {
            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        } else {
            $inquiries = VisaInquiry::leftJoin('reasons', function ($join) {
                $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
            })->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('visa_inquiries.updated_at', 'desc')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
            /*$inquiries = Inquiry::join('reasons', 'inquiries.id', '=', 'reasons.inquiry_id')->where('inquiries.status', '=', 0)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();*/
        }
        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function getArea($id)
    {

        $area = Area::where('city_id', $id)->get();

        return Response::json($area);

    }

    public function autoCompleteFriend()
    {

        $friend = VisaInquiry::where('friend_name', '!=', null)->groupBy('friend_name')->pluck('friend_name')->toArray();

        $tag = VisaInquiry::whereIn('friend_name', $friend)->groupBy('tag')->pluck('tag')->toArray();

        return response()->json(array('friend' => $friend, 'tag' => $tag));
    }


    public function destroy(VisaInquiry $inquiry)
    {

        if (Session::has('user_id')) {

            $userId = Session::get('user_id');

        } else {

            $userId = Admin::user()->id;
        }

        DB::beginTransaction();

        $inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->value('id');

        InquiryAcademic::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryExam::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryWorkExperience::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryTravelHistory::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        InquiryCounsellingSummary::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        $dependent = InquiryDependentDetail::where('inquiry_id', '=', $inquiry_wise_user_id)->value('id');

        $dependent_inquiry_id = InquiryDependentUser::where('dependent_id', '=', $dependent)->where('user_id', $userId)->value('id');

        InquiryDependentChildren::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentExam::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentAcademic::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentWorkExperience::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentTravelHistory::where('dependent_id', '=', $dependent_inquiry_id)->delete();

        InquiryDependentUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->delete();

        InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->delete();

        FollowUp::where('inquiry_wise_user_id', '=', $inquiry_wise_user_id)->delete();

        InquiryDependentDetail::where('inquiry_id', '=', $inquiry_wise_user_id)->delete();

        $inquiry->delete();

        DB::commit();

        return redirect('/inquiries');
    }


    public function followUp(VisaInquiry $inquiry)
    {
        return Admin::content(function (Content $content) use($inquiry) {

            $content->header('Follow Ups');

            //$inquiry_wise_user_id = request()->inquiry_wise_user_id;

            $counselling = InquiryCounsellingSummary::where('inquiry_id', $inquiry->id)->orderBy('id', 'DESC')->first();

            $followUps = FollowUp::join('inquiry_wise_users', 'follow_ups.inquiry_wise_user_id', '=', 'inquiry_wise_users.id')->where('inquiry_wise_users.inquiry_id', '=', $inquiry->id)->orderBy('follow_ups.id', 'desc')->select('follow_ups.*', 'inquiry_wise_users.user_id')->get();

            $inquiry->userName = VisaInquiry::getUserNames($inquiry->id, Admin::user()->id);

            $content->row(function (Row $row) use($inquiry, $followUps, $counselling){
                $row->column(3, view('inquiry.followUp', compact('inquiry', 'followUps', 'counselling')));
                $row->column(9, $this->followUpGrid());
            });



            //  $content->body(view('inquiry.followUp', compact('inquiry', 'followUps', 'counselling')));
        });
    }

    public function createFollowUp(VisaInquiry $inquiry)
    {
        Admin::script(["
            $('#datetimepicker1').datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
            $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/inquiries/". $inquiry->id ."/followUp/create');
        "]);

        return Admin::content(function (Content $content) use($inquiry) {

            $content->header('Create Follow Up');

            $followUps = FollowUp::join('inquiry_wise_users', 'follow_ups.inquiry_wise_user_id', '=', 'inquiry_wise_users.id')->where('inquiry_wise_users.inquiry_id', '=', $inquiry->id)->orderBy('follow_ups.id', 'desc')->select('follow_ups.*', 'inquiry_wise_users.user_id')->get();

            $content->row(function(Row $row) use ($followUps, $inquiry){
                $row->column(4, view('inquiry.createFollowUp', compact('inquiry', 'followUps')));
                $row->column(8, $this->followUpGrid());
            });
        });
    }

    public function storeFollowUp()
    {
        $inquiry_wise_user = InquiryWiseUser::where("inquiry_id", request()->inquiry_id)->first();

        FollowUp::create([
            'inquiry_wise_user_id' => $inquiry_wise_user->id,
            'interest_level' => request()->interest_level,
            'status' => request()->status,
            'action' => request()->action,
            'next_follow_up' => date('Y-m-d H:i:s', strtotime(request()->next_follow_up)),
            'comments' => request()->comments
        ]);

        return redirect('/inquiries/' . request()->inquiry_id . '/followUp');
    }

    public function editFollowUp(FollowUp $followUp)
    {
        Admin::script(["
            $('#datetimepicker1').datetimepicker({format: 'DD-MM-YYYY HH:mm:ss'});
            $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/inquiries/". $followUp->inquiryWiseUser->inquiry_id ."/followUp/create');
        "]);

        return Admin::content(function (Content $content) use($followUp) {

            $content->header('Edit Follow Up');

            $inquiry = $followUp->inquiryWiseUser->inquiry;

            $content->row(function(Row $row) use ($inquiry, $followUp){
                $row->column(4, view('inquiry.editFollowUp', compact('followUp', 'inquiry')));
                $row->column(8, $this->followUpGrid());
            });

            //$content->body(view('inquiry.editFollowUp', compact('followUp', 'inquiry')));

        });
    }

    public function updateFollowUp(FollowUp $followUp)
    {
        $followUp->inquiry_wise_user_id = request()->inquiry_wise_user_id;
        $followUp->interest_level = request()->interest_level;
        $followUp->status = request()->status;
        $followUp->action = request()->action;
        $followUp->next_follow_up = date('Y-m-d H:i:s', strtotime(request()->next_follow_up));
        $followUp->comments = request()->comments;

        $followUp->save();

        return redirect('/inquiries/' . $followUp->inquiryWiseUser->inquiry_id . '/followUp');
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(VisaInquiry::class, function (Grid $grid) {

            $grid->column('name', "Name")->display(function(){
                return $this->first_name . " " . $this->middle_name . " " . $this->last_name;
            })->sortable();
            $grid->mobile('Mobile')->sortable();
            $grid->column('city.name', 'City')->sortable();
            $grid->column('area.name', 'Area')->sortable();
            $grid->column('source.name', 'Source')->sortable();
            $grid->column("Assigned", "Assigned To")->display(function () {
                return VisaInquiry::getUserNames($this->id);
            })->sortable();
            /*$grid->status("Status")->display(function ($status) {
                return '<lable class="label label-' . config("app.inquiry_status." . $status . ".class") . '">' . config("app.inquiry_status." . $status . ".name") . '</lable>';
            })->sortable();*/
            $grid->column("Follow", "Total Follow Ups")->display(function () {
                return VisaInquiry::getNumberOfFollowUps($this->id);
            })->sortable();
            $grid->column("Days Past")->display(function () {
                return VisaInquiry::getDaysFromStartDateOfInquiry($this->created_at);
            })->sortable();

            $grid->actions(function ($actions) {

                if (isRole("guest")) {
                    $actions->disableDelete();
                } else {

                    $actions->prepend('<a href="/inquiries/' . $actions->row->id . '/followUp" data-toggle="tooltip" title="Follow Up" class="btn" style="background-color: #32c861; border: #32c861"><i class="fa fa-plus"></i></a>');
                    $actions->prepend('<a href="" class="btn" style="background-color: #01a4f8; border: #01a4f8;"><i class="fa fa-eye"></i></a>');
                }

            });

            $value = Input::get('search');

            $q = $grid->model();

            if (isRole("administrator") || isRole("counselor") || isRole("receptionist")) {

                if (!empty($value)) {

                    $q->where("name", "like", "%{$value}%");

                    $q->orWhere("mobile", "like", "%{$value}%");

                    $q->orWhereHas('city', function ($query) use ($value) {
                        $query->where("name", "like", "%{$value}%");
                    });

                    $q->orWhereHas('area', function ($query) use ($value) {
                        $query->where("name", "like", "%{$value}%");
                    });

                    $q->orWhereHas('source', function ($query) use ($value) {
                        $query->where("name", "like", "%{$value}%");
                    });

                    if(isRole("receptionist")){
                        $q->doesntHave('inquiryUser');
                    }

                    /*$q->orWhereHas('inquiryUser', function($query) use ($value){
                        $query->orWhereHas('user', function ($query) use ($value){
                            $query->where("name", "like", "%{$value}%");
                        });
                    });*/
                } else {

                    if(isRole("receptionist")){
                        $q->doesntHave('inquiryUser');
                    }

                    /*$q->orderBy('status');*/

                    $q->orderBy('id', 'desc');
                }
            }

            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();

            $grid->tools(function ($tools) {
                $tools->append(new InquiryGridSearch());
            });

        });
    }

    protected function followUpGrid()
    {
        return Admin::grid(FollowUp::class, function (Grid $grid) {

            $grid->interest_level('Interest Level')->sortable();
            $grid->status('Follow Up Status')->sortable();
            $grid->action('Follow Up Action')->sortable();
            $grid->next_follow_up('Next Follow Up Date')->sortable();
            $grid->comments('Comments')->sortable();

            if(isRole("administrator") || isRole("counselor") || isRole("receptionist")) {
                $grid->actions(function ($actions) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                    $actions->prepend('<a href="/inquiries/followUp/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                });
            } else{
                $grid->disableActions();
            }

            $value = Input::get('search');

            if(!empty($value)) {
                $q = $grid->model();

                $q->where("interest_level", "like", "%{$value}%");

                $q->orWhere("status", "like", "%{$value}%");

                $q->orWhere("action", "like", "%{$value}%");

                $q->orWhere("next_follow_up", "like", "%{$value}%");

                $q->orWhere("comments", "like", "%{$value}%");

            }

            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();

            $grid->tools(function ($tools) {
                $tools->append(new FollowUpGridSearch());
            });

            $grid->tools(function ($tools) {
                $tools->append(new ViewInquiry());
            });

        });
    }

}
