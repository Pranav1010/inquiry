<?php

namespace App\Admin\Controllers;

use App\Country;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;

class CountryController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/countries');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/countries');
            "]);

            $content->header('Country');

            $content->row(function(Row $row){
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });

        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Country');

            $content->row(function(Row $row) use ($id) {
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Country');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Country::class, function (Grid $grid) {

            /*$grid->id('ID')->sortable();*/

            $grid->column('name');
            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/countries/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });

            $grid->paginate(10);


            /*$grid->created_at();
            $grid->updated_at();*/
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Country::class, function (Form $form) {

            $form->text('name')->rules('required');

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/countries");
            });

            $form->tools(function (Form\Tools $tools) {

                // Disable back btn.
                $tools->disableBackButton();

                // Disable list btn
                $tools->disableListButton();

                // Add a button, the argument can be a string, or an instance of the object that implements the Renderable or Htmlable interface
                //$tools->add('<a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;delete</a>');
            });

        });
    }
}
