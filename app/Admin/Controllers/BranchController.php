<?php

namespace App\Admin\Controllers;

use App\Branch;
use App\BranchDetail;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Encore\Admin\Widgets\InfoBox;

class BranchController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('select').select2();
            $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/branches');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/branches');
            "]);

            $content->header('Branches');

            $content->row(function(Row $row){
                $row->column(4, view('branch.createBranch'));
                $row->column(8, $this->grid());
            });
        });
    }

    public function show(Branch $branch)
    {
        $contactDetails = BranchDetail::where('branch_id', $branch->id)->get();
        return view('branch.modal-view', compact('branch', 'contactDetails'));
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Branches');
            $branch = Branch::where('id', $id)->first();
            $branchDetails = BranchDetail::where('branch_id', $branch->id)->get();

            $content->row(function(Row $row) use($branch, $branchDetails){
                $row->column(4, view('branch.editBranch', compact('branch', 'branchDetails')));
                $row->column(8, $this->grid());
            });

            //$content->body(view('branch.editBranch', compact('branch', 'branchDetails')));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Branches');
            $content->body(view('branch.createBranch'));
        });
    }

    public function store(Request $request)
    {
        $arr = [];
        $error = [];
        $branch_id = 0;

        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'contact_no' => ['required', 'digits:10 ', 'unique:branches,contact_no'],
        ]);

        DB::beginTransaction();

        if ($validate->fails()) {

            $error['branch_details'] = $validate->getMessageBag();
        } else {
            $branch = Branch::create(request(['name', 'address', 'contact_no', 'city_id']));
            $branch_id = $branch->id;
        }

        foreach ($request->contactDetail as $key => $val) {
            $arr['contactname'] = $val['contactname'];
            $arr['mobile'] = $val['mobile'];
            $arr['email'] = $val['email'];

            $messsages = array(
                'contactname.required' => 'The name field is required',
                'contactname.unique' => 'The name is already taken',
            );

            $rules = array(
                'contactname' => ['required '],
                'mobile' => ['required', 'digits:10 ',
                    'unique:branch_details,mobile'],
                'email' => 'email|nullable',
            );

            $validateForContact = Validator::make($arr, $rules, $messsages);

            if ($validateForContact->fails()) {

                $error['contact_details'][$key] = $validateForContact->getMessageBag();
            } else {

                $contacts = new BranchDetail();
                $contacts->branch_id = $branch_id;
                $contacts->name = $val['contactname'];
                $contacts->mobile = $val['mobile'];
                $contacts->email = $val['email'];

                $contacts->save();
            }
        }
        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }

    }

    public function update(Branch $branch)
    {

        $arr = [];
        $error = [];


        DB::beginTransaction();

        $validate = Validator::make(request()->all(), [
            'name' => 'required',
            'address' => 'required',
            'contact_no' => ['required', 'digits:10 ',
                Rule::unique('branches')->ignore(request()->id)
            ],
        ]);

        if ($validate->fails()) {

            $error['branch_details'] = $validate->getMessageBag();

        } else {

            $branch->name = request()->name;
            $branch->address = request()->address;
            $branch->contact_no = request()->contact_no;
            $branch->city_id = request()->city_id;

            $branch->save();

            //Branch::where('id',request()->id)->update(['name' => request()->name, 'address' => request()->address, 'contact_no' => request()->contact_no, 'city_id' => request()->city_id]);

        }

        $contactDetails = request()->contactDetail;
        BranchDetail::where('branch_id', request()->id)->delete();

        foreach ($contactDetails as $key => $detail) {


            $arr['contactname'] = $detail['contactname'];
            $arr['mobile'] = $detail['mobile'];
            $arr['email'] = $detail['email'];

            $messsages = array(
                'contactname.required' => 'The name field is required',
                'contactname.unique' => 'The name is already taken',
            );

            $rules = array(
                'contactname' => ['required '],
                'mobile' => ['required', 'digits:10 ', 'unique:branch_details,mobile'],
                'email' => 'email|nullable',
            );

            $validateForContact = Validator::make($arr, $rules, $messsages);

            if ($validateForContact->fails()) {

                $error['contact_details'][$key] = $validateForContact->getMessageBag();

            } else {


                $contacts = new BranchDetail();
                $contacts->branch_id = request()->id;
                $contacts->name = $detail['contactname'];
                $contacts->mobile = $detail['mobile'];
                $contacts->email = $detail['email'];

                $contacts->save();
            }

        }

        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }

    }

    /*public function destroy($id)
    {
        Branch::where('id', $id)->delete();
        BranchDetail::where('branch_id', $id)->delete();
    }*/

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Branch::class, function (Grid $grid) {

            $grid->column('name','Branch');
            $grid->column('contact_no','Contact Number');
            $grid->column('address','Address');
            $grid->column('city.name','City');
            //$grid->disableCreation();
            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();

            $grid->actions(function($actions){
                $actions->disableEdit();
                $actions->prepend('<a href="/branches/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a href="#" data-id="' . $actions->row->id . '" class="open-branch-view-modal"><i class="fa fa-eye"></i></a>');
            });
            $grid->paginate(10);
        });
    }

    protected function form()
    {
        return Admin::form(Branch::class, function (Form $form) {

        });
    }
}
