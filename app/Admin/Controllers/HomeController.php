<?php

namespace App\Admin\Controllers;

use App\FollowUp;
use App\Http\Controllers\Controller;
use App\VisaInquiry;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function index()
    {
        return Admin::content(function (Content $content) {

            $userId = Admin::user()->id;

            $content->header("Dashboard");

            if(isRole("administrator") || isRole("counselor")){
                $totalInquiries = VisaInquiry::count();
                $completedInquiries = VisaInquiry::where("status", "=", 1)->count();
                $cancelledInquiries = VisaInquiry::where("status", "=", 2)->count();
            } else{
                $totalInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->count();
                $completedInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where("visa_inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $userId)->count();
                $cancelledInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where("visa_inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            }
            $followUps = FollowUp::getUserFollowUps($userId);

            $startDate = date('d-m-Y');
            $endDate = date('d-m-Y');

            $content->row(view('home', compact('totalInquiries', 'followUps', 'completedInquiries', 'cancelledInquiries', 'userId', 'startDate', 'endDate')));
            $content->row($this->grid());

        });
    }

    /**
     * Make a grid builder.
     *
     * @return \Encore\Admin\Grid
     */
    protected function grid()
    {
        return Admin::grid(FollowUp::class, function (Grid $grid) {

            $grid->column( "inquiry","Inquiry")->display(function(){
                return $this->inquiryWiseUser->inquiry->name;
            })->sortable();
            $grid->interest_level('Interest Level')->sortable();
            $grid->status('Follow Up Status')->sortable();
            $grid->action('Follow Up Action')->sortable();
            $grid->next_follow_up('Next Follow Up Date')->sortable();
            $grid->comments('Comments')->sortable();

            $grid->column("Follow","Total Follow Ups")->display(function(){
                return VisaInquiry::getNumberOfFollowUps($this->inquiryWiseUser->inquiry->id);
            })->sortable();
            $grid->created_at("Days Past")->display(function(){
                return VisaInquiry::getDaysFromStartDateOfInquiry($this->inquiryWiseUser->inquiry->created_at);
            })->sortable();

            if(isRole("administrator") || isRole("counselor")) {
                $grid->actions(function ($actions) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                    $actions->prepend('<a href="/inquiries/followUp/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                });
            } else{
                $grid->disableActions();
            }

            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->disableCreateButton();

            $value = Input::get('search');

            /*if(!empty($value)) {
                $q = $grid->model();

                $q->where("interest_level", "like", "%{$value}%");

                $q->orWhere("status", "like", "%{$value}%");

                $q->orWhere("action", "like", "%{$value}%");

                $q->orWhere("next_follow_up", "like", "%{$value}%");

                $q->orWhere("comments", "like", "%{$value}%");

            }*/

            /*$grid->tools(function ($tools) {
                $tools->append(new FollowUpGridSearch());
            });*/

        });
    }
}
