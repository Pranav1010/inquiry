<?php

namespace App\Admin\Controllers;

use App\City;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;

class CityController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/cities');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/cities');
            "]);

            $cities = City::all();
            $content->header('Cities');

            $content->row(function(Row $row) use ($cities){
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Cities');

            $content->row(function(Row $row) use ($id){
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid());
            });

            //$content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Cities');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(City::class, function (Grid $grid) {

            $grid->column('name');
            //$grid->disableCreateButton();
            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->paginate(10);

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/cities/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(City::class, function (Form $form) {

            $form->text('name')->rules('required');

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/cities");
            });

            $form->tools(function (Form\Tools $tools) {

                // Disable back btn.
                $tools->disableBackButton();

                // Disable list btn
                $tools->disableListButton();

                // Add a button, the argument can be a string, or an instance of the object that implements the Renderable or Htmlable interface
                //$tools->add('<a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;delete</a>');
            });

        });
    }
}
