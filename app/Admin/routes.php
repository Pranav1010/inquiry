<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->get('/dashboard', 'HomeController@index');
    $router->get('/auth/setting', 'AuthController@getSetting');
    $router->resource('/auth/users', 'UserController');
    $router->resource('/countries', 'CountryController');
    $router->resource('/cities', 'CityController');
    $router->resource('/areas', 'AreaController');
    $router->resource('/programs', 'CourseController');
    $router->resource('/sources', 'InquirySourceController');
    $router->resource('/branches', 'BranchController');

    $router->get('/completed', 'VisaInquiryController@completedInquiries');
    $router->get('/cancelled', 'VisaInquiryController@cancelledInquiries');
    $router->get('/forwarded', 'VisaInquiryController@forwardedInquiries');
    $router->get('/inquiries/{inquiry}/followUp', 'VisaInquiryController@followUp');
    $router->post('/inquiries/followUp', 'VisaInquiryController@storeFollowUp');
    $router->get('/inquiries/{inquiry}/followUp/create', 'VisaInquiryController@createFollowUp');
    $router->get('/inquiries/followUp/{followUp}/edit', 'VisaInquiryController@editFollowUp');
    $router->put('/inquiries/followUp/{followUp}', 'VisaInquiryController@UpdateFollowUp');

    $router->get('inquiries/ajax-area/{id}', 'VisaInquiryController@getArea');
    $router->get('inquiries/ajax-friend-list/', 'VisaInquiryController@autoCompleteFriend');

    $router->post('inquiries/personal-information/', 'VisaInquiryController@store');
    $router->post('inquiries/inquiry-details/', 'VisaInquiryController@storeInquiryDetails');
    $router->post('inquiries/{inquiry_id}/inquiry-details/', 'VisaInquiryController@editInquiryDetails');
    /*$router->post('inquiries/academic-information/{id}', 'VisaInquiryController@setAcademicInformation');
    $router->post('inquiries/exam-information/{id}', 'VisaInquiryController@setExamInformation');
    $router->post('inquiries/work-experience-information/{id}', 'VisaInquiryController@setWorkExperienceInformation');
    $router->post('inquiries/travel-history-information/{id}', 'VisaInquiryController@setTravelHistoryInformation');
    $router->post('inquiries/counselling-summary-information/{id}', 'VisaInquiryController@setCounsellingSummaryInformation');*/

    /*$router->post('inquiries/dependent-information', 'VisaInquiryController@setDependentInformation');
    $router->post('inquiries/{inquiry_id}/dependent-information', 'VisaInquiryController@updateDependentInformation');*/

    /*$router->post('inquiries/dependent-academic-information/{dependent_id}', 'VisaInquiryController@setDependentAcademicInformation');
    $router->post('inquiries/dependent-exam-information/{id}', 'VisaInquiryController@setDependentExamInformation');
    $router->post('inquiries/dependent-work-experience-information/{id}', 'VisaInquiryController@setDependentWorkExperienceInformation');
    $router->post('inquiries/dependent-travel-history-information/{id}', 'VisaInquiryController@setDependentTravelHistoryInformation');*/

    /*$router->put('inquiries/{visa_inquiry}/edit-personal-information/', 'VisaInquiryController@update');
    $router->put('inquiries/{inquiry_id}/edit-academic-information/', 'VisaInquiryController@updateInquiryAcademic');
    $router->put('inquiries/{inquiry_id}/edit-work-experience-information/', 'VisaInquiryController@updateInquiryWorkExperience');
    $router->put('inquiries/{inquiry_id}/edit-travel-history-information/', 'VisaInquiryController@updateInquiryTravelHistory');
    $router->put('inquiries/{inquiry_id}/edit-counselling-summary-information/', 'VisaInquiryController@updateInquiryCounsellingSummary');
    $router->put('inquiries/{inquiry_id}/edit-exam-information/', 'VisaInquiryController@updateInquiryExam');*/

    /*$router->put('inquiries/{dependent_id}/edit-dependent-detail/', 'VisaInquiryController@updateDependentDetail');
    $router->put('inquiries/{dependent_id}/edit-dependent-academic-information/', 'VisaInquiryController@updateDependentAcademic');
    $router->put('inquiries/{dependent_id}/edit-dependent-exam-information/', 'VisaInquiryController@updateDependentExam');
    $router->put('inquiries/{dependent_id}/edit-dependent-work-information/', 'VisaInquiryController@updateDependentWorkExperience');
    $router->put('inquiries/{dependent_id}/edit-dependent-travel-information/', 'VisaInquiryController@updateDependentTravel');*/

    $router->resource('/inquiries', 'VisaInquiryController');
});
